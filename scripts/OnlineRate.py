#!/usr/bin/env python

import os, sys, re
import psutil
import atexit
import shutil
import argparse
from time import sleep

parser = argparse.ArgumentParser(usage = 'usage: %(prog)s node')

parser.add_argument("partition", nargs = 1, choices = ["LHCb", "LHCb2"])
parser.add_argument("node", nargs = 1)

args = parser.parse_args()

node = args.node[0]
part = args.partition[0]

dim_dns = node[:-2]
os.environ["DIM_DNS_NODE"] = dim_dns

from MooreTests.MooreOfflineOnline import MooreRateCounter

class NodeParent(object):
    def __init__(self, partition, node):
        self.__partition = partition
        self.__node = node.upper()

    def utgid(self, suffix):
        return '_'.join(self.__partition, self.__node, suffix)

    def _notify(self):
        return

from OfflineOnline.TestUtils import *
logger = setup_loggin("online_rate", node + '.log')

processes = []

re_process = re.compile("%s_%s_Moore1_(\d)(\d{2})" % (part, node.upper()))
for p in psutil.process_iter():
    if not p.cmdline:
        continue
    m = re_process.match(p.cmdline[0])
    if m and int(m.group(2)) != 0:
        processes.append((m.group(0), int(m.group(1)), int(m.group(2))))

from operator import itemgetter
processes = sorted(processes, key = itemgetter(0))

parent = NodeParent(part, node)
counter = MooreRateCounter(parent, logger, [p[0] for p in processes])

counter.subscribe()
counter.run()

n_rates = 0
while True:
    try:
        results = counter.results()
        rates = results['inst_rates']
        if len(rates) != n_rates:
            print "Instantaneous rate = %4.3f Hz" % rates[-1]
            n_rates = len(rates)
        if n_rates >= 5:
            break
        sleep(1)
    except KeyboardInterrupt:
        break
