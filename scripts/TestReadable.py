#!/usr/bin/env python
"""
Simple python script to test if given Test files are readable with ROOT

usage: TestReadable.py <TestFilesDBName> [--help]

"""
sepline="\n===================\n"
def usage():
    print __doc__

import sys, os

if "--help" in sys.argv or "-h" in sys.argv:
    usage()
    sys.exit(0)

if len(sys.argv)>2:
    usage()
    raise AttributeError("Choose only one test file object please")
    sys.exit(0)

try:
    import PRConfig
    from PRConfig import TestFileDB
except ImportError:
    usage()
    raise ImportError("Module PRConfig not found, you need to run this script inside an LHCb application environment, try adding --use PRConfig if all else fails. You also may need to source setup.sh from your checked-out PRConfig package.")


try:
    from ROOT import TFile
except ImportError:
    usage()
    raise ImportError("ROOT not found, you need to run this script inside an LHCb application environment with ROOT.")

try:
    from GaudiConf import IOHelper, IOExtension
except ImportError:
    usage()
    raise ImportError("IOHelper not found, you need to run this script inside an LHCb application environment with ROOT.")

testfile=None
if len(sys.argv)>1:
    testfile=sys.argv[1]
else:
    print "please choose a testfile set to check"
    print TestFileDB.test_file_db.keys()
    testfile=raw_input("Choice: ")

#print "WHAT"

if testfile not in TestFileDB.test_file_db:
    usage()
    raise AttributeError("The test file you have requested does not exist, please try again"+testfile)

# a simple class with a write method
class WritableObject:
    def __init__(self):
        self.content = []
    def write(self, string):
        self.content.append(string)

def printOFF():
    mystderr=WritableObject()
    mystdout=WritableObject()
    sys.stdout=mystdout
    sys.stderr=mystderr
    return mystdout, mystderr

def printON():
    sys.stdout=sys.__stdout__
    sys.stderr=sys.__stderr__
    

def accessfileROOT(filename):
    stdout, stderr=printOFF()
    
    thefile=TFile.Open(filename.replace("PFN:",""))
    
    printON()
    if not thefile or (hasattr(thefile, "isZombie") and thefile.isZombie) or (hasattr(thefile,"IsZombie") and thefile.IsZombie()):
        if thefile:
            thefile.Close()
        print sepline.lstrip(),stdout.content,sepline.rstrip()
        print stderr.content,sepline.rstrip()
        raise IOError("a file could not be opened by Root, check the dumped output, "+ filename)
    thefile.Close()

import commands
def accessFileGaudi(filename):
    printON()
    opts='--option="from Gaudi.Configuration import *; from Configurables import LHCbApp; from GaudiConf import IOExtension; LHCbApp(); IOExtension().inputFiles(['+"'"+filename+"'"+']); LHCbApp().EvtMax=1;"'
    stat,out=commands.getstatusoutput("gaudirun.py " +opts)
    if stat or "FATAL" in out or "Application Manager Finalized successfully" not in out:
        print sepline.lstrip(),out,sepline.rstrip()
        raise IOError("a file could not be opened by Gaudi! check the dumped output, " +filename)

for afile in TestFileDB.test_file_db[testfile].filenames:
    if IOExtension().detectFileType(afile)=="ROOT":
        accessfileROOT(afile)
    accessFileGaudi(afile)
    print afile, "OK"

print sepline.lstrip(),"Files read successfully :) (if not, you would have seen an exception)"
