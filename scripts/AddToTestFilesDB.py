#!/usr/bin/env python
"""
Simple python script to append lists of files to the file database

setup: SetupProject --build-env a project corresponding to your options files
       getpack PRConfig head
       cd PRConfig
       ./scripts/AddToTestFilesDB.py

usage: AddToTestFilesDB.py [filename, [filename, ...]] [list_of_files.txt] [python_options.py] [--help]

any number of arguments>0 supported

See the Twiki: https://twiki.cern.ch/twiki/bin/view/LHCb/TestFileDB
"""

def usage():
    print __doc__

import sys, os

filestoadd=sys.argv[1:]
truefiles=[]

if not len(filestoadd) or "--help" in filestoadd or "-h" in filestoadd:
    usage()
    sys.exit(0)

try:
    import PRConfig
    from PRConfig import TestFileDB
except ImportError:
    usage()
    raise ImportError("Module PRConfig not found, you need to run this script inside an LHCb application environment, try adding --use PRConfig if all else fails. You also may need to source setup.sh from your checked-out PRConfig package.")


if not os.access(PRConfig.__file__, os.W_OK):
    usage()
    raise IOError("You do not have permission to edit the PRConfig database, probably you're not working with a local PRConfig version. The version you are picking up is in "+PRConfig.__file__)

#is it a gaudi options file? Then use IOHelper to parse the file names.
for filetoadd in filestoadd:
    import GaudiConf
    from GaudiConf import IOHelper
    if filetoadd.endswith(".qmt"):
        usage()
        raise IOError("Sorry, .qmt files are not yet supported by this script.")
    elif filetoadd.endswith(".opts"):
        usage()
        raise IOError("Sorry, old style .opts files are not really supported, but you could wrap them in the appropriate importOptions() from Gaudi.")
    elif filetoadd.endswith(".py"):
        try:
            execfile(filetoadd)
        except:
            usage()
            raise IOError("You asked to process the files in the Gaudi options file "+filetoadd+", but I was unable to exec this file, are you running this script in the correct environment to understand this file?")
        from Gaudi.Configuration import EventSelector
        eventSelector=EventSelector()
        for afile in eventSelector.Input:
            truefiles.append(IOHelper().undressFile(afile))
        IOHelper().inputFiles([],clear=True)
    elif filetoadd.endswith(".txt") or filetoadd.endswith(".lst"):
        #it's a list of file names
        f=open(filetoadd)
        if not f:
            usage()
            raise IOError("You asked to read a list of files from "+filetoadd+", but that file cannot be opened")
        for afile in f.readlines():
            truefiles.append(IOHelper().undressFile(afile.strip().strip(",")))
        f.close()
    else:
        #it's probably a file itself, or a list of files passed on the command line directly
        truefiles.append(IOHelper().undressFile(filetoadd))

if not len(truefiles):
    raise ValueError("No files supplied/found")

existing=[]
for key,val in TestFileDB.test_file_db.iteritems():
    existing.append(key)
    sfiles=[v.replace("PFN:","").split("?")[0].split('/lhcb/')[-1] for v in val.filenames]
    tfiles=[v.replace("PFN:","").split("?")[0].split('/lhcb/')[-1] for v in truefiles]
    for t in tfiles:
        if t in sfiles:
            raise ValueError("file looks very similar to something already in the database "+ t +" might be in "+key+"\n"+val.__str__())

#detect the file format automatically
from GaudiConf import IOExtension
aformat=IOExtension().detectMinType(truefiles)

def ext(f):
    #copied from IOExtension...
    return f.split('.')[-1].strip().split('?')[0].strip().upper()

if aformat=="ROOT":
    itype=ext(truefiles[0])
    for f in truefiles:
        atype=ext(f)
        if atype!=itype:
            print "Multiple extensions detected, you will need to specify the format"
            aformat=None
            break
    if aformat is not None:
        aformat=itype


def name():
    print "-> please choose a new name for the testfiles, here are some examples..."
    print existing[0:10]
    aname=raw_input("test name: ").strip()
    if aname in TestFileDB.test_file_db:
        print "already exists, choose again"
        return name()
    return aname

def format():
    global aformat
    if aformat is not None:
        print "-> ",aformat,"format auto-detected."
        return aformat
    print "-> Format not auto-detected. what format is this? e.g. MDF, DIGI, DST ..."
    aformat=raw_input("format: ").strip()
    if len(aformat)>5:
        print "format too long, try again"
        return format()
    if len(aformat)<3:
        print "format too short, try again"
        return format()
    return aformat

def datatype():
    aformat=raw_input("DataType: ").strip()
    if len(aformat)>8:
        print "datatype too long, try again"
        return datatype()
    if len(aformat)<3:
        print "datatype too short, try again"
        return datatype()
    return aformat

def info():
    print "-> you should now write an explanation of what the dataset is supposed to be for, why it was produced, etc."
    aninfo=raw_input("Information: ").strip()
    if len(aninfo)>300:
        print "explanation waaay too long, try again"
        return info()
    if len(aninfo)<10:
        print "explanation too short, try again"
        return info()
    return aninfo

def myzip(adict1,adict2):
    """
    zip together two distionaries, needed to gather together the user input
    """
    retdict={}
    #print adict1, adict2
    for adict in [adict1,adict2]:
        for k,v in adict.iteritems():
            retdict[k]=v
    #print retdict
    return retdict

def sim():
    Sim=raw_input("Simulation? (True/False) : ").strip()
    try:
        exec("Sim="+Sim)
        if type(Sim) is not type(True):
            print Sim, type(Sim)
            raise TypeError()
    except:
        print "failed to set type, retry please"
        return sim()
    return Sim

def dbtag():
    adbtag=raw_input("DDDB (at creation) : ").strip()
    if not adbtag.startswith("dddb-") and not adbtag.startswith('head-'):
        print "tag not recognized, add to db manually, or specify correct tag!"
        return dbtag()
    return adbtag
    
def condtag():
    adbtag=raw_input("CondDB (at creation) : ").strip()
    if not adbtag.startswith("cond-") and not adbtag.startswith('head-') and not adbtag.startswith("sim-"):
        print "tag not recognized, add to db manually, or specify correct tag!"
        return condtag()
    return adbtag
    
def extras():
    print "-> Please add the following information, if known/relevent.\n-> Whitespace or enter to skip."
    adict={}
    from PRConfig.TestFileObjects import testfiles
    for qual in testfiles.__known_qualifiers__:
        if qual in testfiles.__required_qualifiers__+["Author","Simulation","DDDB","CondDB"]:
            continue
        val=raw_input(qual+": ").strip()
        if len(val):
            try:
                exec("val="+val)
            except NameError:
                val=val
            adict[qual]=val
    #print adict
    return adict

import datetime

def maketestfile():
    """
    Obtain user input to create database entry
    """
    from PRConfig.TestFileObjects import testfiles
    from PRConfig.TestFileDB import test_file_db
    import os
    t=testfiles(filenames=truefiles,myname=name(),qualifiers=myzip(
        {"Format": format(),"DataType":datatype(),"Date": datetime.datetime.now().__str__(), "Author":os.environ["USER"], "Simulation": sim(), "DDDB": dbtag(), "CondDB":condtag()}, extras()),comment=info(),
              test_file_db=test_file_db)
    print t
    res=raw_input("is this OK? y/[n]").strip().lower()
    if res.startswith("y"):
        return t
    else:
        del test_file_db[t.myname]
        raise ValueError("you said this is not OK, exiting")

def pintodatabase(t):
    """
    Automatically write into database file
    """
    import PRConfig
    print t.recreateme()
    yn=raw_input("Pin to the database? y/[n]").strip().lower()
    if not yn.startswith("y"):
        return
    f=open("/".join(PRConfig.__file__.split('/')[0:-1]+["TestFileDB.py"]),'a')
    if not f:
        raise IOError("didn't find, or could not open the test database file, did you have the environment setup correctly?!")
    f.write("\n### auto created ###\n"+t.recreateme().replace("\t","    ")+"\n")
    f.close()
    print "Added to database, use with:"
    print t.useme()

t=maketestfile()
pintodatabase(t)
print "Done, if needed you can now migrate these files to CERN-SWTEST using $PRCONFIGROOT/scripts/MigrateToSWTEST.py"
