#!/usr/bin/env python
"""
Simple script to browse through the available test files using the file-wise browser provided.

setup: SetupProject any LHCb application, or SetupProject LHCb --use PRConfig
"""
try:
    import PRConfig
    from PRConfig import TestFileDB
except ImportError:
    usage()
    raise ImportError("Module PRConfig not found, you need to run this script inside an LHCb application environment, try adding --use PRConfig if all else fails. You also may need to source setup.sh from your checked-out PRConfig package.")


from PRConfig.TestFileUtils import browseTestFiles
mydir=''

#don't allow infinite loops
for i in range(1000):
    print "--------------------------------------"
    print "-->", mydir
    lowest=browseTestFiles(TestFileDB.test_file_db,mydir)
    next=raw_input("Choose <category>/all/any/.. [quit]:").strip().strip('/')
    if next=="..":
        mydir='/'.join(mydir.strip('/').split('/')[0:-1])
        continue
    elif next.lower()=="q" or next.lower()=="quit" or next.lower()=="exit" or not len(next):
        break
    elif next.strip('/').startswith(mydir.split('/')[0]):
        mydir=next
    elif lowest:
        print "already at the lowest level, please go up!"
    else:
        mydir='/'+mydir.strip('/')+'/'+next.strip('/')
