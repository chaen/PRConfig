#!/usr/bin/env python
import os
import sys
import re
import argparse
import pydim
import multiprocessing


def is_valid_dir(parser, arg):
    if arg == 'network':
        return arg
    if (arg is not None and (not arg or not os.path.exists(arg)
                             or not os.path.isdir(arg))):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg


parser = argparse.ArgumentParser(usage='usage: %(prog)s file')

parser.add_argument("user_package", nargs=1)
parser.add_argument("--numa", action='store_true', dest='numa', default=False,
                    help="Use NUMA binding to bind instances of all tasks "
                    "to a specific CPU and memory nodes.")
parser.add_argument("--dim-dns-node", type=str, dest="dim_dns_node",
                    default="localhost", help="Hostname or IP address "
                    "of the DIM dns node.")
parser.add_argument("--nodes", type=int, dest="nodes", default=-1,
                    choices=[-1] + range(1, 10), help="How many NUMA nodes to "
                    "run on, default is all.")
parser.add_argument("--log-file", type=str, dest="log",
                    default='MooreOnlineTest.log', help="Log file to use.")
parser.add_argument("--viewers", action="store_true", dest="viewers",
                    help="Run the LogViewer and buffer manager monitor.")
parser.add_argument("--ncpu-scale-factor", type=float,
                    dest="ncpu_scale_factor", default=1.0,
                    help="Scale the number of available cores by this factor "
                    "to obtain the number of Moore processes to run.")
parser.add_argument("--warmup", type=int, dest="warmup", default=60,
                    help="Process events for X seconds before starting "
                    "to count.")
parser.add_argument("--runtime", type=int, dest="runtime", default=300,
                    help="Count for X seconds.")
parser.add_argument("--slaves", type=int, dest="slaves", default=-1,
                    help="Number of Moore processes to run.")
parser.add_argument("--input", type=lambda x: is_valid_dir(parser, x),
                    dest="input", default='/localdisk/hlt1/test')
parser.add_argument("--output", type=lambda x: is_valid_dir(parser, x),
                    dest="output", default=None)
parser.add_argument("--moore-output-level", type=int, dest="output_level",
                    default=3, choices=range(6))
parser.add_argument("--moore-log", dest="moore_log", type=str,
                    default="test/moore.log", help="Write Moore tasks "
                    "output to log file.")
parser.add_argument("--mode", dest="mode", type=str, default="time",
                    choices=["time", "all_data"],
                    help="Write Moore tasks output to log file.")
parser.add_argument("-w", "--timing-width", type=int, default=60,
                    dest="timing_width",
                    help="Width of the name column in the timing table")
parser.add_argument("-p", "--partition", type=str, default="TEST",
                    dest="partition", help="Which partition?")
parser.add_argument("--preload", type=str, default="",
                    dest="preload", help="set LD_PRELOAD to this")
parser.add_argument("--streams", action='store_true', default=False,
                    dest="streams", help="Use output streams.")

args = parser.parse_args()

os.environ["DIM_DNS_NODE"] = args.dim_dns_node
if not pydim.dis_get_dns_node():
    print "No Dim DNS node found, problem with DIM_DNS_NODE??"
    sys.exit(1)

# if the default fifo isn't there, stuff doesn't work. Create it in that case
# and make sure others can write to it too.
fifo = "/tmp/logSrv.fifo"
if not os.path.exists(fifo):
    import stat
    os.mkfifo(fifo)
    os.chmod(fifo, stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP |
             stat.S_IWGRP | stat.S_IROTH | stat.S_IWOTH)

# Write log in directory
if os.path.sep in args.moore_log:
    log_dir, f = args.moore_log.rsplit(os.path.sep, 1)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_file = os.path.join(log_dir, args.log)
else:
    log_dir = '.'
    log_file = args.log

# logging
from OfflineOnline.TestUtils import *
logger = setup_loggin(__name__, log_file)

results = None

input_dir = args.input
if input_dir != 'network':
    files = partition(os.listdir(input_dir), mdf_mep)
    # Check that there are either mdf or mep files, not both
    if files['REST'] or (not bool(len(files['MDF'])) ^ bool(len(files['MEP']))):
        print 'The input directory must contain either only MDF or only MEP files, not a mix.'
        print 'And no other files.'
        sys.exit(-1)

    input_type = 'MDF' if files.get('MDF', []) else 'MEP'
    prefixes = set(fn[: 4] for fn in files[input_type])
    if len(prefixes) != 1:
        print 'All files in the input directory must have filenames with the same first 4 characters.'
        sys.exit(-1)
else:
    input_type = 'MDF'
    prefixes = ['MDF']

# Number of workers and number of slaves
n_nodes = args.nodes
n_slaves = args.slaves
scale_factor = args.ncpu_scale_factor

mode = args.mode
if mode == 'all_data':
    from OfflineOnline.Processes import get_tmpdir
    tmpdir = get_tmpdir()
    dest_dir = os.path.join(tmpdir, 'data_dir')
    if not os.path.exists(dest_dir):
        os.mkdir(dest_dir)
    for f in sorted(os.listdir(input_dir)):
        source = os.path.join(input_dir, f)
        dest = os.path.join(dest_dir, f)
        os.symlink(source, dest)
    input_dir = dest_dir

# Instantiate the benchmark processes.
from MooreTests.Benchmark import MooreBenchmark
bench_args = dict(output_level=args.output_level,
                  user_package=args.user_package[0], viewers=args.viewers,
                  n_slaves=n_slaves, input_directory=input_dir,
                  output_directory=args.output,
                  write_mdf=(args.output is not None),
                  n_nodes=n_nodes, scale_factor=scale_factor, mode=mode,
                  input_type=input_type, log_file=args.moore_log,
                  file_prefix=list(prefixes)[0],
                  logger=logger, numa=args.numa, partition=args.partition,
                  timing_width=args.timing_width, preload=args.preload,
                  streams=args.streams)

av_inst = 0.
av_inst_s = 0.

# For jemalloc profiling
os.environ['MALLOC_CONF'] = "prof:true,prof_leak:true"

# Run benchmark
with MooreBenchmark(**bench_args) as benchmark:
    benchmark.start()

    import time
    import datetime
    if mode == "time" and args.warmup:
        logger.info("Letting benchmark warm up for %d seconds." % args.warmup)
        logger.info("Current time " + datetime.datetime.now().ctime())
        logger.info("Warmup finishes at " + (datetime.datetime.now() +
                                             datetime.timedelta(0, args.warmup)).ctime())
        time.sleep(args.warmup)

    if mode == 'time':
        logger.info("Starting analysis, running for %d seconds." %
                    args.runtime)
        logger.info("Current time " + datetime.datetime.now().ctime())
        logger.info("Analysis finishes at " + (datetime.datetime.now() +
                                               datetime.timedelta(0, args.runtime)).ctime())

    benchmark.run()

    if mode == "time":
        time.sleep(args.runtime)
        logger.info("Stop counting.")
        benchmark.stop()
    else:
        benchmark.wait()

    results = benchmark.results()

    inst_rates = results['Moore'][0]['inst_rates']
    if mode != 'time':
        inst_rates = inst_rates[:-1]

    # Calculate the mean instantaneous rate for normalisation
    from math import sqrt
    av_inst = sum(inst_rates) / len(inst_rates)
    av_inst2 = sum((r ** 2 for r in inst_rates)) / len(inst_rates)
    av_inst_s = sqrt(av_inst2 - av_inst ** 2)
    logger.info('Average instantaneous rate: %8.3f +- %8.3f.\n' %
                (av_inst, av_inst_s))

    import shelve
    db = shelve.open(os.path.join(log_dir, 'test_results.db'))
    db['results'] = results
    db.close()
    del db

moore_log = os.path.basename(args.moore_log)
if '.' in moore_log:
    log_base = moore_log.rsplit('.', 1)[0]
else:
    log_base = moore_log


# Write average log file
from MooreTests.TestUtils import *
n_nodes, n_slaves = n_processes(args.numa, n_nodes, n_slaves, scale_factor)


def filter_logs(log_file):
    if not hasattr(filter_logs, 'regex'):
        filter_logs.regex = re.compile(r'^%s_(\d(\d+)).*' % log_base)
    r = filter_logs.regex.match(log_file)
    return int(r.group(2)) if r else r

log_files = [os.path.join(log_dir, lf)
             for lf in os.listdir(log_dir) if filter_logs(lf)]
log_files = sorted(log_files)
average_times = get_tpes(log_files, av_inst, multiprocessing.cpu_count(), n_nodes * n_slaves,
                         args.timing_width)

with open(os.path.join(log_dir, 'averages.log'), 'w') as average_log:
    average_log.write('Average rate: %f +- %f\n' % (av_inst, av_inst_s))
    max_name_len = max(len(k[0]) for k in average_times.iterkeys())
    max_indent = max(k[2] for k in average_times.iterkeys())
    lines = sorted(average_times.iteritems(), key=lambda item: item[0][1])
    for (name, n, indent), values in lines:
        av_time = sum(values) / len(values)
        av_time2 = sum((r ** 2 for r in values)) / len(values)
        av_time_s = sqrt(av_time2 - av_time ** 2)
        pat = "{0:<" + str(max_name_len + max_indent - indent) + "s}"
        line = (' ' * indent) + pat.format(name) + \
            ("  %8.3f +- %5.3f\n" % (av_time, av_time_s))
        average_log.write(line)

output_dict = {}

output_dict['scale_factor'] = scale_factor
output_dict['numa'] = args.numa
output_dict['n_nodes'] = n_nodes
output_dict['n_slaves'] = n_slaves
output_dict['av_inst'] = av_inst
output_dict['av_inst_s'] = av_inst_s
output_dict['inst_rates'] = inst_rates
output_dict['cpu_count'] = multiprocessing.cpu_count()
output_dict['results'] = dict(results)

import json
with open(os.path.join(log_dir, 'output.json'), 'w') as _file:
    json.dump(output_dict, _file)

import os
for i in os.listdir('/dev/shm'):
    try:
        os.remove(os.path.join('/dev/shm', i))
    except OSError:
        pass
os.remove('/tmp/logSrv.fifo')
