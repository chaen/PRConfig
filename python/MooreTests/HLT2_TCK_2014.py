
MooreOptions = dict(Simulation = False,
                    DataType = "2012",
                    DDDBtag = "dddb-20120831",
                    CondDBtag = "cond-20120831",
                    RemoveInputHltRawBanks = True,
                    UseTCK = True,
                    InitialTCK = "0x0a030046",
                    HltLevel = 'Hlt1Hlt2')

def configure():
    pass
