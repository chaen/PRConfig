import sys
try:
    import AllHlt1
except ImportError:
    rd = '/group/online/hlt/conditions/RunChangeHandler'
    sys.path.append(rd)
    import AllHlt1
settings = "Physics_pp_Draft2016"

MooreOptions = dict(Simulation = False,
                    DataType = "2015",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20160517",
                    UseTCK = False,
                    RemoveInputHltRawBanks = True,
                    EnableTimer = True,
                    ForceSingleL0Configuration = False,
                    ThresholdSettings = settings,
                    TimingTest = True,
                    HltLevel = 'Hlt1',
                    Split = 'Hlt1')

def configure():
    from Configurables import HltConf
    HltConf().L0TCK = '0x1600'

    from Gaudi.Configuration import appendPostConfigAction
    appendPostConfigAction(getHlt1SelectionIDs )
## DDDBtag = "dddb-20120831",
## CondDBtag = "cond-20120831",
