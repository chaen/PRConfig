settings = "Physics_pp_2017"
MooreOptions = dict(Simulation = False,
                    UseTCK = False,
                    ThresholdSettings = settings,
                    DataType = "2017",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20170510",
                    RemoveInputHltRawBanks = True,
                    EnableTimer = True,
                    ForceSingleL0Configuration = False,
                    TimingTest = True,
                    HltLevel = 'Hlt1',
                    Split = 'Hlt1')

from Configurables import HltConf
HltConf().L0TCK = '0x1705'
from ConfigureJobs import configureOnlineConditions, removeHlt1Technical, getHlt1SelectionIDs
removeHlt1Technical()
from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(lambda: getHlt1SelectionIDs(settings))


