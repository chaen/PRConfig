import sys
try:
    import All
except ImportError:
    rd = '/group/online/hlt/conditions/RunChangeHandler'
    sys.path.append(rd)
    import All

settings = "Physics_pp_Draft2016"
MooreOptions = dict(Simulation = False,
                    DataType = "2015",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20150828",
                    UseTCK = False,
                    RemoveInputHltRawBanks = False,
                    EnableTimer = True,
                    CheckOdin = False,
                    ForceSingleL0Configuration = False,
                    ThresholdSettings = settings,
                    TimingTest = True,
                    HltLevel = 'Hlt2',
                    Split = 'Hlt2')

def configure():
    from Configurables import UpdateAndReset
    UpdateAndReset().abortRetroEvents = False
    from Configurables import CondDB
    conddb = CondDB()
    conddb.EnableRunStampCheck = False
    conddb.RunChangeHandlerConditions = All.ConditionMap,
    from Configurables import HltConf
    HltConf().L0TCK = '0xFF61'
    from Gaudi.Configuration import appendPostConfigAction
    def setHlt1SelectionIDs():
        from Configurables import HltANNSvc
        import pickle
        configuration = pickle.load(open("hlt1SelectionIDs_%s.p" % settings))
        Hlt1SelectionIDs = configuration[settings]
        HltANNSvc().Hlt1SelectionID = Hlt1SelectionIDs
    appendPostConfigAction( setHlt1SelectionIDs )

## DDDBtag = "dddb-20120831",
## CondDBtag = "cond-20120831",
