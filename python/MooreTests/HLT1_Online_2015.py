import sys
try:
    import AllHlt1
except ImportError:
    rd = '/group/online/hlt/conditions/RunChangeHandler'
    sys.path.append(rd)
    import AllHlt1

MooreOptions = dict(Simulation = False,
                    DataType = "2015",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20150828",
                    UseTCK = False,
                    RemoveInputHltRawBanks = True,
                    EnableTimer = True,
                    UseDBSnapshot = True,
                    ForceSingleL0Configuration = False,
                    ThresholdSettings = "Hlt1_TrackingOnly",
                    RunChangeHandlerConditions = AllHlt1.ConditionMap,
                    HltLevel = 'Hlt1')

def configure():
    pass

## DDDBtag = "dddb-20120831",
## CondDBtag = "cond-20120831",
