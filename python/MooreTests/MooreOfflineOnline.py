import os
import pydim
import time
import psutil
from operator import itemgetter
from collections import defaultdict
from OfflineOnline.Processes import GaudiTestProcess, State


class AdderRateCounter(object):

    def __init__(self, parent, logger, mothers, partition):
        self.__dim_svc = []
        self.__parent = parent
        self.__logger = logger
        self.__partition = partition
        self.__mothers = {pid: psutil.Process(pid) for pid in mothers}

        # Stuff for synchronisation
        from multiprocessing import Lock
        self._stopping = False
        self.__stop_counting = False
        self.__count = False
        self.__counter_lock = Lock()

        # Info will contain for each slave a list of entries, which are tuples
        # of (call_number, time, total)
        from collections import defaultdict
        self.__info = []

        # Results
        self.__run_time = 0
        self.__total_events = 0
        self.__inst_rates = []
        self.__time_stamps = []
        self.__mem_info = defaultdict(list)

    def run(self):
        self.__count = True

    def stop(self):
        self._stopping = True

    def stop_counting(self):
        # First stop counting
        self.__stop_counting = True

    def logger(self):
        return self.__logger

    def results(self):
        return {'run_time': self.__run_time,
                'total_events': self.__total_events,
                'inst_rates': self.__inst_rates,
                'mem_info': dict(self.__mem_info),
                'time_stamps': self.__time_stamps}

    def subscribe(self):
        svc = "stat/%s_x_Runable/EvtCount/Count" % self.__partition
        self.logger().debug("Subscribing to: %s" % svc)
        self.__dim_svc = pydim.dic_info_service(
            svc, "i", self.__counter_callback, pydim.MONITORED,
            0, 0, None)

    def __counter_callback(self, tag, ev_val):
        self.__counter_lock.acquire()
        if not self.__count and not self._stopping:
            self.__counter_lock.release()
            return

        if ev_val is None:
            self.__counter_lock.release()
            return

        now = time.time()

        # Add info to the stack for this call
        self.__info.append((now, ev_val))

        if len(self.__info) < 2:
            self.__counter_lock.release()
            return

        # We now have at least two entries, so we can calculate the
        # average rate.  The Moore children already give the
        # integrated number of events.
        time_diff = 0
        good_rate = True

        time_diff = self.__info[-1][0] - self.__info[-2][0]
        ev_diff = self.__info[-1][1] - self.__info[-2][1]
        if ev_diff < 0:
            good_rate = False

        inst_rate = ev_diff / time_diff

        # If we're stopping, the only thing that matters is total difference in
        # processed events
        # self.logger().debug("done? %d %d" % (self._stopping, total_ev_diff))
        if self._stopping:
            if ev_diff == 0:
                self.__parent._notify()
            self.__counter_lock.release()
            return

        # We're counting, calculate the instantaneous rate.
        if good_rate:
            self.__total_events += ev_diff
            self.__run_time += time_diff
            self.logger().info("instantaneous rate = %6.2f" % inst_rate)
            self.__inst_rates.append(inst_rate)

        # remove bottom entry from each info stack
        self.__info.pop(0)

        # Append time stamp
        self.__time_stamps.append(now)

        # Set last call 1 and wipe out info, this also works if rate counters
        # are reset
        if self.__stop_counting:
            if self.__inst_rates:
                # If at least one instantaneous rate was calculated, we can
                # stop safely
                self.logger().debug("Final rate calculated.")
                self.__count = False
                self.__parent._notify()

        # self.logger().debug("Reached end.")
        self.__counter_lock.release()
        return


class Moore(GaudiTestProcess):

    def __init__(self, logger, launcher, **kwargs):
        GaudiTestProcess.__init__(
            self, logger, launcher, kwargs.pop('priority'))
        self.__n_nodes = kwargs.pop('n_nodes')
        self.__n_slaves = kwargs.pop('n_slaves')
        self.__user_package = kwargs.pop('user_package')
        self.__output_level = kwargs.pop('output_level', 4)
        self.__input_type = kwargs.pop('input_type', 'MEP')
        self.__streams = kwargs.pop('streams', False)
        self.__preload = [os.path.expandvars(
            '${CHECKPOINTINGROOT}/../../InstallArea/${CMTCONFIG}/lib/libCheckpointing.so')]
        if 'preload' in kwargs:
            self.__preload = [kwargs.pop('preload')] + self.__preload
        self.__processes = []
        self.__dim_svc = []

        self.__workers = {}
        from itertools import product
        for n, i in product(range(self.__n_nodes), range(self.__n_slaves)):
            gid = self.utgid("Moore1_%d%02d" % (n, i + 1))
            self.__workers[n * self.__n_slaves + i] = gid
        self.__mothers = {len(self.__workers) + n: self.utgid("Moore1_%d00" % n)
                          for n in range(self.__n_nodes)}

        mt = __import__(
            "MooreTests", globals(), locals(), [self.__user_package])
        up = getattr(mt, self.__user_package)
        self.__hlt_level = up.MooreOptions['HltLevel']

        # Stuff for synchronisation
        from multiprocessing import Lock
        self.__stop_counting = False
        self.__count = False
        self.__last_call = None
        self.__callback_lock = Lock()
        self.__counter_lock = Lock()

        # Info will contain for each slave a list of entries, which are tuples
        # of (call_number, time, total)
        self.__info = defaultdict(list)

        # Results
        self.__run_time = 0
        self.__total_events = 0
        self.__inst_rates = []
        self.__time_stamps = []
        self.__mem_info = defaultdict(list)

    def initialize(self):
        from copy import deepcopy
        for processes in (self.__workers, self.__mothers):
            for i, utgid in processes.iteritems():
                svc = utgid + '/status'
                self.logger().debug("Subscribing to %s service." % svc)
                self.__dim_svc.append(
                    pydim.dic_info_service(svc, "C", self.__callback,
                                           pydim.MONITORED, 0, i, None))
        cmd = ("import MooreScripts.runOfflineOnline; " +
               "MooreScripts.runOfflineOnline.configure(NbOfSlaves={0}, " +
               "UserPackage='{1}', PartitionName = '{2}', OutputLevel={3}, " +
               "InputType='{4}', EnableOutputStreaming={5})")
        cmd = cmd.format(self.__n_slaves, self.__user_package,
                         self.partition(), self.__output_level,
                         self.__input_type, self.__streams)

        python_path = os.environ['PYTHONPATH']
        online_env = self.online_env()
        env = {'LC_ALL': 'C', 'MBM_SETUP_OPTIONS': self.mbm_opts(),
               'INFO_OPTIONS': self.info_opts(), 'LOGFIFO': self.log_fifo(),
               'LD_PRELOAD': ' '.join(self.__preload),
               'PYTHONPATH': os.path.dirname(online_env) + ":" + python_path}
        command = ['${GAUDIONLINEROOT}/../../InstallArea/${CMTCONFIG}/bin/GaudiCheckpoint.exe',
                   '${GAUDIONLINEROOT}/../../InstallArea/${CMTCONFIG}/lib/libGaudiOnline.so',
                   'OnlineTask', '-tasktype=LHCb::Class1Task', '-msgsvc=LHCb::FmcMessageSvc',
                   '-opt=command="%s"' % cmd, '-main=%s' % self.main_opts(),
                   '-auto']
        self.condition().acquire()
        mothers = []
        for n in range(self.__n_nodes):
            utgid = self.utgid("Moore1_%d00" % n)
            e = deepcopy(env)
            e.update({'UTGID': utgid, 'TASK_TYPE': 'Moore1'})
            p = self.launch(utgid, command, e, node=n)
            mothers += [p]

        self.__counter = AdderRateCounter(self, self.logger(),
                                          [proc.pid for proc in mothers],
                                          self.partition())

        n_running = 0
        while n_running < len(self.__workers):
            self.condition().wait()
            n_running += 1
            self.logger().debug("%d Moore children running out of %d" %
                                (n_running, len(self.__workers)))
        self.logger().info("All Moore children running.")
        self.condition().release()
        self.set_state(State.RUNNING)
        self.__subscribe()

    def run(self):
        self.__counter.run()

    def stop_counting(self):
        # First stop counting
        self.condition().acquire()
        self.__counter.stop_counting()
        self.condition().wait()
        self.condition().release()

    def stop(self):
        # Then stop process
        self.condition().acquire()
        self._stopping = True
        self.__counter.stop()
        # And wait until buffers are empty
        self.condition().wait()
        self.condition().release()
        # Send stop to all moore tasks and wait for them to be ready.
        workers = [e[1] for e in sorted(
            self.__workers.items(), key=itemgetter(0), reverse=True)]
        for utgid in workers:
            self.__send_command(utgid, "stop")
        for utgid in self.__mothers.itervalues():
            self.__send_command(utgid, "stop")
        self.set_state(State.READY)

    def finalize(self):
        if not self._stopping:
            return
        # Send reset to all moore tasks and wait for them to finalize.
        workers = [e[1] for e in sorted(
            self.__workers.items(), key=itemgetter(0), reverse=True)]
        for utgid in workers:
            self.__send_command(utgid, "reset")
        for utgid in self.__mothers.itervalues():
            self.__send_command(utgid, "reset")
        self.set_state(State.NOT_READY)
        # Disconnect from DIM services
        for svc in self.__dim_svc:
            pydim.dic_release_service(svc)
        self.__dim_svc = []

    def terminate(self):
        from psutil import NoSuchProcess

        # Kill workers
        workers = [e[1] for e in sorted(
            self.__workers.items(), key=itemgetter(0), reverse=True)]
        for utgid in workers:
            self.__send_command(utgid, "unload", False)
        for utgid in self.__mothers.itervalues():
            # Kill master
            self.__send_command(utgid, "unload", False)

        for p in self.__processes:
            if p is None:
                continue
            master = psutil.Process(p.pid)
            try:
                workers = master.children()
            except AttributeError:
                workers = master.get_children()
            except NoSuchProcess:
                continue
            for worker in workers:
                if not worker.is_running():
                    continue
                try:
                    worker.kill()
                except NoSuchProcess:
                    continue
            self._terminate(p)
            # subprocess.Popen(['pkill', '-9', 'GaudiCheckpoint'])

    def results(self):
        return self.__counter.results()

    def launch(self, utgid, command, env, do_exec=True, node=0):
        p = self.launcher().launch(utgid, command, env, do_exec, node)
        self.__processes.append(p)
        return p

    def __send_command(self, utgid, command, wait=True):
        if wait:
            self.condition().acquire()
            pydim.dic_cmnd_service(utgid, (command,), "C")
            self.condition().wait()
            self.condition().release()
        else:
            pydim.dic_cmnd_service(utgid, (command,), "C")

    def __callback(self, tag, val):
        if not val:
            return
        self.__callback_lock.acquire()

        def __msg(tag, state):
            utgid = self.__workers[
                tag] if tag in self.__workers else self.__mothers[tag]
            return "%s went to %s." % (utgid, state)
        try:
            if (self.state() == State.UNKNOWN
                    and val.strip().startswith("RUNNING")):
                self.logger().info(__msg(tag, "RUNNING"))
                self._notify()
            elif (self.stopping() and self.state() == State.RUNNING
                  and val.strip().startswith("READY")):
                self.logger().info(__msg(tag, "READY"))
                self._notify()
            elif (self.stopping() and self.state() == State.READY
                  and val.strip().startswith("NOT_READY")):
                self.logger().info(__msg(tag, "NOT_READY"))
                self._notify()
            else:
                self.logger().debug(__msg(tag, val.strip()))
        except Exception:
            utgid = self.__workers[
                tag] if tag in self.__workers else self.__mothers[tag]
            self.logger().debug("exception while waiting for %s to start up." %
                                utgid, exc_info=True)
        self.__callback_lock.release()

    def __subscribe(self):
        self.__counter.subscribe()
