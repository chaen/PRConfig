#!/usr/bin/env python
import os
# os.system(
#     "python $PRCONFIGROOT/python/MooreTests/Moore_RateTest.py"
#     " --settings Physics_pp_Tight2018"
#     " --L0TCK=\"0x1707\""
#     " --inputdata=\"2017NB_L0Filt0x1707\""
# )
os.system(
    "python $PRCONFIGROOT/python/MooreTests/RunL0App.py"
    " -n 30000"
    " --replay"
    " --TCK 0x1801"
    " --inputdata 2017NB_L0Filt0x1707"
    " --outputfile 2017NB_L0Filt0x1707_0x1801.dst"
)
os.system(
    "python $PRCONFIGROOT/python/MooreTests/Moore_RateTest.py"
    " -n 1000000000"
    " --settings Physics_pp_Tight2018"
    " --L0TCK 0x1801"
    " --inputdata 2017NB_L0Filt0x1707_0x1801.dst"
    " --TFDBForTags 2017NB_L0Filt0x1707"
)
