import sys
try:
    import AllHlt1
except ImportError:
    rd = '/group/online/hlt/conditions/RunChangeHandler'
    sys.path.append(rd)
    import AllHlt1

MooreOptions = dict(Simulation = False,
                    DataType = "2015",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20150828",
                    UseTCK = False,
                    RemoveInputHltRawBanks = True,
                    EnableTimer = True,
                    ForceSingleL0Configuration = False,
                    ThresholdSettings = "Physics_pp_Draft2016",
                    RunChangeHandlerConditions = AllHlt1.ConditionMap,
                    HltLevel = 'Hlt1Hlt2'
                    )

def configure():
    from Configurables import UpdateAndReset
    UpdateAndReset().abortRetroEvents = False
    from Configurables import HltConf
    HltConf().L0TCK = '0x0050'
    pass

## DDDBtag = "dddb-20120831",
## CondDBtag = "cond-20120831",
