#!/usr/bin/env python
import os
os.system(
    "python $PRCONFIGROOT/python/MooreTests/RunL0App.py"
    " -n 10000"
    " --replay"
    " --TCK 0x1724"
    " --inputdata pNe2015NB_BE"
    " --outputfile pNe2015NB_L0Filt0x1724_BE.dst"
)
os.system(
    "python $PRCONFIGROOT/python/MooreTests/Moore_RateTest.py"
    " -n 1000000000"
    " --settings Physics_5tev2017"
    " --L0TCK 0x1724"
    " --input_rate 500e3"
    " --inputdata pNe2015NB_L0Filt0x1724_BE.dst"
    " --TFDBForTags pNe2015NB_BE"
)
