from OfflineOnline.Processes import (Launcher, NumaLauncher, MBMMon,
                                     LogViewer, MDFWriter, LogServer,
                                     MBM, Adder, MEPRx, MEPReader)

from OfflineOnline.BenchmarkProcess import BenchmarkProcessBase


class MooreBenchmarkProcess(BenchmarkProcessBase):

    def __init__(self, logger, components):
        BenchmarkProcessBase.__init__(self, logger, self.run, can_wait=True)

        self.__components = components
        self.__processes = []
        for process, kwargs in components:
            launcher = kwargs.pop('launcher')
            p = process(logger, launcher, **kwargs)
            self.__processes.append(p)

    def process_id(self):
        return "Moore"

    def initialize(self):
        for p in self.processes():
            p.initialize()
        self.put_status("initialized")

    def processes(self, reverse=False):
        return sorted(self.__processes, key=lambda p: p.priority(),
                      reverse=reverse)

    def run(self):
        # old_stdout = os.dup( sys.stdout.fileno() )
        # fd = os.open( '%s.log' % self._name, os.O_CREAT | os.O_WRONLY )
        # os.dup2( fd, sys.stdout.fileno() )
        self.initialize()
        while True:
            command = self.get_command()
            if command == "run":
                for p in self.processes():
                    p.run()
            elif command in ["wait", "stop"]:
                if command == "wait":
                    reader = max(self.processes(), key=lambda p: p.priority())
                    reader.wait()
                for p in self.processes():
                    p.stop_counting()
                for p in self.processes(True):
                    p.stop()
                for p in self.processes(True):
                    r = p.results()
                    if r is not None:
                        self.put_status(r)
                for p in sorted(self.__processes, key=lambda p: p.priority(),
                                reverse=True):
                    p.finalize()
                self.logger().info("%s is done" % self.process_id())
                self.put_status("done")
                self.close_status()
                break
            else:
                self.logger().error("Bad command from queue: %s" % command)

        self.terminate()
        # os.close( fd )
        # os.dup2( old_stdout, sys.stdout.fileno() )

    def terminate(self):
        for p in self.processes(True):
            p.terminate()


class MooreBenchmark(object):

    def __init__(self, **kwargs):
        output_level = kwargs.pop('output_level', 4)
        user_package = kwargs.pop('user_package')
        viewers = kwargs.pop('viewers', False)
        n_slaves = kwargs.pop('n_slaves')
        input_directory = kwargs.pop('input_directory')
        output_directory = kwargs.pop('output_directory', None)
        write_mdf = kwargs.pop('write_mdf', False)
        n_nodes = kwargs.pop('n_nodes')
        scale_factor = kwargs.pop('scale_factor')
        partition = kwargs.pop('partition')
        do_numa = kwargs.pop('numa')
        log_file = kwargs.pop('log_file')
        input_type = kwargs.pop('input_type', 'MEP')
        file_prefix = kwargs.pop('file_prefix', 'Run_')
        timing_width = kwargs.pop('timing_width', 60)
        partition_id = kwargs.pop('partition_id', hash(partition) % 0xfff)
        preload = kwargs.pop('preload', None)
        mode = kwargs.pop('mode')
        delete_files = (mode == 'all_data')
        streams = kwargs.pop('streams', False)

        self.__logger = kwargs.pop('logger')

        from collections import defaultdict
        self.__results = defaultdict(list)

        if do_numa:
            MooreLauncher = NumaLauncher
        else:
            MooreLauncher = Launcher
        from TestUtils import n_processes
        n_nodes, n_sl = n_processes(do_numa, n_nodes, n_slaves, scale_factor)

        from MooreTests.MooreOfflineOnline import Moore
        self.__logger.info("Will start %d Moore instances per node." % n_sl)
        launcher = Launcher(partition, self.__logger, partition_id)
        moore_launcher = MooreLauncher(partition, self.__logger, partition_id)
        self.__components = [(LogServer, dict(launcher=launcher, priority=1)),
                             (MBM, dict(launcher=launcher, priority=3)),
                             (Moore, dict(n_nodes=n_nodes, n_slaves=n_sl,
                                          launcher=moore_launcher,
                                          user_package=user_package,
                                          output_level=output_level,
                                          priority=6, input_type=input_type,
                                          preload=preload, streams=streams)),
                             (Adder, dict(launcher=launcher, priority=7))]

        if input_directory == 'network':
            self.__components += [(MEPRx, dict(priority=8, launcher=launcher))]
        else:
            self.__components += [(MEPReader, dict(directory=input_directory,
                                                   file_prefix=file_prefix,
                                                   delete_files=delete_files,
                                                   priority=8,
                                                   launcher=launcher))]

        if log_file:
            self.__components.append((LogViewer,
                                      dict(launcher=launcher, priority=2,
                                           output_file=log_file,
                                           split_regex=r'Moore1_(\d+)',
                                           timing_width=timing_width)))
        if viewers:
            self.__components.append(
                (MBMMon, dict(launcher=launcher, priority=4)))
            self.__components.append(
                (LogViewer, dict(launcher=launcher, priority=2)))
        if write_mdf:
            moore_tests = __import__(
                "MooreTests", globals(), locals(), [user_package])
            up = getattr(moore_tests, user_package)
            split = up.MooreOptions.get('HltLevel', 'Hlt1Hlt2')
            if not streams or split == 'Hlt1':
                # One file for everything
                out_streams = {
                    'All': ['0xffffffff', '0xffffffff', '0xffffffff',
                            '0xffffffff']}
            else:
                out_streams = {}
                from Configurables import HltOutputConf
                known_streams = HltOutputConf.__streams__
                # This one is not an output stream
                known_streams.pop('VELOCLOSING')
                for stream, (bit, on) in known_streams.iteritems():
                    # Build the trigger mask that selects only events from this
                    # particular stream.
                    if not on:
                        continue
                    mask = ['0x0'] * 4
                    mask[bit / 32] = ('0x%08x' % (1 << (bit % 32)))
                    out_streams[stream] = mask

            self.__components.append((MDFWriter,
                                      dict(launcher=launcher, priority=5,
                                           output_directory=output_directory,
                                           streams=out_streams)))

        self.__processes = [MooreBenchmarkProcess(
            self.__logger, self.__components)]

        # To get an idea of overall CPU usage
        from OfflineOnline.BenchmarkProcess import CPUMeasurer
        self.__processes.append(CPUMeasurer(self.__logger))

    def start(self):
        for bp in self.__processes:
            bp.start()

        for bp in self.__processes:
            status = bp.get_status()
            if status != 'initialized':
                msg = ("Got unexpected statusfrom benchmark "
                       "processes %s" % status)
                self.__logger.warning(msg)

    def run(self):
        for bp in self.__processes:
            bp.put_command("run")

    def stop(self):
        for bp in self.__processes:
            bp.put_command("stop")
        self.__get_results(self.__processes)
        self.__join()

    def wait(self):
        # Wait for those we can wait for
        wait_for = [p for p in self.__processes if p.can_wait()]
        for p in wait_for:
            p.put_command("wait")
        self.__get_results(wait_for)

        # And then we stop the rest
        others = [p for p in self.__processes if not p.can_wait()]
        for p in others:
            p.put_command("stop")
        self.__get_results(others)

        # We're done
        self.__join()

    def results(self):
        return self.__results

    def __get_results(self, processes):
        from exceptions import EOFError
        for bp in processes:
            try:
                while True:
                    r = bp.get_status()
                    if r == "done":
                        break
                    else:
                        self.__results[bp.process_id()].append(r)
            except EOFError:
                pass

    def __join(self):
        for bp in self.__processes:
            bp.join()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        for bp in self.__processes:
            bp.terminate()
