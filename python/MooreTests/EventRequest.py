from OnlineEnv import *
import OnlineConfig

pid = PartitionID
pnam = PartitionName
print_config = False

#pnam = 'MSF'
#pid  = 0

def _run(app): return (app,end_config(False))

def runBuffer(buffer='Events', partitionBuffers=True):
  return _run(mbmInitApp(pid,pnam,flags='-s=8096 -e=64 -u=64 -i='+buffer+' -c',partitionBuffers=partitionBuffers))

def runNetCons(source,load=1,print_freq=0.999005):
  mepMgr  = mepManager(pid,pnam,['Events'],partitionBuffers=True)
  runable = evtRunable(mepMgr)
  evtSel  = netSelector(source,'ALL',2)
  #evtSel.OutputLevel   = 2
  #evtSel.PrintFreq     = 1
  evtSel.CancelOnDeath = True
  evtdata = evtDataSvc()
  evtPers = rawPersistencySvc()

  algs = []
  ##algs.append(storeExplorer(load=load,freq=print_freq))
  algs.append(evtMerger(buffer='Events',name='Writer',location='/Event/DAQ/RawEvent',routing=0x1,datatype=MDF_NONE))
  app = OnlineConfig._application('NONE',extsvc=[monSvc(),mepMgr,evtSel],runable=runable,algs=algs)
  return _run(app)
