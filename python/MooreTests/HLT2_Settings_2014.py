MooreOptions = dict(Simulation = True,
                    DataType = "2012",
                    DDDBtag = "dddb-20140729",
                    CondDBtag = "sim-20140730-vc-mu100",
                    UseTCK = False,
                    RemoveInputHltRawBanks = False,
                    EnableTimer = True,
                    ThresholdSettings = "Physics_December2014",
                    HltLevel = 'Hlt1Hlt2')

def configure():
    pass

## DDDBtag = "dddb-20120831",
## CondDBtag = "cond-20120831",
