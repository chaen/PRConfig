MooreOptions = dict(Simulation = True,
                    DataType = "2012",
                    DDDBtag = "dddb-20140729",
                    CondDBtag = "sim-20140730-vc-md100",
                    UseTCK = False,
                    RemoveInputHltRawBanks = False,
                    EnableTimer = True,
                    ThresholdSettings = "Hlt1_TrackingOnly",
                    HltLevel = 'Hlt1')

def configure():
    pass
