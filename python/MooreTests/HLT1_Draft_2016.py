settings = "Physics_pp_Draft2016"
MooreOptions = dict(Simulation = False,
                    UseTCK = False,
                    ThresholdSettings = settings,
                    DataType = "2016",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20160522",
                    RemoveInputHltRawBanks = True,
                    EnableTimer = True,
                    ForceSingleL0Configuration = True,
                    TimingTest = True,
                    HltLevel = 'Hlt1',
                    Split = 'Hlt1')

def configure():
    from Configurables import HltConf
    HltConf().L0TCK = '0xFF61'
    from ConfigureJobs import configureOnlineConditions, removeHlt1Technical, getHlt1SelectionIDs
    configureOnlineConditions("Hlt1")
    removeHlt1Technical()
    from Gaudi.Configuration import appendPostConfigAction
    appendPostConfigAction(lambda: getHlt1SelectionIDs(settings))
