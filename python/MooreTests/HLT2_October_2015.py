settings = "Physics_25ns_October2015"
MooreOptions = dict(Simulation = False,
                    DataType = "2015",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20150828",
                    UseTCK = False,
                    EnableTimer = True,
                    ForceSingleL0Configuration = False,
                    ThresholdSettings = settings,
                    HltLevel = 'Hlt2',
                    Split = 'Hlt2')

def configure():
    from Configurables import UpdateAndReset
    UpdateAndReset().abortRetroEvents = False
    from Configurables import CondDB
    conddb = CondDB()
    conddb.EnableRunStampCheck = False
    from Configurables import HltConf
    HltConf().L0TCK = '0x0050'
    from Gaudi.Configuration import appendPostConfigAction
    def setHlt1SelectionIDs():
        from Configurables import HltANNSvc
        import pickle
        configuration = pickle.load(open("hlt1SelectionIDs_%s.p" % settings))
        Hlt1SelectionIDs = configuration[settings]
        HltANNSvc().Hlt1SelectionID = Hlt1SelectionIDs
    appendPostConfigAction( setHlt1SelectionIDs )

