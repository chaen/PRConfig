settings = "Physics_pp_Draft2016"
MooreOptions = dict(Simulation = False,
                    UseTCK = False,
                    ThresholdSettings = settings,
                    DataType = "2016",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20160517",
                    RemoveInputHltRawBanks = False,
                    EnableTimer = True,
                    CheckOdin = False,
                    ForceSingleL0Configuration = True,
                    TimingTest = True,
                    HltLevel = 'Hlt2',
                    Split = 'Hlt2')

def configure():
    from Configurables import HltConf
    HltConf().L0TCK = '0x1606'
    from ConfigureJobs import configureOnlineConditions, removeHlt1Technical, setHlt1SelectionIDs
    configureOnlineConditions("Hlt2")
    from Gaudi.Configuration import appendPostConfigAction
    appendPostConfigAction(lambda: setHlt1SelectionIDs(settings) )

