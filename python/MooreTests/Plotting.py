import os
from ROOT import gROOT, TFile, kBlue, TLegend
gROOT.SetBatch(True)

import LHCbStyle

__graphs = []
__canvases = []

def plot_results(results, directory = '.'):
    pid = results['Moore'][0]['mem_info'].keys()[0]
    n_s = len(results['Moore'][0]['mem_info'][pid])
    n_pid = len([pid for pid in results['Moore'][0]['mem_info'].keys() if type(pid) == int])

    av_uss = [0] * n_s
    av_pss = [0] * n_s
    av_rss = [0] * n_s
    av_virt = [0] * n_s
    total = results['Moore'][0]['mem_info']['total']
    t_zero = results['Moore'][0]['time_stamps'][0]
    times = []

    for i in xrange(n_s):
        for pid, l in results['Moore'][0]['mem_info'].iteritems():
            if type(pid) != int:
                continue
            for index, av in {0 : av_uss, 1 : av_pss, 2 : av_rss, 3 : av_virt}.iteritems():
                av[i] += float(l[i][index]) / n_pid
    for ts in results['Moore'][0]['time_stamps']:
        times.append(ts - t_zero)

    from ROOT import TCanvas
    from ROOT import TGraph
    from array import array

    canvases = {}
    legends = {}
    ranges = {'virtual' : (1.5, 2), 'pss' : (0.0, 0.5)}
    y_vals = (('pss', av_pss), ('uss', av_uss), ('rss', av_rss), ('virtual', av_virt), ('total', total))
    graphs = {e[0] : [] for e in y_vals}
    for prefix, yv in y_vals:
        cn = "canvas_%s" % prefix if prefix not in ('uss', 'pss') else 'canvas_mem'
        if cn not in canvases:
            canvas = TCanvas(cn, cn, 600, 400)
            legend = TLegend(0.7, 0.2, 0.9, 0.4)
            canvases[cn] = canvas
            legends[cn] = legend
        else:
            canvas = canvases[cn]
            legend = legends[cn]

        x = array('d', times)
        #print x
        y = array('d', (v / (1024**2) for v in yv))
        mean = sum(y) / len(y)
        y = array('d', y)
        print "<{0} memory> = {1:.3f} GB".format(prefix,mean)
        graph = TGraph(len(x), x, y)
        graph.SetName(prefix)
        y_axis = graph.GetYaxis()
        y_axis.SetTitle("memory [GiB]")
        y_axis.SetTitleOffset(1.0)
        x_axis = graph.GetXaxis()
        x_axis.SetRangeUser(0, times[-1])
        if prefix in ranges:
            y_axis.SetRangeUser(*ranges[prefix])
        graph.GetXaxis().SetTitle("time [s]")
        if prefix == 'uss':
            graph.SetLineColor(kBlue)
            graph.SetMarkerColor(kBlue)
            graph.Draw("P,same")
        else:
            graph.Draw("AP")
        legend.AddEntry(graph, prefix.upper(), "p")
        legend.Draw()
        graphs[prefix].append(graph)
        __graphs.append(graph)

    for canvas in canvases.itervalues():
        canvas.Print(os.path.join(directory, canvas.GetName() + '.pdf'))
        __canvases.append(canvas)

    root_file = 'test_results.root'
    rf = TFile(root_file, 'recreate')
    for prefix, gs in graphs.iteritems():
        for i, g in enumerate(gs):
            rf.WriteTObject(g, g.GetName() + '_' + str(i))
    rf.Close()

    return graphs
