MooreOptions = dict(Simulation = True,
                    DataType = "2012",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20150828",
                    UseTCK = False,
                    RemoveInputHltRawBanks = False,
                    EnableTimer = True,
                    ThresholdSettings = "Physics_25ns_August2015",
                    HltLevel = 'Hlt1Hlt2')

def configure():
    pass

## DDDBtag = "dddb-20120831",
## CondDBtag = "cond-20120831",
