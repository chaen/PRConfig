import re, os, sys, pwd, optparse, errno, tarfile

warned = False

class procdata(object):
    def __init__(self, source = ""):
        self._ucache = {}
        self._gcache = {}
        self.source = source and source or ""
        self._memdata = None
    def _list(self):
        return os.listdir(self.source + "/proc")
    def _read(self, f):
        return file(self.source + '/proc/' + f).read()
    def _readlines(self, f):
        return self._read(f).splitlines(True)
    def _stat(self, f):
        return os.stat(self.source + "/proc/" + f)
    def iskernel(pid):
        return self.pidcmd(pid) == ""
    def pids(self):
        '''get a list of processes'''
        return [int(e) for e in self._list()
                if e.isdigit() and not self.iskernel(e)]
    def mapdata(self, pid):
        return self._readlines('%s/smaps' % pid)
    def memdata(self):
        if self._memdata is None:
            self._memdata = self._readlines('meminfo')
        return self._memdata
    def version(self):
        return self._readlines('version')[0]
    def pidname(self, pid):
        try:
            l = self._read('%d/stat' % pid)
            return l[l.find('(') + 1: l.find(')')]
        except:
            return '?'
    def pidcmd(self, pid):
        try:
            c = self._read('%s/cmdline' % pid)[:-1]
            return c.replace('\0', ' ')
        except:
            return '?'
    def piduser(self, pid):
        try:
            return self._stat('%d' % pid).st_uid
        except:
            return -1
    def pidgroup(self, pid):
        try:
            return self._stat('%d' % pid).st_gid
        except:
            return -1
    def username(self, uid):
        if uid == -1:
            return '?'
        if uid not in self._ucache:
            try:
                self._ucache[uid] = pwd.getpwuid(uid)[0]
            except KeyError:
                self._ucache[uid] = str(uid)
        return self._ucache[uid]
    def groupname(self, gid):
        if gid == -1:
            return '?'
        if gid not in self._gcache:
            try:
                self._gcache[gid] = pwd.getgrgid(gid)[0]
            except KeyError:
                self._gcache[gid] = str(gid)
        return self._gcache[gid]

def pidmaps(src, pid):
    global warned
    maps = {}
    start = None
    seen = False
    empty = True
    for l in src.mapdata(pid):
        empty = False
        f = l.split()
        if f[-1] == 'kB':
            if f[0].startswith('Pss'):
                seen = True
            maps[start][f[0][:-1].lower()] = int(f[1])
        elif '-' in f[0] and ':' not in f[0]: # looks like a mapping range
            start, end = f[0].split('-')
            start = int(start, 16)
            name = "<anonymous>"
            if len(f) > 5:
                name = f[5]
            maps[start] = dict(end=int(end, 16), mode=f[1],
                               offset=int(f[2], 16),
                               device=f[3], inode=f[4], name=name)

    if not empty and not seen and not warned:
        sys.stderr.write('warning: kernel does not appear to support PSS measurement\n')
        warned = True

    return maps

def sortmaps(totals, key):
    l = []
    for pid in totals:
        l.append((totals[pid][key], pid))
    l.sort()
    return [pid for pid,key in l]

def memory(src):
    t = {}
    f = re.compile('(\\S+):\\s+(\\d+) kB')
    for l in src.memdata():
        m = f.match(l)
        if m:
            t[m.group(1).lower()] = int(m.group(2))
    return t

def pidtotals(src, pid):
    maps = pidmaps(src, pid)
    t = dict(size=0, rss=0, pss=0, shared_clean=0, shared_dirty=0,
             private_clean=0, private_dirty=0, referenced=0, swap=0)
    for m in maps.iterkeys():
        for k in t:
            t[k] += maps[m].get(k, 0)

    t['uss'] = t['private_clean'] + t['private_dirty']
    t['maps'] = len(maps)

    return t
