settings = "Physics_pp_2018"
MooreOptions = dict(Simulation = False,
                    UseTCK = False,
                    ThresholdSettings = settings,
                    DataType = "2018",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20170510",
                    RemoveInputHltRawBanks = False,
                    EnableTimer = True,
                    ForceSingleL0Configuration = True,
                    TimingTest = True,
                    HltLevel = 'Hlt2',
                    Split = 'Hlt2',
                    EnableOutputStreaming = True)

def configure():
    pass

from Configurables import HltConf
HltConf().L0TCK = '0x1706'
from ConfigureJobs import configureOnlineConditions,  setHlt1SelectionIDs
from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(lambda: setHlt1SelectionIDs(settings) )
