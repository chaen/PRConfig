def removeHlt1Technical():
    from Configurables import HltConf
    HltConf().RemoveHlt1Lines = [ "Hlt1MBNoBias"
                                  , "Hlt1MBNoBiasRateLimited"
                                  , "Hlt1Lumi", 'Hlt1L0Any','Hlt1L0AnyNoSPD'
                                  , 'Hlt1MBNoBias'
                                  , 'Hlt1MBNoBiasRateLimited'
                                  , 'Hlt1ODINTechnical', 'Hlt1Tell1Error' , 'Hlt1ErrorEvent' # , 'Hlt1Incident'
                                  , 'Hlt1VeloClosingMicroBias'
                                  ]

def getHlt1SelectionIDs(settings):
    from Configurables import HltANNSvc
    import pickle
    Hlt1SelectionIDs = HltANNSvc().Hlt1SelectionID
    configuration = {settings : Hlt1SelectionIDs }
    pickle.dump( configuration, open( "hlt1SelectionIDs_%s.p" % (settings), "wb" ) )

def setHlt1SelectionIDs(settings):
    from Configurables import HltANNSvc
    import pickle
    configuration = pickle.load(open("hlt1SelectionIDs_%s.p" % settings))
    Hlt1SelectionIDs = configuration[settings]
    HltANNSvc().Hlt1SelectionID = Hlt1SelectionIDs

def configureOnlineConditions(level):
    import sys
    try:
        if level=="Hlt1":
            import AllHlt1
        if level=="Hlt2":
            import All
    except ImportError:
        rd = '/group/online/hlt/conditions/RunChangeHandler'
        sys.path.append(rd)
        if level=="Hlt1":
            import AllHlt1
        if level=="Hlt2":
            import All
    from Configurables import CondDB
    conddb = CondDB()
    conddb.UseDBSnapshot = True
    conddb.DBSnapshotDirectory = "/group/online/hlt/conditions"
    conddb.Online = True
    conddb.EnableRunChangeHandler = True
    conddb.Tags["ONLINE"] = 'fake'

    if level=="Hlt1":
        conddb.RunChangeHandlerConditions = {k.replace('2016', '2015') : v for k, v in AllHlt1.ConditionMap.iteritems()}
    if level=="Hlt2":
        conddb.RunChangeHandlerConditions = All.ConditionMap
    conddb.EnableRunStampCheck = False

    from Configurables import UpdateAndReset
    UpdateAndReset().abortRetroEvents = False
