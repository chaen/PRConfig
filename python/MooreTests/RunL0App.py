#!/usr/bin/env python
# Script to rerun L0 with L0App, to provide input to the Moore_RateTest script
# Mika Vesterinen

import os, sys, subprocess, re, optparse, math

# Configuration
import Gaudi.Configuration
from Gaudi.Configuration import *
from Configurables import GaudiSequencer as Sequence
from Configurables import EventSelector, HltConf
from Moore.Configuration import Moore
from Gaudi.Configuration import appendPostConfigAction
from multiprocessing import Process, Queue

# GaudiPython
import GaudiPython
from GaudiPython import AppMgr
LHCb = GaudiPython.gbl.LHCb

def main():

    parser = optparse.OptionParser( usage = "usage: %prog [options]" )

    # general test options

    parser.add_option( "-n", "--evtmax", type="int", action = "store", dest = "evtmax",
                       default = 30000, help = "Number of events to run over" )

    parser.add_option( "--TCK", action = "store", dest="TCK",
                       default = '0x1609',
                       help = "HLT TCK. If unspecified, then run from ThresholdSettings.")

    parser.add_option( "--inputdata",action="store",dest="inputdata",
                       default="2016NB_25ns_L0Filt0x1609",help="TestFileDB label, or path to raw file.")

    parser.add_option( "--outputfile",action="store",dest="outputfile",
                       default="rerunL0.dst",help="OutputFile")

    parser.add_option( "--replay",action="store_true",help="Replay L0DU only")


    (options, args) = parser.parse_args()
    
    from Configurables import L0App
    L0App().TCK = options.TCK
    L0App().EvtMax = options.evtmax
    L0App().ReplaceL0Banks = True
    L0App().ReplayL0DUOnly = options.replay
    
    from PRConfig import TestFileDB
    TestFileDB.test_file_db[options.inputdata].run(configurable=L0App())

    from Configurables import bankKiller
    hlt_banks = [ 'HltDecReports',
                  'HltRoutingBits',
                  'HltSelReports',
                  'HltVertexReports',
                  'HltLumiSummary',
                  'HltTrackReports',
                  'DstData' ]
    killer = bankKiller( 'RemoveInputHltRawBanks',  BankTypes = hlt_banks )
    
    
    from Configurables import EventSelector
    EventSelector().PrintFreq = 100
    
    from Configurables import L0DUFromRawAlg
    l0du_alg = L0DUFromRawAlg()
    
    from Configurables import L0DUReportMonitor
    l0du_mon = L0DUReportMonitor()
    
    from Configurables import LoKiSvc
    LoKiSvc().Welcome = False
    from Configurables import LoKi__L0Filter
    l0_filter = LoKi__L0Filter('L0Filter', Code = "L0_DECISION(LHCb.L0DUDecision.Any)")
    
    from GaudiConf import IOHelper
    from Configurables import InputCopyStream
    input_copy = IOHelper().outputAlgs(options.outputfile, InputCopyStream('CopyStream'), writeFSR = False)[0]
    
    from Configurables import GaudiSequencer
    from Gaudi.Configuration import ApplicationMgr
    writer_seq = GaudiSequencer('WriterSeq')
    writer_seq.Members = [l0du_alg, l0du_mon, l0_filter, input_copy]
    

    from Configurables import RootCnvSvc
    RootCnvSvc().GlobalCompression = "ZLIB:1"

    ApplicationMgr().OutStream = [writer_seq]
    ApplicationMgr().TopAlg = [killer]
    
    ### getting ready for the event loop
    gaudi = AppMgr()
    gaudi.ExtSvc += ['ToolSvc']
    gaudi.ExtSvc.append( 'DataOnDemandSvc' )
    gaudi.initialize()

    #### start of the event loop
    i=0
    while i < L0App().EvtMax:
        i+=1
        gaudi.run(1)
    # finalise
    gaudi.stop()
    gaudi.finalize()
    gaudi.exit()

if __name__ == "__main__":
    sys.exit( main() )
