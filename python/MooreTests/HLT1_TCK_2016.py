
MooreOptions = dict(Simulation = False,
                    DataType = "2016",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20160522",
                    RemoveInputHltRawBanks = False,
                    UseTCK = True,
                    InitialTCK = "0x11361609",
                    HltLevel = 'Hlt1')

def configure():
    from Moore import Funcs

    lines = ["Hlt1MBNoBias",
             "Hlt1MBNoBiasRateLimited",
             "Hlt1Lumi",
             "Hlt2Lumi"]

    trans={".*{}PreScaler".format(line) : {"AcceptFraction" : {"^.*$": "0"}} for line in lines}
    Funcs._mergeTransform(trans)
