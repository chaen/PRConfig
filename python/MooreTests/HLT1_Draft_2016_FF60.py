import sys
try:
    import AllHlt1
except ImportError:
    rd = '/group/online/hlt/conditions/RunChangeHandler'
    sys.path.append(rd)
    import AllHlt1
settings = "Physics_pp_Draft2016"

MooreOptions = dict(Simulation = False,
                    DataType = "2015",
                    DDDBtag = "dddb-20150724",
                    CondDBtag = "cond-20150828",
                    UseTCK = False,
                    RemoveInputHltRawBanks = True,
                    EnableTimer = True,
                    ForceSingleL0Configuration = False,
                    ThresholdSettings = settings,
                    TimingTest = True,
                    RunChangeHandlerConditions = AllHlt1.ConditionMap,
                    HltLevel = 'Hlt1',
                    Split = 'Hlt1')

def configure():
    from Configurables import HltConf
    HltConf().L0TCK = '0xFF60'
    from Configurables import CondDB
    conddb = CondDB()
    conddb.EnableRunStampCheck = False
    from Gaudi.Configuration import appendPostConfigAction
    HltConf().RemoveHlt1Lines = [ "Hlt1MBNoBias"
                                 , "Hlt1MBNoBiasRateLimited"
                                 , "Hlt1Lumi", 'Hlt1L0Any','Hlt1L0AnyNoSPD'
                                 , 'Hlt1MBNoBias'
                                 , 'Hlt1MBNoBiasRateLimited'
                                 , 'Hlt1ODINTechnical', 'Hlt1Tell1Error' , 'Hlt1ErrorEvent' # , 'Hlt1Incident'
                                 , 'Hlt1VeloClosingMicroBias'
                                 ]
    def getHlt1SelectionIDs():
        from Configurables import HltANNSvc
        import pickle
        Hlt1SelectionIDs = HltANNSvc().Hlt1SelectionID 
        configuration = {settings : Hlt1SelectionIDs }
        pickle.dump( configuration, open( "hlt1SelectionIDs_%s.p" % (settings), "wb" ) )
    appendPostConfigAction(getHlt1SelectionIDs )
## DDDBtag = "dddb-20120831",
## CondDBtag = "cond-20120831",
