#!/usr/bin/env python
# =============================================================================
# @file   TestFileDBObject.py
# @author Albert Puig (albert.puig@epfl.ch)
# @date   09.09.2014
# =============================================================================
"""Define a dict-like class to contain the TestFileDB."""

import re

class TestFileDBDict(dict):
    """Class that adds extra functionality to the dictionary containing the
       TestFileDB, allowing to batch-search for files with regular expressions."""
    def search_files(self, regex):
        """Get the list of keys matching a given regular expression.

        @param self The object pointer.
        @param regex Regular expression to match to the files in the DB.

        @return List of keys matching the regex.
        """
        regex_c = re.compile(regex)
        found_keys = []
        for key in self.keys():
            if regex_c.search(key):
                found_keys.append(key)
        return found_keys

    def get_files(self, regex):
        """Get files matching a given regular expression.

        @param self The object pointer.
        @param regex Regular expression to match to the files in the DB.

        @return List of files matching the regex.
        """
        regex_c = re.compile(regex)
        found_files = []
        for key, file_ in self.items():
            if regex_c.search(key):
                found_files.append(file_)
        return found_files

# EOF
