"""
TestFileDB: the database of files used for testing in the nightlies and elsewhere.

Usage: Add into this module a testfiles() object which maps a logical name for a dataset, like '2012_raw_default' to a list of files.

This module creates the dictionary {logicalname : <testfiles>)

Testfile object, minimum information:
   Minimally you must supply->
     1) a logical name
     2) a list of files
     3) a dictionary with { 'Date':<> , 'Format':<>, 'DataType' : <>}
     4) a meaningful comment as to why the file was added to the test DB
     (the database to add to, test_file_db)

So, minimally something like:

testfiles(
    'Dummy!!',
    ['/dev/null'],
    {
    'Author': 'Rob Lambert',
    'Format': 'DIGI',
    'DataType': '2011',
    'Date': '2013.05.31',
    },
    'Dummy test file, for testing the testfiles mechanism...',
    test_file_db
    )

The testfiles object has the concept of a qualifier, for which a small number are additionally known, such as the DDDB tag, CondDB tag, Production, Stripping, Reconstruction, etc.

For the list of possibilites, see PRConfig.TestFileObjects.testfiles.__known_qualifiers__

The testfiles object is smart enough to know how to add it into the EventSelector, e.g.:

from PRConfig.TestFileDB import test_file_db

test_file_db['2012_raw_default'].run() #pass and options to LHCbApp

or

test_file_db['2012_raw_default'].run(configurable=Moore())

---------------------------------

There is now an automated mechanism to append to this database, see:
-> $PRCONFIGROOT/scripts/AddToTestFilesDB.py

To check files in the database can be read, see:
-> $PRCONFIGROOT/scripts/TestReadable.py

To migrate files in this database to EOS/CERN-SWTEST, see:
-> $PRCONFIGROOT/scripts/MigrateToSWTEST.py

"""
#get the testfile object definition
from PRConfig.TestFileObjects import testfiles
from PRConfig.TestFileDBObject import TestFileDBDict

#the database to fill
test_file_db=TestFileDBDict()

###############  TEST FILES START HERE ###################

# Moore tests
testfiles(
    "S20r0p2_stripped_test",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/B2CC.S20r0p2.Dimuon.dst"],
    {
    "Author": "Stefano Perazzini",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20r0p2",
    "Reconstruction":14,
    "Stripping":"20r0p2",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20p2","moore.swim_s20p2"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Test stripping v20r0p2 2013 DST including packed flavour tag classes. DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db
    )

testfiles(
    "S20r1p2_stripped_test",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/B2CC.S20r1p2.Dimuon.dst"],
    {
    "Author": "Stefano Perazzini",
    "Format": "DST",
    "DataType": "2011",
    "Date": "2013.09.30",
    "Processing": "Stripping20r1p2",
    "Reconstruction":14,
    "Stripping":"20r1p2",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": [],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Test stripping v20r1p2 2013 DST including packed flavour tag classes. DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db
    )

testfiles(
    "S20_stripped_test",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00020567/0000/00020567_00006473_1.dimuon.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20",
    "Reconstruction":14,
    "Stripping":20,
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20", "ioexample.copyreco14stripping20dsttoroot"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Test stripping v20r0 2013 DST *not* including packed flavour tag classes, for Moore v14r8 (nominal). ",
    test_file_db
    )


testfiles(
    "S20_v14r11_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00021211/0000/00021211_00009739_1.dimuon.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20",
    "Reconstruction":14,
    "Stripping":20,
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r11. ",
    test_file_db
    )


testfiles(
    "S20_v14r9_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00024183/0000/00024183_00009746_1.dimuon.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20",
    "Reconstruction":14,
    "Stripping":20,
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r9. ",
    test_file_db
    )


testfiles(
    "S20_v14r6_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00024183/0002/00024183_00020006_1.dimuon.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20",
    "Reconstruction":14,
    "Stripping":20,
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"head-20120413",
    "CondDB": "cond-20120730"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r6. ",
    test_file_db
    )

testfiles(
    "S20_v14r2_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00018117/0000/00018117_00002737_1.dimuon.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20",
    "Reconstruction":14,
    "Stripping":20,
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"head-20120413",
    "CondDB": "head-20120420"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r2. ",
    test_file_db
    )

testfiles(
    "S20r0p2_v14r9_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030613/0001/00030613_00018961_1.bhadroncompleteevent.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20r0p2",
    "Reconstruction":14,
    "Stripping":"20r0p2",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r9.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db
    )

testfiles(
    "S20r0p2_v14r8_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030613/0001/00030613_00015694_1.bhadroncompleteevent.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20r0p2",
    "Reconstruction":14,
    "Stripping":"20r0p2",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r8.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db
    )


testfiles(
    "S20r0p2_v14r6_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030613/0000/00030613_00006296_1.bhadroncompleteevent.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20r0p2",
    "Reconstruction":14,
    "Stripping":"20r0p2",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"head-20120413",
    "CondDB": "cond-20120730"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r6.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db
    )

testfiles(
    "S20r0p2_v14r2_swim",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030264/0000/00030264_00006362_1.bhadroncompleteevent.dst"],
    {
    "Author": "Vava Gligorov",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2013.09.30",
    "Processing": "Stripping20r0p2",
    "Reconstruction":14,
    "Stripping":"20r0p2",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.rerun_s20","moore.swim_s20"],
    "DDDB":"head-20120413",
    "CondDB": "head-20120420"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r2.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db
    )

testfiles(
    "S21_bhadron_mdst",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00041836/0000/00041836_00000001_1.bhadron.mdst"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDST",
    "DataType": "2012",
    "Date": "2012",
    "Processing": "Stripping21",
    "Reconstruction":14,
    "Stripping":"21",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["ioexample.copyreco14stripping21mdsttoroot"],
    "DDDB":"dddb-20130929-1",
    "CondDB": "cond-20141107"
    },
    "Test with Reco14-Stripping21 BHADRON.MDST from 2012",
    test_file_db
    )

testfiles(
    "2017_raw_full",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2017/RAW/FULL/LHCb/COLLISION17/198660/198660_0000000211.raw"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDF",
    "DataType": "2017",
    "Date": "2017.06.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["brunel.2017magup","l0app.data_xxx","DaVinciTests.davinci.gaudipython_algs"],
    },
    "Proton-Proton collision raw data FULL stream, run 198660 from 6th September 2017. Same run as used for Stripping tests",
    test_file_db
    )

testfiles(
    "2016_raw_full",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2016/RAW/FULL/LHCb/COLLISION16/179277/179277_0000000601.raw"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDF",
    "DataType": "2016",
    "Date": "2016.07.05",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["brunel.2016magup"],
    },
    "Proton-Proton collision raw data FULL stream, run 179277 from 7th July 2016. Same run as used for Stripping tests",
    test_file_db
    )

testfiles(
    "2015_raw_full",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2015/RAW/FULL/LHCb/COLLISION15/164668/164668_0000000744.raw"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDF",
    "DataType": "2015",
    "Date": "2015.10.02",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["brunel.2015magdown, brunel.compatibility.2015-latest-*db"],
    },
    "Proton-Proton collision raw data FULL stream, run 164668 from 2nd October 2015. Same run as used for Stripping tests",
    test_file_db
    )

testfiles(
    "2009_raw",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2009/RAW/FULL/LHCb/COLLISION09/63497/063497_0000000001.raw"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDF",
    "DataType": "2009",
    "Date": "2009.12.06",
    "Simulation" : False,
    "QMTests": ["brunel.fixedfile-brunel2009-*"],
    },
    "Proton-Proton collision raw data FULL stream, run 32474 from 12th December 2009. Magnet on, all detectors in readout, Velo open",
    test_file_db
    )

testfiles(
    "2008_cosmics",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2008/RAW/LHCb/COSMICS/35537/035537_0000088110.raw"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDF",
    "DataType": "2008",
    "Date": "2008",
    "Simulation" : False,
    "QMTests": ["brunel.brunel-cosmics"],
    },
    "Cosmics run, with CALO, OT and  RICH2 (35569 events)",
    test_file_db
    )

testfiles(
    "2008_TED",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2008/RAW/LHCb/BEAM/32474/032474_0000081642.raw"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDF",
    "DataType": "2008",
    "Date": "2008.09",
    "Simulation" : False,
    "QMTests": ["brunel.brunel-2008"],
    },
    "TED data from September 2008 with velo - prev2",
    test_file_db
    )

testfiles(
    "2012_raw_default",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_default/1318"+str(n)+"_0x0046_NB_L0Phys_00.raw" for n in range(83,100)],
    {
    "Author": "Eric Van Herwijnen",
    "Format": "MDF",
    "DataType": "2012",
    "Date": "2012.11.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["moore.physics.2012","moore.database.2012","l0app."],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "L0 Phys, L046, from 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets",
    test_file_db
    )

testfiles(
    "2012_raw_full",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2012/RAW/FULL/LHCb/COLLISION12/114753/114753_0000000"+n+".raw" for n in ["015","016","017","298"]],
    {
    "Author": "Jaap Panman",
    "Format": "MDF",
    "DataType": "2012",
    "Date": "2012.11.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["lumialgs"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Direct raw data from the pit taken in 2012. Used for Rec/LumiAlgs tests so that there are lumi events, etc. copied out of the freezer",
    test_file_db
    )

testfiles(
    "2012_raw_express",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2012/RAW/EXPRESS/LHCb/COLLISION12/124915/124915_0000000"+n+".raw" for n in ["008","112"]],
    {
    "Author": "Jaap Panman",
    "Format": "MDF",
    "DataType": "2012",
    "Date": "2012.11.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["lumialgs"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "Direct raw data from the pit taken in 2012 from the express stream. Used for Rec/LumiAlgs tests so that there are lumi events, etc. copied out of the freezer",
    test_file_db
    )

testfiles(
    "2012_raw_small",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_small/131883_0x0046_NB_L0Phys_00_3ev.raw","mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_small/131884_0x0046_NB_L0Phys_00_3ev.raw","mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_small/131886_0x0046_NB_L0Phys_00_1000ev.raw","mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_small/131885_0x0046_NB_L0Phys_00_5ev.raw"],
    {
    "Author": "Eric Van Herwijnen",
    "Format": "MDF",
    "DataType": "2012",
    "Date": "2012.11.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["filememrger.mdf"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "L0 Phys, L046, from 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets, reduced to be only 4 files and 1011 events for testing mdf merging",
    test_file_db
    )

testfiles(
    "2012_raw_L041",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_L041/2012_raw_L041.mdf"],
    {
    "Author": "Eric Van Herwijnen",
    "Format": "MDF",
    "DataType": "2012",
    "Date": "2012.11.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["moore.vdm"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "L0 Phys, L041, from 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets, originally L046, re-emulated to L041.",
    test_file_db
    )

testfiles(
    "2012_raw_L03D",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_L03D/1147"+str(n)+"_0x003D_NB_L0Phys_00.raw" for n in range(74,65,-1)+range(63,50,-1)]+["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2012_raw_L03D/1153"+str(n)+"_0x003D_NB_L0Phys_00.raw" for n in range(70,74)+range(75,90)],
    {
    "Author": "Eric Van Herwijnen",
    "Format": "MDF",
    "DataType": "2012",
    "Date": "2012.11.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["moore.physics.2012"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "L0 Phys, L03D, from early 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets",
    test_file_db
    )

testfiles(
    "2011_raw_default",
    [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_raw_default/81349_0x002a_MBNB_L0Phys.raw",
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_raw_default/80881_0x002a_MBNB_L0Phys.raw",
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_raw_default/79647_0x002a_MBNB_L0Phys.raw",
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_raw_default/79646_0x002a_MBNB_L0Phys.raw"
    ],
    {
    "Author": "Eric Van Herwijnen",
    "Format": "MDF",
    "DataType": "2011",
    "Date": "2011.01.07",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["moore.database.2011"],
    "DDDB":"head-20110302",
    "CondDB": "head-20110622"
    },
    "L0 Phys, L002, from 2011 data, used previously for run_more script in Moore tests. Now no longer used there.",
    test_file_db
    )

testfiles(
    "2010_raw_default",
    ["mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2010_raw_default/075161_0000000282.raw"],
    {
    "Author": "LHCb",
    "Format": "MDF",
    "DataType": "2010",
    "Date": "2010.07.09",
    "Application": "Moore",
    "Simulation" : False,
    "QMTests": ["compatibility","moore.database.2010"],
    "DDDB":"head-20101026",
    "CondDB": "head-20101112"
    },
    "Raw data used for 'fixed file' compatibilty tests of old Moore with latest databases. Found from the book-keeping some time in 2010.",
    test_file_db
    )


testfiles(
    "2010_digi_default",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2010_digi_default/00007107_00000001_2.digi"],
    {
    "Author": "Marco Cattaneo",
    "Format": "MDF",
    "DataType": "2010",
    "Date": "2010.12.07",
    "Application": "Boole",
    "Simulation" : True,
    "QMTests": ["compatibility"],
    "DDDB":"head-20100624",
    "CondDB": "sim-20100624-vc-md100"
    },
    "2010 simulation Digi files for data used for 'fixed file' compatibilty tests of old Moore with latest databases. Found from the book-keeping some time in 2010.",
    test_file_db
    )


testfiles(
    "Sim08_2012_sim_pGun",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08_pGun/00042604_1.sim"],
    {
    "Author": "Gloria Corti",
    "Format": "SIM",
    "DataType": "2012",
    "Date": "2015.02.18",
    "Application": "Gauss",
    "Simulation" : True,
    "QMTests": [],
    "DDDB":"dddb-20130929-1",
    "CondDB": "sim-20130522-1-vc-md100"
    },
    "2012 simulation Sim file for signal particle gun used for tests of Boole processing.",
    test_file_db
    )


testfiles(
    "Sim08_2012_digi_pGun",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08_pGun/00042604_2.digi"],
    {
    "Author": "Gloria Corti",
    "Format": "DIGI",
    "DataType": "2012",
    "Date": "2015.02.18",
    "Application": "Boole",
    "Simulation" : True,
    "QMTests": ["brunel.sim08-pgun"],
    "DDDB":"dddb-20130929-1",
    "CondDB": "sim-20130522-1-vc-md100"
    },
    "2012 simulation Digi files for signal particle gun used for tests of Brunel special configuration.",
    test_file_db
    )

testfiles(
    "Sim08_2012_digi",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08-2012/Gauss-v45r0-10000000-50ev-20130313-HLT.digi"],
    {
    "Author": "Gloria Corti",
    "Format": "DIGI",
    "DataType": "2012",
    "Date": "2013.03.13",
    "Application": "Moore",
    "Simulation" : True,
    "QMTests": ["brunel.sim08", "brunel.sim08-invfield-down", "brunel.sim08-invfield-up"],
    "DDDB":"dddb-20130312-1",
    "CondDB": "sim-20130522-1-vc-md100"
    },
    "2012 simulation minimum bias Digi files (Gauss v45r0, Boole v26r3, Moore v14r8p1 used for tests of Brunel Sim08 settings",
    test_file_db
    )

testfiles(
        "Sim09_2016_digi",
            ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/debug/2016/DIGI/00073919/0000/00073919_00000001_4.digi"],
            {
    "Author": "Marco Cattaneo",
    "Format": "DIGI",
    "DataType": "2016",
    "Date": "2018.05.29",
    "Application": "Moore",
    "Simulation" : True,
    "QMTests": ["brunel.sim09-2016"],
    "DDDB":"dddb-20170721-3",
    "CondDB": "sim-20170721-2-vc-md100"
        },
            "2016 Sim09 Digi, event type 49000041, /MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/ (Boole step 133533, Moore steps 130089, 130090)",
            test_file_db
            )

testfiles(
    "Charm_Strip20_Test",
    ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Charm_Strip20_Test/00022726_00111043_1.CharmToBeSwum.dst'],
    {
    "Author": "Nick Torr",
    "Format": "DST",
    "DataType": "2012",
    "Processing": "Stripping20",
    "Reconstruction":14,
    "Stripping":20,
    "Date": "2013.02.15",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.swim_strip20"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20121025"
    },
    "DST of offline-selected charm physics events for testing the swimming. See task #40021",
    test_file_db
    )

testfiles(
    "BsMuMu_Strip20_Test",
    ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/BSMUMUUNBLIND.DST/00012992/0000/00012992_00000010_1.bsmumuunblind.dst'],
    {
    "Author": "Xabier Cid Vidal",
    "Format": "DST",
    "DataType": "2011",
    "Processing": "Stripping20",
    "Reconstruction":14,
    "Stripping":20,
    "Date": "2013.01.01",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["moore.swim_strip20"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20121025"
    },
    "DST of offline-selected Bs->mumu physics events for testing the swimming/re-running the trigger, copy of an original from production. See task #42208",
    test_file_db
    )

testfiles(
    "Sim08_2012_ForMoore",
    [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08_2012_ForMoore/HepMCYes-10000000-100ev-20130225.digi",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08_2012_ForMoore/HepMCNo-10000000-100ev-20130304.digi"
    ],
    {
    "Author": "Gloria Corti",
    "Format": "DIGI",
    "DataType": "2012",
    "Processing": "Sim08",
    "Date": "2013.02.26",
    "Application": "Boole",
    "Simulation" : True,
    "QMTests": ["moore.mc2012","l0app"],
    "DDDB":"dddb-20120831",
    "CondDB": "sim-20121025-vc-md100"
    },
    "Gauss/Boole output for testing Moore v14r8p6 in preparation for large scale 2012 simulation, Sim08, with Reco14 Stripping20. see task #39714",
    test_file_db
    )

testfiles(
    "Sim08_2012_L045",
    [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08_2012_L045/HepMCYes-10000000-100ev-20130225-L045.digi"
    ],
    {
    "Author": "Gloria Corti, Rob Lambert",
    "Format": "DIGI",
    "DataType": "2012",
    "Processing": "Sim08",
    "Date": "2013.02.26",
    "Application": "Boole",
    "Simulation" : True,
    "QMTests": ["moore.mc2012"],
    "DDDB":"dddb-20120831",
    "CondDB": "sim-20121025-vc-md100"
    },
    "Generated from Sim08_2012_ForMoore with L0 0x0045 added by L0App, for testing of new running of Moore MC after L0App.",
    test_file_db
    )

testfiles(
    "Sim08_2012_L044",
    [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08_2012_L044/HepMCYes-10000000-100ev-20130225-L044.digi"
    ],
    {
    "Author": "Gloria Corti, Rob Lambert",
    "Format": "DIGI",
    "DataType": "2012",
    "Processing": "Sim08",
    "Date": "2013.02.26",
    "Application": "Boole",
    "Simulation" : True,
    "QMTests": ["moore.mc2012"],
    "DDDB":"dddb-20120831",
    "CondDB": "sim-20121025-vc-md100"
    },
    "Generated from Sim08_2012_ForMoore with L0 0x0044 added by L0App, for testing of new running of Moore MC after L0App.",
    test_file_db
    )

testfiles(
    "Sim08_2011_ForMoore",
    [ "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Sim08_2011_ForMoore/Sim08-2011-Boole-10000000-20130508-100ev.digi" ],
    {
    "Author": "Gloria Corti",
    "Format": "DIGI",
    "DataType": "2011",
    "Processing": "Sim08",
    "Date": "2013.05.08",
    "Application": "Boole",
    "Simulation" : True,
    "QMTests": ["moore.run_moore_mc2011"],
    "DDDB":"dddb-20130503",
    "CondDB": "sim-20130503-vc-md100"
    },
    "Gauss/Boole output for testing Moore v12r8g3 in preparation for large scale 2011 re-simulation with, Sim08, Reco14, Stripping20. see task #40023",
    test_file_db
    )

testfiles(
    "BsPhiPhi_TISTOSTest",
    [ "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/2011/ALLSTREAMS.DST/00026133/0000/00026133_00000013_1.allstreams.dst" ],
    {
    "Author": "Sean Benson",
    "Format": "DST",
    "DataType": "2011",
    "Processing": "Sim08",
    "Date": "2014.08.23",
    "Application": "DaVinci",
    "Simulation" : True,
    "QMTests": ["moore.tistos"],
    "DDDB":"dddb-20130503",
    "CondDB": "sim-20130503-vc-md100"
    },
    "Bs->phi phi MC DST, to check sensible and consistent trigger efficiency is found from the Hlt",
    test_file_db
    )

testfiles(
    "2015NB_25ns_L0Filt0x0050",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2015NB_25ns_L0Filt0x0050/2015NB_25ns_0x0050_'+str(n)+'.mdf' for n in range(0,9)],
    {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2015.12.10",
        "Application": "Moore",
        "Simulation" : False,
        "QMTests": [],
        "DDDB"  : "dddb-20150724",
        "CondDB" :"cond-20150828"
        },
    "2015NB data that has been filtered through 0x0050",
    test_file_db
    )


testfiles(
    "2016_Hlt1_0x11291605",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016_Hlt1_0x11291605/Run_0176198_20160605-%s.hltf1001_%s.mdf' %(f[0],f[1]) for f in [['124747','000'],['124859','001'],['125010','002'],['125121','003'],['125232','004'],['125345','005'],['125457','006'],['125609','007'],['125722','008'],['125834','009'],['125946','010'],['130058','011'],['130211','012'],['130325','013'],['130437','014'],['130550','015'],['130819','016'],['130932','017'],['131046','018'],['131201','019'],['131316','020'],['131433','021'],['131550','022'],['131707','023'],['131823','024'],['131956','025'],['132114','026'],['132231','027'],['132348','028'],['132623','029'],['132741','030'],['132858','031'],['133017','032'],['133134','033'],['133252','034'],['133409','035'],['133525','036'],['133642','037'],['133800','038'],['133918','039'],['134036','040'],['134150','041'],['134414','042'],['134524','043']]#,
     #'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016_Hlt1_0x11291605/Run_0176263_20160606-%s.hltf1001_%s.mdf' %(f[0],f[1]) for f in [['053944','044'],['054206','045'],['054321','046'],['054435','047'],['054551','048'],['054705','049'],['054820','050'],['054935','051'],['055050','052'],['055205','053'],['055322','054'],['055437','055'],['055552','056'],['055706','057'],['055821','058'],['055935','059'],['060012','060'],['060049','061'],['060204','062'],['060319','063'],['060436','064'],['060552','065'],['060708','066'],['060824','067'],['060940','068'],['061054','069'],['061206','070'],['061319','071'],['061433','072'],['061546','073'],['061701','074'],['061931','075'],['062047','076'],['062203','077'],['062314','078'],['062425','079'],['062538','080'],['062651','081'],['062803','082'],['062917','083'],['063029','084'],['063144','085'],['063256','086'],['063409','087'],['063521','088'],['063634','089'],['063711','090'],['063749','091'],['063902','092']]
     ],
    {
        "Author": "Mark Williams",
        "Format": "MDF",
        "DataType": "2016",
        "Processing": "Moore",
        "Date": "2016.06.06",
        "Application": "Moore",
        "Simulation" : False,
        "QMTests": [],
        "DDDB"  : "dddb-20150724",
        "CondDB" :"cond-20160517",
        "InitialTime": "2016-06-05 11:46:00 UTC",
        },
    "2016 data accepted by Hlt1 TCK 0x11291605",
    test_file_db
    )

testfiles(
    "2016_Hlt1_0x11361609",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016_Hlt1_0x11361609/Run_0179276_20160705-%s.hlta0101_%s.mdf'%(f[0],f[1]) for f in [['162951','000'],['163211','001'],['163432','002'],['163656','003'],['163917','004'],['164139','005'],['164401','006'],['164622','007'],['164843','008'],['165107','009'],['165330','010'],['165556','011']]
     ],
    {
        "Author": "Jessica Prisciandaro",
        "Format": "MDF",
        "DataType": "2016",
        "Processing": "Moore",
        "Date": "2016.07.05",
        "Application": "Moore",
        "Simulation" : False,
        "QMTests": [],
        "DDDB"  : "dddb-20150724",
        "CondDB" :"cond-20160522",
        "InitialTime": "2016-07-05 15:29:00 UTC",
        },
    "2016 data accepted by Hlt1 TCK 0x11361609",
    test_file_db
    )



testfiles(
    "2015NB_25ns_L0Filt0xFF60",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xFF60/2015NB_25ns_0xFF60_%s_%s.mdf' %(f[0],f[1]) for f in [[162530,'001'],[162531,'002'],[162533,'003'],[162535,'004'],[162538,'005'],[162540,'006'],[162544,'007'],[162546,'008'],[162549,'009'],[162553,'010'],[162559,'011'],[162600,'012'],[162623,'013'],[162871,'014'],[162873,'000'],[162873,'015'],[162874,'016'],[162877,'017'],[162880,'018'],[162881,'019'],[162883,'020'],[162884,'021'],[162949,'022']]],
    {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.03.30",
        "Application": "Moore",
        "Simulation" : False,
        "QMTests": [],
        "DDDB"  : "dddb-20150724",
        "CondDB" :"cond-20150828"
        },
    "2015NB data that has been filtered through 0xFF60",
    test_file_db
    )

testfiles(
    "2015NB_25ns_L0Filt0x1606",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Run2_L0Processed_0x1606/25ns_NoBias/2015NB_25ns_0x1606_%03d.mdf' %(i,)  for i in range(0,475)],
    {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.04.16",
        "Application": "Moore",
        "Simulation" : False,
        "QMTests": [],
        "DDDB"  : "dddb-20150724",
        "CondDB" :"cond-20160517"
        },
    "2015NB data that has been filtered through 0x1606 using the EFF",
    test_file_db
    )

testfiles(
    "2015NB_25ns_L0Filt0x1600",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2015NB_25ns_L0Filt0x1600/2015NB_25ns_0x1600_0.mdf'],
    {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.04.13",
        "Application": "Moore",
        "Simulation" : False,
        "QMTests": [],
        "DDDB"  : "dddb-20150724",
        "CondDB" :"cond-20150828"
        },
    "2015NB data that has been filtered through 0x1600",
    test_file_db
    )

testfiles(
    myname="2016NB_25ns_L0Filt0x1609",
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609/2016NB_0x1609_%s.mdf' %f for f in ['174822_01','174823_01','174824_01','175266_01','175266_02','175269_02','175363_01','175364_01','175364_02','175431_02','175492_02','175585_01','175585_02','175589_01']],
    qualifiers={'Author': 'Conor Fitzpatrick', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False,
                "DDDB"  : "dddb-20150724",
                "CondDB" :"cond-20160517",
                "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
                'Date': '2016-06-18', 'Application': 'Moore'},
    comment='2015NB data that has been filtered through 0x1609 with L0App',
    test_file_db=test_file_db
    )

testfiles(
    myname="2016NB_25ns_L0Filt0x160B",
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160B/2016NB_0x160B_%s.mdf'%i for i in ['174819_01','174822_01','174823_01','174824_01','175266_01','175266_02','175269_01','175269_02','175359_01','175363_01','175364_01','175364_02','175385_01','175431_01','175431_02','175434_01','175492_01','175492_02','175497_01','175499_01','175580_01','175584_01','175585_01','175585_02','175589_01']],
    qualifiers={'Author': 'Conor Fitzpatrick, Mika Vesterinen', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False,
                "DDDB"  : "dddb-20150724",
                "CondDB" :"cond-20160517",
                "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
                'Date': '2016-07-25', 'Application': 'Moore'},
    comment='2016NB data that has been filtered through 0x160B with L0App',
    test_file_db=test_file_db
    )

testfiles(
    "2015_pbpb_raw_full",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2015/RAW/FULL/LHCb/LEAD15/169329/169329_0000000062.raw'],
    {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2015",
        "Date": "2016.02.01",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["brunel.2015magdown"],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150828",
    },
    "Lead-Lead collision raw data FULL stream, run 169329 from 8th December 2015",
    test_file_db
    )

testfiles(
    "2015NB_25ns_L0Filt0xFF61",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2015NB_25ns_L0Filt0xFF61/2015NB_25ns_0xFF61_'+str(n)+'.mdf' for n in range(0,15)],
    {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.02.17",
        "Application": "Moore",
        "Simulation" : False,
        "QMTests": [],
        "DDDB"  : "dddb-20150724",
        "CondDB" :"cond-20150828"
        },
    "2015NB data that has been filtered through 0xFF61",
    test_file_db
    )

testfiles(
    "pNe2015NB_L0Filt0x1608_small",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_BB_small.mdf',  # from 500 nobias events, run 161119
     'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_BE_small.mdf',  # from 2500 nobias events
     'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_EB_small.mdf',  # from 2500 nobias events
     ],
    {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.06.01",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150805",
        "InitialTime": "2015-08-25 03:00:00 UTC",  # during run 161119
        },
    "2015 pNe nobias data that has been filtered through 0x1608 (few hundred events)",
    test_file_db
    )

testfiles(
    "pNe2015NB_BE",
    ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe_NoBias_BEXing/{0}/output/protonNeon_NoBias.raw'.format(i)
     for i in set(range(161, 235)).difference([168,171,185,188,189,194,195,196,198,201,210,211,215])],
    {
        "Author": "Yanxi Zhang",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2017.11.02",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150805",
        "InitialTime": "2015-08-25 03:00:00 UTC",  # during run 161119
        },
    "2015 pNe nobias data that has been filtered through 0x1608 (few hundred events)",
    test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2JpsiPhi_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_0.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_1.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_2.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_3.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_4.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_6.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_7.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_8.dst',],
    qualifiers={'Format': 'DST', 'Author': 'egovorko', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15Dev', 'Date': '2017-02-13', 'Application': 'Moore'},
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2JpsiPhi_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_0.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_1.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_2.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_3.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_5.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_6.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_7.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_8.dst',],
    qualifiers={'Format': 'DST', 'Author': 'egovorko', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15Dev', 'Date': '2017-02-13', 'Application': 'Moore'},
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2Psi2SPhi_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_0.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_1.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_2.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_3.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_5.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_6.dst',],
    qualifiers={'Format': 'DST', 'Author': 'egovorko', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15Dev', 'Date': '2017-02-13', 'Application': 'Moore'},
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2Psi2SPhi_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_0.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_1.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_2.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_3.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_4.dst',],
    qualifiers={'Format': 'DST', 'Author': 'egovorko', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15Dev', 'Date': '2017-02-13', 'Application': 'Moore'},
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_0.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_1.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_5.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_7.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_8.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_10.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_13.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_14.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_15.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_19.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_21.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_23.dst',],
    qualifiers={'Format': 'DST', 'Author': 'egovorko', 'DataType': '2015', 'Processing': 'Sim08g', 'Simulation': True, 'CondDB': 'sim-20150119-3-vc-md100', 'DDDB': 'dddb-20150119-3', 'Reconstruction': 'Reco15Dev', 'Date': '2017-02-13', 'Application': 'Moore'},
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_5.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_7.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_10.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_11.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_12.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_13.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_15.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_16.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_18.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_19.dst',
               'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_22.dst',],
    qualifiers={'Format': 'DST', 'Author': 'egovorko', 'DataType': '2015', 'Processing': 'Sim08g', 'Simulation': True, 'CondDB': 'sim-20150119-3-vc-mu100', 'DDDB': 'dddb-20150119-3', 'Reconstruction': 'Reco15Dev', 'Date': '2017-02-13', 'Application': 'Moore'},
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db
    )

###############  DUMMY FILES ###################

testfiles(
    "Dummy!!",
    ["/dev/null"],
    {
    "Author": "Rob Lambert",
    "Format": "DIGI",
    "DataType": "2011",
    "Date": "2013.05.31",
    },
    "Dummy test file, for testing the testfiles mechanism...",
    test_file_db
    )

###############  Auto-additions are appended to this file ###################
#
# There is now an automated mechanism to append to this database.
# See: $PRCONFIGROOT/scripts/AddToTestFilesDB.py


### auto created ###
testfiles(
    myname='2011_25ns_raw_default',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2011/RAW/FULL/LHCb/COLLISION11_25/103053/103053_0000000035.raw'],
    qualifiers={'Author': 'cattanem', 'DataType': '2011', 'Format': 'MDF', 'Simulation': False, 'Date': '2013-11-26 15:03:01.559482', 'DDDB':'dddb-20130503', 'CondDB': 'cond-20130522', 'QMTests': 'brunel.brunel_25ns'},
    comment='RAW data file with events from run 103053, fill 2186 on 2011-10-07, 25ns bunch spacing. DB tags are default 2011 tags in LHCb v36r3',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2010_MagUp_raw_default',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2010/RAW/FULL/LHCb/COLLISION10/69623/069623_0000000003.raw'],
    qualifiers={'Author': 'cattanem', 'DataType': '2010', 'Format': 'MDF', 'Simulation': False, 'Date': '2013-11-26 15:56:50.465614', 'DDDB':'head-20110721', 'CondDB': 'head-20110614', 'QMTests': 'brunel.brunel2010magup'},
    comment='Default RAW file for 2010 MagUp tests, 3500 GeV data. DB tags are default 2010 tags in LHCb v36r3',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2010_MagOff_raw_default',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2010/RAW/FULL/LHCb/COLLISION10/69947/069947_0000000004.raw'],
    qualifiers={'Author': 'cattanem', 'DataType': '2010', 'Format': 'MDF', 'Simulation': False, 'Date': '2013-11-26 16:01:05.210397', 'QMTests': 'brunel.brunel2010magoff'},
    comment='Default RAW file for 2010 MagOff tests, 3500 GeV data. DB tags are default 2010 tags in LHCb v36r3',
    test_file_db=test_file_db
    )


### auto created ###
testfiles(
    myname='2009_dst_read_test',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2009/DST/00006290/0000/00006290_00000001_1.dst'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'DST', 'Simulation': False, 'CondDB': 'head-20110614', 'DDDB': 'head-20101206', 'Date': '2014-01-21 10:37:23.196590', 'QMTests': 'ioexample.copy2009dsttoroot'},
    comment='Official POOL DST from 2009',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Reco10-sdst-10events',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Reco10-sdst-10events/00011652_00000001_1-evt-18641to18650.sdst'],
    qualifiers={'Format': 'SDST', 'Author': 'cattanem', 'DataType': '2011', 'Processing': 'Reco10', 'Simulation': False, 'CondDB': 'head-20110622', 'DDDB': 'head-20110302', 'Reconstruction': 10, 'Date': '2014-01-21 15:14:33.219187', 'Application': 'Brunel', 'QMTests': 'ioexample.testunpacktrack'},
    comment='10 events from Reco10 SDST, including one testing UnpackTrack bug fix in EventPacker v2r10',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC09-pool-dst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/MC09-pool-dst/00001820_00000001.dst'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20090402-vc-mu100', 'DDDB': 'head-20090330', 'Date': '2014-01-21 17:20:23.041861', 'Application': 'Brunel', 'QMTests': 'ioexample.copypooldsttoroot'},
    comment='100 minimum bias events produced with MC09 default settings, Gauss v37r0, Boole v18r0, Brunel v34r5',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC09-pool-digi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/MC09-pool-digi/30000000-100ev-20090407-MC09.digi'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20090508-vc-mu100', 'DDDB': 'head-20090508', 'Date': '2014-01-21 17:47:29.732855', 'Application': 'Boole', 'QMTests': 'ioexample.copypooldigitoroot'},
    comment='100 minimum bias events produced with MC09 default settings, Gauss v37r0, Boole v19r2',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC09-pool-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/MC09-pool-sim/30000000-100ev-20090407-MC09.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20090402-vc-mu100', 'DDDB': 'head-20090330', 'Date': '2014-01-21 17:51:25.261926', 'Application': 'Gauss', 'QMTests': 'ioexample.copypoolsimtoroot'},
    comment='100 minimum bias events produced with MC09 default settings, Gauss v37r0',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2012_raw_nu6',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2012/RAW/NOBIAS/LHCb/COLLISION12/125745/125745_0000000007.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2012/RAW/NOBIAS/LHCb/COLLISION12/125745/125745_0000000018.raw'],
    qualifiers={'Author': 'rlambert', 'DataType': '2012', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20120730', 'DDDB': 'head-20120413', 'Date': '2014-02-05 16:46:08.788714', 'Application': 'Moore'},
    comment='Run 125745, nu around 6, high-occupancy data for stress-testing Moore and timing tests of tracking.',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2011_smallfiles_EW',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000049_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010654_00000049_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000040_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000168_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000163_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010352_00000014_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010352_00000002_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000003_1.radiative.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00011760_00000130_1.dimuon.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_smallfiles_EW/00008385_00000552_1.bhadron.dst'],
    qualifiers={'Format': 'DST', 'Author': 'rlambert', 'DataType': '2011', 'Processing': 'Stripping13b', 'Simulation': False, 'CondDB': 'head-20111102', 'DDDB': 'head-20110302', 'Stripping': 'Stripping12', 'Reconstruction': 'Reco08', 'Date': '2014-02-12 11:43:35.705405', 'Application': 'DaVinci', 'QMTests': 'lhcbalgs.fsr-small-files-root'},
    comment='Very small files generated in S13b, which have a few events only, but a lot of FSRs potentially. Used to test if FSRs really work properly. Copied from the grid by rlambert in 2011.',
    test_file_db=test_file_db
    )

testfiles(
    myname='2010_justFSR_EW',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2010_justFSR_EW/JustFSR.2010.EW.dst'],
    qualifiers={'Format': 'DST', 'Author': 'rlambert', 'DataType': '2010', 'Processing': 'Stripping12', 'Simulation': False, 'CondDB': 'head-20101112', 'DDDB': 'head-20101026', 'Stripping': 'Stripping12', 'Reconstruction': 'Reco08', 'Date': '2014-02-12 11:43:35.705405', 'Application': 'DaVinci', 'QMTests': ['lhcbalgs.fsr-small-files-root','lhcbalgs.fsr-only-file.root']},
    comment='A file which contains nothing but FSRs. Early on in FSR development, files with only FSRs were not readable. This file is kept to test against future re-occurance of that.',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='boole.boole-mc11',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/boole.boole-mc11/Gauss-10000000-100ev-20111014.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': '2011', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20110723-vc-md100', 'DDDB': 'head-20110914', 'Date': '2014-02-13 09:02:22.212231', 'Application': 'Gauss', 'QMTests': 'boole.boole-mc11'},
    comment='Generic b events with spillover from Gauss v40r4, settings as in $APPCONFIGOPTS/Gauss/beam35000GeV-md100-MC11-nu2-50ns.py',
    test_file_db=test_file_db
    )

testfiles(
    myname='MC2012.dst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/2012/ALLSTREAMS.DST/00024784/0000/00024784_00000001_1.allstreams.dst'],
    qualifiers={'Author': 'jonrob', 'DataType': '2012', 'Format': 'DST', 'Simulation': True, 'CondDB': 'Sim08-20130326-1-vc-md100', 'DDDB': 'Sim08-20130326-1', 'Date': '2014-07-23 09:02:22.212231', 'Application': 'Brunel', 'QMTests': ['mc2012-testread']},
    comment='Example MC2012 DST (JpsiK)',
    test_file_db=test_file_db
    )

testfiles(
    myname='mc11a-xdst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/2011/XDST/00012935/0000/00012935_00000827_3.xdst'],
    qualifiers={'Author': 'cattanem', 'DataType': '2011', 'Format': 'XDST', 'Simulation': True, 'CondDB': 'sim-20111020-vc-md100', 'DDDB': 'head-20110914', 'Date': '2014-02-13 09:02:22.212231', 'Application': 'Brunel', 'QMTests': ['boole.boole-mc11a-xdst','brunel.brunel-mc11a-xdst']},
    comment='Inclusive b events Beam3500GeV-2011-MagDown-Nu2-50ns-EmNoCuts',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='boole.fixedfile-gauss-v39r4-mc10-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/boole.fixedfile-gauss-v39r4-mc10-sim/Gauss-v39r4-30000000-100ev-20110902-MC10.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': '2010', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20101210-vc-md100', 'DDDB': 'head-20101206', 'Date': '2014-02-13 10:04:53.289232', 'Application': 'Gauss', 'QMTests': ['boole.fixedfile-gauss-v39r4-mc10-sim','boole.boole-mc10','boole.boole-mc10-notruth','boole.boole-fest','boole.boole-2010-latestdb']},
    comment='MC10 file produced with Gauss v39r4',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='brunel-mc10',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-mc10/Boole-v21r8p1-MC2010Tuning.digi'],
    qualifiers={'Author': 'cattanem', 'DataType': '2010', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20101026-vc-mu100', 'DDDB': 'head-20101026', 'Date': '2014-02-13 16:13:31.976938', 'Application': 'Boole', 'QMTests': 'brunel.brunel-mc10-withtruth'},
    comment='Boole v21r8p1 digi file for 2010 MC tuning',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009/Boole-v19r8-30000000-100ev-20091112-vc-md100.digi'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-13 17:21:20.645501', 'Application': 'Boole', 'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi'},
    comment='100 minimum bias events, 2009 geometry, with Boole v19r8',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='brunel.fixedfile-sim2010',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel.fixedfile-sim2010/Boole-v21r9-00007107_00000001_2.digi'],
    qualifiers={'Author': 'cattanem', 'DataType': '2010', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20100624-vc-md100', 'DDDB': 'head-20100624', 'Date': '2014-02-13 17:43:36.579037', 'Application': 'Boole', 'QMTests': ['brunel.fixedfile-sim2010',  'brunel.fixedfile-sim2010-globaldb', 'brunel.fixedfile-sim2010-localdb']},
    comment='Simulation of 2010 geometry with Boole v21r9',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-mdf',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-mdf/Boole-v19r8-30000000-100ev-20091112-vc-md100.mdf'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'MDF', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-13 17:56:16.931913', 'Application': 'Boole', 'QMTests': 'brunel.fixedfile-boole-v19r8-2009-mdf'},
    comment='100 minimum bias events, 2009 geometry, with Boole v19r8, mdf format - no MC truth',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-extended',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-extended/Boole-v19r8-30000000-100ev-20091112-vc-md100-extended.digi'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-13 17:59:26.133915', 'Application': 'Boole', 'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi-extended'},
    comment='100 minimum bias events, 2009 geometry, with Boole v19r8, xdigi format',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-extended-spillover',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-extended-spillover/Boole-v19r8-30000000-100ev-20091112-vc-md100-extended-spillover.digi'],
    qualifiers={'Author': 'cattanem', 'DataType': '2009', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-13 18:01:51.972516', 'Application': 'Boole', 'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi-extended-spillover'},
    comment='100 minimum bias events, 2009 geometry, with Boole v19r8, with spillover, xdigi',
    test_file_db=test_file_db
    )

testfiles(
    myname='R08S14_smallfiles',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_5.dst'],
    qualifiers={'Format': 'DST', 'Author': 'rlambert', 'DataType': '2010', 'Processing': 'R08S14', 'Simulation': False, 'CondDB': 'head-20100531', 'DDDB': 'head-20100531', 'Stripping': 14, 'Reconstruction': 8, 'Date': '2014-02-14 11:08:29.713073', 'Application': 'DaVinci', 'QMTests': 'lhcbalgs.merge-small-files'},
    comment='Small files extracted from the grid for running FSR merging tests',
    test_file_db=test_file_db
    )


testfiles(
    myname='brunel-v35r8-2009',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-v35r8-2009/Brunel-v35r8-30000000-100ev-20091112-vc-md100.dst'],
    qualifiers={'Format': 'DST', 'Author': 'rlambert', 'DataType': 'MC09', 'Processing': 'MC09', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-17 14:29:37.350134', 'Application': 'DaVinci', 'QMTests': 'davinci.fixedfile'},
    comment='Brunel output in 2009 checking that DaVinci is still able to read it',
    test_file_db=test_file_db
    )

testfiles(
    myname='brunel-v35r8-2009-wtruth',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-v35r8-2009-wtruth/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'rlambert', 'DataType': 'MC09', 'Processing': 'MC09', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-17 14:24:01.596343', 'Application': 'DaVinci', 'QMTests': 'davinci.fixedfile'},
    comment='Tests that DaVinci can still read very old file formats, fixedfile dst from 2009 with MCTruth',
    test_file_db=test_file_db
    )

testfiles(
    myname='brunel-v35r8-2009-xdst-wtruth',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-v35r8-2009-xdst-wtruth/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth.xdst'],
    qualifiers={'Format': 'XDST', 'Author': 'rlambert', 'DataType': 'MC09', 'Processing': 'MC09', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-17 14:24:01.596343', 'Application': 'DaVinci', 'QMTests': 'davinci.fixedfile'},
    comment='Tests that DaVinci can still read very old file formats, fixedfile xdst from 2009 with MCTruth',
    test_file_db=test_file_db
    )

testfiles(
    myname='brunel-v35r8-2009-xdst-wtruth-wspill',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-v35r8-2009-xdst-wtruth-wspill/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth-spillover.xdst'],
    qualifiers={'Format': 'XDST', 'Author': 'rlambert', 'DataType': 'MC09', 'Processing': 'MC09', 'Simulation': True, 'CondDB': 'sim-20091112-vc-md100', 'DDDB': 'head-20091112', 'Date': '2014-02-17 14:24:01.596343', 'Application': 'DaVinci', 'QMTests': 'davinci.fixedfile'},
    comment='Tests that DaVinci can still read very old file formats, fixedfile xdst from 2009 with MCTruth, with spillover',
    test_file_db=test_file_db
    )

testfiles(
    myname='brunel-v37r3-fsrs-md',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-v37r3-fsrs-md/Brunel-v37r3-20ev-FSR1.dst'],
    qualifiers={'Author': 'rlambert', 'DataType': '2010', 'Format': 'DST', 'Simulation': False, 'CondDB': 'head-20110614', 'DDDB': 'head-20110721', 'Date': '2014-02-17 14:46:53.777561', 'Application': 'DaVinci', 'QMTests': 'davinci.fsrs'},
    comment='Input test file with FSRs for checking the streaming procedires of the stripping in DaVinci. Magnet Down.',
    test_file_db=test_file_db
    )

testfiles(
    myname='brunel-v37r3-fsrs-mu',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-v37r3-fsrs-mu/Brunel-v37r3-20ev-FSR2.dst'],
    qualifiers={'Author': 'rlambert', 'DataType': '2010', 'Format': 'DST', 'Simulation': False, 'CondDB': 'head-20110614', 'DDDB': 'head-20110721', 'Date': '2014-02-17 14:46:53.777561', 'Application': 'DaVinci', 'QMTests': 'davinci.fsrs'},
    comment='Input test file with FSRs for checking the streaming procedires of the stripping in DaVinci. Magnet Up.',
    test_file_db=test_file_db
    )
testfiles(
    myname='brunel-v37r1-sdst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/brunel-v37r1-sdst/Brunel-v37r1-069857_0000000006-1000ev.sdst'],
    qualifiers={'Author': 'rlambert', 'DataType': '2010', 'Format': 'SDST', 'Simulation': False, 'CondDB': 'head-20110614', 'DDDB': 'head-20110721', 'Date': '2014-02-17 14:46:53.777561', 'Application': 'DaVinci', 'QMTests': 'davinci.io'},
    comment='Test SDST from Brunel v37r1 in DaVinci, also links to a raw file, see the catalog in DaVinciTests/tests.options/TestSDSTCatalog.xml.',
    test_file_db=test_file_db
    )

testfiles(
    myname='MC2010_BdJPsiKs',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/MC2010_BdJPsiKs/MC2010_BdJPsiKs_00008414_00000106_1.dst'],
    qualifiers={'Author': 'jonrob', 'DataType': '2010', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20100715-vc-md100', 'DDDB': 'head-20100624', 'Date': '2014-03-07 17:16:03.678324', 'Application': 'Brunel'},
    comment='DaVinci MC2010 decaytreeTuple test',
    test_file_db=test_file_db
    )

testfiles(
    myname='Brunel-SIM08-MinBias-Beam6500GeV-md100-nu4.8.xdst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Brunel-SIM08-MinBias-Beam6500GeV-md100-nu4.8.xdst/Brunel_Beam6500GeV-md100-nu4.8.xdst'],
    qualifiers={'Format': 'XDST', 'Author': 'dhcroft', 'DataType': 'XDST', 'Processing': 'SIM08', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Stripping': None, 'Reconstruction': 'Brunelv44r7', 'Date': '2014-03-21 16:28:57.998963', 'Application': 'Brunel', 'QMTests': 'boole-sim08-xdst.qmt'},
    comment='1000 MinBias XDST events used to test reprocessing in Boole of XDSTs',
    test_file_db=test_file_db
    )

testfiles(
    myname='2010-Raw-data-testVetraErrorHandling',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2010-Raw-data-testVetraErrorHandling/2010_RAW_FULL_LHCb_COLLISION10_69984_069984_0000000001.raw'],
    qualifiers={'Author': 'dhcroft', 'DataType': 'MDF', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'head-20110524', 'DDDB': 'head-20110303', 'Date': '2014-04-03 18:36:32.282645', 'QMTests': 'vetra-moni-error-banks-2010'},
    comment='Normal raw data from 2010, used to test VETRA error handling',
    test_file_db=test_file_db
    )

testfiles(
    myname='2009-Raw-data-Vetra_NZS_NoTestPulse',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2009-Raw-data-Vetra_NZS_NoTestPulse/2009_RAW_FULL_VELOA_PHYSICSNTP_45445_045445_0000000001.raw'],
    qualifiers={'Author': 'dhcroft', 'DataType': 'MDF', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'head-20110524', 'DDDB': 'head-20110303', 'Date': '2014-04-04', 'QMTests': 'VetraMoni_NZS_NoTestPulse'},
    comment='Non-zero suppressed VELO data from 2009, no test pulses',
    test_file_db=test_file_db
    )

testfiles(
    myname='2010_RAW_FULL_VELO_NZS_75770_075770_0000000001.raw',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2010_RAW_FULL_VELO_NZS_75770_075770_0000000001.raw/2010_RAW_FULL_VELO_NZS_75770_075770_0000000001.raw'],
    qualifiers={'Author': 'dhcroft', 'DataType': 'MDF', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'head-20110524', 'DDDB': 'head-20110303', 'Date': '2014-04-04', 'QMTests': 'VetraMoni_NZS_RoundRobin_100'},
    comment='Non-zero suppressed VELO data from 2010: taken in round robin mode',
    test_file_db=test_file_db
    )

testfiles(
    myname='2010_RAW_FULL_VELO_TESTPULSENZS_71649_071649_0000000001.raw',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2010_RAW_FULL_VELO_TESTPULSENZS_71649_071649_0000000001.raw/2010_RAW_FULL_VELO_TESTPULSENZS_71649_071649_0000000001.raw'],
    qualifiers={'Author': 'dhcroft', 'DataType': 'MDF', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'head-20110524', 'DDDB': 'head-20110303', 'Date': '2014-04-04', 'QMTests': 'VetraMoni_NZS_RoundRobin_1'},
    comment='Non-zero suppressed VELO testpulse data from 2010: taken in round robin mode',
    test_file_db=test_file_db
    )

testfiles(
    myname='2011_RAW_CALIB_LHCb_COLLISION11_92222_092222_0000000003.raw',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_RAW_CALIB_LHCb_COLLISION11_92222_092222_0000000003.raw/2011_RAW_CALIB_LHCb_COLLISION11_92222_092222_0000000003.raw'],
    qualifiers={'Author': 'dhcroft', 'DataType': 'MDF', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'head-20110524', 'DDDB': 'head-20110303', 'Date': '2014-04-04', 'QMTests': 'VetraMoni_NZS_Stream_201105'},
    comment='Non-zero suppressed VELO data from 2011',
    test_file_db=test_file_db
    )

testfiles(
    myname='2010_RAW_FULL_LHCb_COLLISION10_69354_069354_0000000003.raw',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2010_RAW_FULL_LHCb_COLLISION10_69354_069354_0000000003.raw/2010_RAW_FULL_LHCb_COLLISION10_69354_069354_0000000003.raw'],
    qualifiers={'Author': 'dhcroft', 'DataType': 'MDF', 'Format': 'MDF',
                'Simulation': False, 'CondDB': 'head-20110524', 'DDDB': 'head-20110303',
                'Date': '2014-04-04', 'QMTests': 'VetraMoni_ZS_2010'},
    comment='Non-zero suppressed VELO data from 2011',
    test_file_db=test_file_db
    )

testfiles(
    myname='2011_RAW_FULL_LHCb_COLLISION11_92222_092222_0000000005.raw',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/2011_RAW_FULL_LHCb_COLLISION11_92222_092222_0000000005.raw/2011_RAW_FULL_LHCb_COLLISION11_92222_092222_0000000005.raw'],
    qualifiers={'Author': 'dhcroft', 'DataType': 'MDF', 'Format': 'MDF',
                'Simulation': False, 'CondDB': 'head-20110524', 'DDDB': 'head-20110303',
                'Date': '2014-04-04', 'QMTests': 'VetraMoni_ZS_201105'},
    comment='LHCb raw data from May 2011',
    test_file_db=test_file_db
    )


testfiles(
    myname='MC10-MinBias',
    filenames=['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod//lhcb/swtest/lhcb/MC/MC10/ALLSTREAMS.DST/00008898/0000/00008898_00000002_1.allstreams.dst'],
    qualifiers={'Format': 'DST', 'Author': 'cattanem', 'DataType': '2010', 'Processing': 'MC10', 'Simulation': True, 'CondDB': 'sim-20101210-vc-md100', 'DDDB': 'head-20101206', 'Reconstruction': '8', 'Date': '2014-04-11 10:04:18.947716', 'QMTests': 'ioexample.copymc10dsttoroot'},
    comment='MC10 minimum bias DST',
    test_file_db=test_file_db
    )

testfiles(
    myname='Reco08-charm.mdst',
    filenames=['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2010/CHARM.MDST/00008397/0000/00008397_00000939_1.charm.mdst'],
    qualifiers={'Format': 'DST', 'Author': 'cattanem', 'DataType': '2010', 'Processing': 'Reco08', 'Simulation': False, 'CondDB': 'head-20101112', 'DDDB': 'head-20101206', 'Reconstruction': '8', 'Stripping': '12', 'Date': '2014-04-11 10:04:18.947716', 'QMTests': 'ioexample.copyreco08mdsttoroot'},
    comment='2010 Reco08 CHARM.MDST',
    test_file_db=test_file_db
    )

testfiles(
    myname='Reco08-bhadron.dst',
    filenames=['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2010/BHADRON.DST/00008399/0000/00008399_00001052_1.bhadron.dst'],
    qualifiers={'Format': 'DST', 'Author': 'cattanem', 'DataType': '2010', 'Processing': 'Reco08', 'Simulation': False, 'CondDB': 'head-20101112', 'DDDB': 'head-20101206', 'Reconstruction': '8', 'Stripping': '12', 'Date': '2014-04-11 10:04:18.947716', 'QMTests': 'ioexample.copyreco08dsttoroot'},
    comment='2010 Reco08 BHADRON.DST',
    test_file_db=test_file_db
    )

testfiles(
    myname='R12S17-bhadron.dst',
    filenames=['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/BHADRON.DST/00012545/0000/00012545_00000003_1.bhadron.dst'],
    qualifiers={'Format': 'DST', 'Author': 'cattanem', 'DataType': '2011', 'Processing': 'R21S17', 'Simulation': False, 'CondDB': 'head-20110914', 'DDDB': 'head-20110914', 'Reconstruction': '12', 'Stripping': '17', 'Date': '2014-04-11 10:04:18.947716', 'QMTests': 'ioexample.copyreco12stripping17dsttoroot'},
    comment='2011 R12S17 BHADRON.DST',
    test_file_db=test_file_db
    )

testfiles(
    myname='R12S17-charm.mdst',
    filenames=['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/CHARM.MDST/00012547/0000/00012547_00000013_1.charm.mdst'],
    qualifiers={'Format': 'MDST', 'Author': 'cattanem', 'DataType': '2011', 'Processing': 'R21S17', 'Simulation': False, 'CondDB': 'head-20110914', 'DDDB': 'head-20110914', 'Reconstruction': '12', 'Stripping': '17', 'Date': '2014-04-11 10:04:18.947716', 'QMTests': 'ioexample.copyreco12stripping17mdsttoroot'},
    comment='2011 R12S17 CHARM.MDST',
    test_file_db=test_file_db
    )

testfiles(
    "R14S20-bhadron.mdst",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00020198/0000/00020198_00000758_1.bhadron.mdst"],
    {
    "Author": "cattanem",
    "Format": "MDST",
    "DataType": "2012",
    "Date": "2014.04.11",
    "Processing": "R14S20",
    "Reconstruction":14,
    "Stripping":20,
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["ioexample.copyreco14stripping20mdsttoroot"],
    "DDDB":"dddb-20120831",
    "CondDB": "cond-20120831"
    },
    "2012 R14S20 BHADRON.MDST",
    test_file_db
    )


testfiles(
    myname='upgrade-reprocessing-xdst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/Upgrade/XDST/00032467/0000/00032467_00000002_1.xdst'],
    qualifiers={'Format': 'XDST',
                'Author': 'yamhis',
                'DataType': 'Upgrade',
                'Processing': 'Step-125740',
                'Simulation': True,
                'CondDB': 'sim-20130830-vc-md100',
                'DDDB': 'dddb-20131025',
                'Reconstruction': '14',
                'Date': '2014-06-18 17:56:23.229933',
                'Application': 'Boole',
                'QMTests': 'boole-reprocess-xdst.qmt'},
    comment='xdst for reprocessing of upgrade MC sample',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-25ns-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-baseline-25ns-sim/Baseline-25ns.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20131108-vc-md100', 'DDDB': 'dddb-20131108', 'Date': '2014-06-23 11:02:41.522819', 'QMTests': 'boole-upgrade-baseline-25ns.qmt'},
    comment='Simulation of 2014 baseline for upgrade detectors, with spillover',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-25ns-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-baseline-25ns-xdigi/Baseline-25ns.xdigi'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20131108-vc-md100', 'DDDB': 'dddb-20131108', 'Date': '2014-06-23 11:02:41.522819', 'QMTests': 'brunel-upgrade-baseline-25ns.qmt'},
    comment='Digitisation of 2014 baseline for upgrade detectors, with spillover',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-baseline-sim/Baseline.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20131108-vc-md100', 'DDDB': 'dddb-20131108', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'boole-upgrade-baseline.qmt'},
    comment='Simulation of 2014 baseline for upgrade detectors',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-baseline-xdigi/Baseline.xdigi'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20131108-vc-md100', 'DDDB': 'dddb-20131108', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'brunel-upgrade-baseline.qmt'},
    comment='Digitisation of 2014 baseline for upgrade detectors',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-ut-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-baseline-ut-sim/Baseline-UT.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20130830-vc-md100', 'DDDB': 'dddb-20131025', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'boole-upgrade-baseline-ut.qmt'},
    comment='Simulation of 2014 baseline for upgrade detectors plus UT',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-ut-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-baseline-ut-xdigi/Baseline-UT.xdigi'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20130830-vc-md100', 'DDDB': 'dddb-20131025', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'brunel-upgrade-baseline-ut.qmt'},
    comment='Digitisation of 2014 baseline for upgrade detectors plus UT',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-minimal-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-minimal-sim/Minimal.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130808', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'boole-upgrade-minimal.qmt'},
    comment='Simulation for studies of minimal upgrade detector',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-minimal-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-minimal-xdigi/Minimal.xdigi'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130808', 'Application': 'Brunel', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'brunel-upgrade-minimal.qmt'},
    comment='Digitisation for studies of minimal upgrade detector',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-calo_nospdprs-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-calo_nospdprs-sim/Calo_NoSpdPrs.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130808', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'boole-upgrade-calo_nospdprs.qmt'},
    comment='Simulation for studies of minimal upgrade detector without spd and prs',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-calo_nospdprs-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-calo_nospdprs-xdigi/Calo_NoSpdPrs.xdigi'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130808', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'brunel-upgrade-calo_nospdprs.qmt'},
    comment='Digitisation for studies of minimal upgrade detector without spd and prs',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-muon_nom1-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-muon_nom1-sim/Muon_NoM1.sim'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130808', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'boole-upgrade-muon_nom1.qmt'},
    comment='Simulation for studies of minimal upgrade detector without M1',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-muon_nom1-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade-muon_nom1-xdigi/Muon_NoM1.xdigi'],
    qualifiers={'Author': 'cattanem', 'DataType': 'Upgrade', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130808', 'Date': '2014-06-23 12:47:20.529593', 'QMTests': 'brunel-upgrade-muon_nom1.qmt'},
    comment='Digitisation for studies of minimal upgrade detector without M1',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-reprocessing-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/Upgrade/XDIGI/00027904/0000/00027904_00000001_2.xdigi'],
    qualifiers={'Author': 'yamhis', 'DataType': 'Upgrade', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130806', 'Date': '2014-07-02 10:43:43.592234', 'Application': 'Boole', 'QMTests': 'boole-reprocess-xdigi.qmt'},
    comment='files to test reprocessing of xdigi files in Boole',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC2015_RecKstee',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/MCFILTER.DST/00033103/0000/00033103_00000006_1.mcfilter.dst'],
    qualifiers={'Author': 'sneubert', 'DataType': '2012', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2014-08-05 17:14:15.919588', 'Application': 'Brunel'},
    comment='Standard signal MC for 2015 tests',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC2015_digi_nu2.6_B2Kstmumu',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034730/0000/00034730_00000003_1.xdigi'],
    qualifiers={'Author': 'sneubert', 'DataType': '2012', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2014-08-28 11:09:54.975311'},
    comment='MC2015 signal for tracking studies',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC2015_digi_nu2.6_B2Kstmumu_L0',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/digi/MC2015_digi_nu2.6_B2Kstmumu_L0.dst'],
    qualifiers={'Author': 'sneubert', 'DataType': '2012', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2014-08-28 15:36:15.365588'},
    comment='L0 processed digi file for 2015 tracking studies',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='upgrade-rotnm_foil_gbt-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319117/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319127/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319139/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319157/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319176/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319198/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319211/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319224/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319234/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319236/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319237/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319254/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319263/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319275/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319284/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319293/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319303/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319314/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319320/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319335/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319341/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319359/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319382/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319392/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319409/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319413/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319419/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319422/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319440/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319444/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319447/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319450/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319495/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319502/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319506/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319516/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319527/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319534/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319557/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319569/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319585/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319592/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319597/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319603/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319622/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319632/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319639/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319651/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319664/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319675/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319684/OctTest-Extended.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/user/t/tbird/2014_07/82319/82319692/OctTest-Extended.digi'],
    qualifiers={'Author': 'lben', 'DataType': 'Upgrade', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20130722-vc-md100', 'DDDB': 'dddb-20130806', 'Date': '2014-08-21 10:04:11.651755', 'Application': 'Brunel'},
    comment='Test files for the Velo Pixel created by Tim Head',
    test_file_db=test_file_db
    )



### auto created ###
testfiles(
    myname='MC2015_digi_nu1.6_minbias',
    filenames=[
"root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000018_1.xdigi",
"root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000016_1.xdigi",
"root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000006_1.xdigi",
"root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000001_1.xdigi",
    ],
    qualifiers={'Author': 'sneubert', 'DataType': '2012', 'Format': 'XDIGI', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2014-09-13 14:49:04.554731'},
    comment='2015 minbias monte carlo. xdigi file for trigger studies',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2012_CaloFemtoDST',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000001_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000002_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000003_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000004_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000005_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000006_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000007_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000008_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000009_1.fmdst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000010_1.fmdst'],
    qualifiers={'Author': 'jonesc', 'DataType': '2012', 'Format': 'FMDST', 'Simulation': False, 'CondDB': 'cond-20140604', 'DDDB': 'dddb-20130929-1', 'Date': '2014-09-29 12:59:33.800587'},
    comment='CALO FemtoDST tests',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bd2Kpi_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_10.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_11.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_12.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_13.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_14.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_15.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_16.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_17.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_18.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_19.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_20.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_21.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_22.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_23.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_24.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_25.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_26.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_27.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_28.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_29.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_30.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_7.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_8.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_9.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_10.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_11.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_12.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_13.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_14.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_15.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_16.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_17.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_18.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_19.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_20.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_21.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_22.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_23.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_24.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_25.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_26.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_27.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_28.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_29.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_7.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_8.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_9.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-05 17:11:42.330871', 'Application': 'Moore'},
    comment='MC2015 samples of Bd2Kpi, processed for Run II trigger studies, MagUp and MagDown, for the BnoC group',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_10.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_11.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_12.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_13.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_7.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_8.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_9.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_10.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_11.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_12.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_13.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_14.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_15.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_16.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_17.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_18.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_7.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_8.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_9.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-06 15:02:16.232429', 'Application': 'Moore'},
    comment='MC2015 samples of Bs2PhiPhi, processed for Run II trigger studies, MagUp and MagDown, for the BnoC group',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_2015_D2KKPi_PrompyOnly_MagDown.dst',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.21263002_MagDown.dst'],
    qualifiers={'Author': 'sreicher', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-10 10:21:12.705113', 'Application': 'Moore_v23r1'},
    comment='Prompt only D->KKPi candidates (event tupe 21263002), pT > 1 GeV, MagDown for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_2015_D2KKPi_PrompyOnly_MagUp.dst',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.21263002_MagUp.dst'],
    qualifiers={'Author': 'sreicher', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-10 10:23:23.084335', 'Application': 'Moore_v23r1'},
    comment='Prompt only D->KKPi candidates (event tupe 21263002), pT > 1 GeV, MagUp for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_Dst2DPi2KsKK_PromptOnly_MagDown.dst',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.27265101_MagDown.dst'],
    qualifiers={'Author': 'sreicher', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-10 10:25:05.229285', 'Application': 'Moore_v23r1'},
    comment='Prompt only D*->D(->KsKK)pi candidates (event type 27265101) , pT > 1 GeV, MagDown for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_Dst2DPi2KsKK_PromptOnly_MagUp.dst',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.27265101_MagUp.dst'],
    qualifiers={'Author': 'sreicher', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-10 10:26:27.155885', 'Application': 'Moore_v23r1'},
    comment='Prompt only D*->D(->KsKK)pi candidates (event type 27265101) , pT > 1 GeV, MagUp for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KpiGamma_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-10 23:41:36.785427'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KpiGamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-10 23:43:18.187332'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KstGamma_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-10 23:46:16.965375'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KstGamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-10 23:47:24.451381'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Dst2D0Pi_D02mumu_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 09:54:50.836637'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Dst2D0Pi_D02mumu_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 09:55:45.142074'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2JpsiK_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 10:32:30.677116'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2JpsiK_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 10:33:04.978984'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_19.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:27:59.918947'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_14.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:28:48.020421'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2Lst1670Gamma_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:31:34.385704'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2Lst1670Gamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:32:13.313359'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagUp/L0processed_1.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:36:01.576431'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:37:22.269945'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_14.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:46:00.591309'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_19.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:46:56.208205'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_19.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_20.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_21.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_22.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_23.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_24.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_25.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_26.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_27.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_28.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_29.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_30.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_31.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_32.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_33.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:07:16.370383'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_19.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_20.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_21.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_22.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_23.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_24.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_25.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_26.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_27.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_28.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_29.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_30.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_31.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_32.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_33.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_34.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_35.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:07:53.465299'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Ksmumu_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:10:35.480375'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Ksmumu_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:11:11.895297'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2K1Gamma_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_6.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2012', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:13:07.964964'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2K1Gamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:13:39.124451'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2KstGamma_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:17:49.449453'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2KstGamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:18:24.882589'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiPhiGamma_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_6.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:20:39.519342'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiPhiGamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_6.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:21:09.569733'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_tau23mu_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:24:16.798859'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_tau23mu_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:25:20.157185'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Sigma2pmumu_MagUp_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:26:36.125797'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Sigma2pmumu_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_5.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 12:27:07.548354'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bd2Kspipi_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_5.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-14 16:39:05.628041', 'Application': 'Moore'},
    comment='MC2015 samples of Bd2Kspipi, processed for RUn II trigger studies, MagUp and MagDown, for the BnoC WG',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_Mc2015_Bu2KKpi_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_6.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2012', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-14 16:47:56.221845', 'Application': 'Moore'},
    comment='MC2015 samples of Bu2KKpi, processed for Run II trigger studies, MagUp and MagDown, for BnoC WG',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bu2KsK_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_6.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-14 16:55:22.711013', 'Application': 'Moore'},
    comment='MC2015 samples of Bu2KsK, processed for Run II trigger studies, MagUp and MagDown, for the BnoC WG',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bs2KKpi0_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_7.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_6.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'SIm08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-11-14 16:58:40.428013', 'Application': 'Moore'},
    comment='MC2015 samples of Bs2KKpi0, processed for Run II trigger studies, MagUp and MagDown, BnoC WG',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagUp_L0Processed',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_0.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_1.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_2.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_3.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_4.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_5.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_6.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_7.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_8.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_9.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_10.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_11.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_12.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_13.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_14.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_15.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_16.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_17.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_18.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_19.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_20.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_21.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_22.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_23.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_24.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_25.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_26.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_27.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_28.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_29.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_30.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_31.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_32.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_33.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-17 17:04:06.766959'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagDown_L0Processed',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_0.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_1.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_2.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_3.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_4.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_5.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_6.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_7.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_8.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_9.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_10.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_11.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_12.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_13.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_14.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_15.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_16.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_17.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_18.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_19.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_20.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_21.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_22.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_23.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_24.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_25.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_26.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_27.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_28.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_29.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_30.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_31.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_32.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_33.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_34.dst','root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_35.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-17 17:05:02.109845'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2OC_MC2015_DD_L0Processed_Strip21',
    filenames=['/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/6/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/6/output/L0BW.BhadronCompleteEvent.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jasmith', 'DataType': 'LDST', 'Processing': 'juggling_L0App_B2OCStripping21Lines', 'Simulation': True, 'CondDB': 'cond-20141007', 'DDDB': 'dddb-20130929-1', 'Stripping': 21, 'Date': '2014-11-18 11:24:18.858900', 'Application': 'Moore_v23r1,2'},
    comment='L0BWdivision',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2OC_MC2015_DsPi_L0Processed_Strip21',
    filenames=['/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/6/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/7/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/6/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/7/output/L0BW.BhadronCompleteEvent.dst'],
    qualifiers={'Author': 'jasmith', 'DataType': 'LDST', 'Format': 'DST', 'Simulation': True, 'CondDB': 'cond-20141007', 'DDDB': 'dddb-20130929-1', 'Date': '2014-11-18 11:30:58.072365', 'Application': 'Moore_v23r1,2'},
    comment='L0BWdivision',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2L0Gamma_MagUp_L0Processed',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_7.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-25 09:59:30.534281'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2L0Gamma_MagDown_L0Processed',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_7.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-25 10:00:35.357301'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bd2Kspipi_StripFiltered_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_0.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_10.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_11.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_12.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_2.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_3.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_4.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_5.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_6.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_7.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_8.dst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_9.dst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2014-12-15 13:58:03.930293', 'Application': 'Moore'},
    comment='MC2015 samples of Bd2Kspipi, filtered on stripping candidates, for Run II trigger studies, BnoC WG',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_Bs2JpsiPhi_L0Processed_Stripped_Selected',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_7.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_8.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_9.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_10.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_11.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_12.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_13.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_14.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_15.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_16.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_17.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_18.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_19.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_20.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_21.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_22.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_23.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_24.dst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2012', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Stripping': 20, 'Date': '2014-12-29 19:46:08.054146', 'Application': 'Moore_v23r2/DaVinci_v36r2'},
    comment='bw samples for run2 trigger optimisation',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_L0Processed_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_7.dst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2012', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2014-12-29 19:51:15.860076', 'Application': 'Moore_v23r2/DaVinci_v36r2'},
    comment='bw sample for run2 trigger optimisation',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_L0Processed_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_6.dst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2012', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2014-12-29 19:58:21.484033', 'Application': 'Moorev23r2/DaVinci_v36r2'},
    comment='bw division sample for run 2 trigger optimisation',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bd2Kpi_StripFiltered_TCK_FF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_17.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_18.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_19.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_20.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_21.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_9.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2015-01-22 12:49:11.533034', 'Application': 'Moore'},
    comment='MC2015 Bd2Kpi sample processed with L0 TCK 0xFF66',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bd2Kpi_StripFiltered_TCK_FF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_17.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_18.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_19.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_20.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_6.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_9.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Revo15DEV', 'Date': '2015-01-22 12:51:42.153368', 'Application': 'Moore'},
    comment='MC2015 Bd2Kpi sample processed with L0 TCK 0xFF66',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bs2PhiPhi_StripFiltered_TCK_FF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_17.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_18.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_19.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_20.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_21.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_22.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_23.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_25.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_26.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_27.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_28.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_29.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_30.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_31.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_32.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_6.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_9.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Revo15DEV', 'Date': '2015-01-22 12:53:54.596883', 'Application': 'Moore'},
    comment='MC2015 Bs2PhiPhi sample processed with L0 TCK 0xFF66',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bs2PhiPhi_StripFiltered_TCK_FF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_17.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_18.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_19.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_20.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_21.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_22.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_23.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_24.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_25.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_26.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_27.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_28.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_29.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_30.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_31.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_32.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_6.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_9.ldst'],
    qualifiers={'Format': 'DST', 'Author': 'jmccarth', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2015-01-22 12:55:22.005800', 'Application': 'Moore'},
    comment='MC2015 sample of Bs2PhiPhi processed with L0 TCK 0xFF67',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bu2Kpi0_MagUp',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_17.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_18.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_19.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_20.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_21.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_22.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_23.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_24.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_25.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_26.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_27.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_28.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_29.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_30.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_31.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_32.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_33.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_34.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_35.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_36.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_37.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_38.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_6.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_9.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'amerli', 'DataType': '2015', 'Processing': 'Sim08f', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2017-02-17 12:50', 'Application': 'Moore'},
    comment='MC2015 sample of Bu2Kpi0 MagUp no L0 Processed',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bu2Kpi0_MagDown',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_17.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_18.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_19.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_20.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_21.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_22.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_6.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_9.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'amerli', 'DataType': '2015', 'Processing': 'Sim08f', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2017-02-17 12:50', 'Application': 'Moore'},
    comment='MC2015 sample of Bu2Kpi0 MagDown no L0 Processed',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2016_B2pppp_MagDown',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_1.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_10.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_11.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_12.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_13.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_14.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_15.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_16.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_17.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_18.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_19.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_2.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_20.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_3.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_4.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_5.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_6.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_7.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_8.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_9.digi'],
    qualifiers={'Format': 'DIGI', 'Author': 'amerli', 'DataType': '2016', 'Processing': 'Sim09b', 'Simulation': True, 'CondDB': 'sim-20161124-2-vc-md100', 'DDDB': 'dddb-20150724', 'Reconstruction': '', 'Date': '2017-02-17 12:50', 'Application': 'Moore'},
    comment='MC2016 sample of B2ppbarppbar MagDown digitalized no L0 processed',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2016_B2pppp_MagUp',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_1.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_10.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_11.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_12.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_13.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_14.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_15.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_16.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_17.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_18.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_19.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_2.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_20.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_21.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_22.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_23.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_24.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_25.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_26.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_3.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_4.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_5.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_6.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_7.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_8.digi', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_9.digi'],
    qualifiers={'Format': 'DIGI', 'Author': 'amerli', 'DataType': '2016', 'Processing': 'Sim09b', 'Simulation': True, 'CondDB': 'sim-20161124-2-vc-mu100', 'DDDB': 'dddb-20150724', 'Reconstruction': '', 'Date': '2017-02-17 12:50', 'Application': 'Moore'},
    comment='MC2016 sample of B2ppbarppbar MagUp digitalized no L0 Processed',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='SLWG_MC2015_Bs2Dsmunu_L0Processed_TCK_FF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_6.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_9.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_17.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'powen', 'DataType': '2015', 'Processing': 'Sim08f', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2015-01-29 13:12:19.003247', 'Application': 'Moore'},
    comment='MC2015 samples of Bs2Dsmunu for HLT tuning (TCK = 0xFF67)',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='SLWG_MC2015_Bs2Dsmunu_L0Processed_TCK_FF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_3.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_4.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_5.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_6.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_7.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_8.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_9.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_10.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_11.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_12.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_13.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_14.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_15.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_16.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_17.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'powen', 'DataType': '2015', 'Processing': 'Sim08f', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2015-01-29 13:22:13.114037', 'Application': 'Moore'},
    comment='MC2015 samples of Bs2Dsmunu for HLT tuning (TCK = 0xFF66)',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='SLWG_MC2015_Lb2pmunu_L0Processed_TCK_FF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_3.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'powen', 'DataType': '2015', 'Processing': 'Sim08f', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2015-01-29 13:26:48.116227', 'Application': 'Moore'},
    comment='MC2015 samples of Lb2pmunu for HLT tuning (TCK = 0xFF66)',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='SLWG_MC2015_Lb2pmunu_L0Processed_TCK_FF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_0.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_1.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_2.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_3.ldst'],
    qualifiers={'Format': 'LDST', 'Author': 'powen', 'DataType': '2015', 'Processing': 'Sim08f', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'Reco15DEV', 'Date': '2015-01-29 13:29:22.650237', 'Application': 'Moore'},
    comment='MC2015 samples of Lb2pmunu for HLT tuning (TCK = 0xFF67)',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagUp_L0Processed_TCK66_Stripped_Selected',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_7.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_8.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_9.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_10.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_11.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_12.dst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Stripping': 20, 'Date': '2015-01-30 16:06:00.380193', 'Application': 'Moore_v23r3/DaVinci_v36r2'},
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagUp_L0Processed_TCK67_Stripped_Selected',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_7.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_8.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_9.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_10.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_11.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_12.dst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Stripping': 20, 'Date': '2015-01-30 16:11:14.974191', 'Application': 'Moore_v23r3/DaVinci_v36r2'},
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagDown_L0Processed_TCK66_Stripped_Selected',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_7.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_8.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_9.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_10.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_11.dst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Stripping': 20, 'Date': '2015-01-30 16:13:18.417172', 'Application': 'Moore_v23r3/DaVinci_v36r2'},
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagDown_L0Processed_TCK67_Stripped_Selected',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_0.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_1.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_2.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_3.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_4.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_5.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_6.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_7.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_8.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_9.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_10.dst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_11.dst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Stripping': 20, 'Date': '2015-01-30 16:16:29.454194', 'Application': 'Moore_v23r3/DaVinci_v36r2'},
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagUp_L0Processed_TCK66_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_6.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 20:46:44.385086', 'Application': 'Moore_v23r3'},
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagUp_L0Processed_TCK67_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_6.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 20:52:05.416118', 'Application': 'Moore_v23r3'},
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagDown_L0Processed_TCK66_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_6.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 20:55:32.773393', 'Application': 'Moore_v23r3'},
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagDown_L0Processed_TCK67_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_6.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 20:56:29.387313', 'Application': 'Moore_v23r3'},
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagUp_L0Processed_TCK66_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_6.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 21:03:02.494157', 'Application': 'Moore_v23r3'},
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagUp_L0Processed_TCK67_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_6.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 21:04:57.433179', 'Application': 'Moore_v23r3'},
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagDown_L0Processed_TCK66_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_6.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_7.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 21:06:29.221969', 'Application': 'Moore_v23r3'},
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagDown_L0Processed_TCK67_Stripped',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_0.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_1.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_2.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_3.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_4.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_5.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_6.ldst', 'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_7.ldst'],
    qualifiers={'Author': 'vsyropou', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2015-01-31 21:07:49.450345', 'Application': 'Moore_v23r3'},
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagDown_0xFF66',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.21263002_MagDown.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 10:53:15.485129', 'Application': 'Moorev23r2'},
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagUp_0xFF66',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.21263002_MagUp.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 11:06:34.347142', 'Application': 'Moorev23r2'},
    comment='filtred for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagDown_0xFF66',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.27265101_MagDown.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 11:10:22.670068', 'Application': 'Moorev23r2'},
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagUp_0xFF66',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.27265101_MagUp.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 11:12:26.388181', 'Application': 'Moorev23r2'},
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagDown_0xFF67',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.21263002_MagDown.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 11:57:11.521643', 'Application': 'Moorev23r2'},
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagUp_0xFF67',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.21263002_MagUp.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 12:12:09.734123', 'Application': 'Moorev23r2'},
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagDown_0xFF67',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.27265101_MagDown.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 12:14:49.386141', 'Application': 'Moorev23r2'},
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagUp_0xFF67',
    filenames=['/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.27265101_MagUp.ldst'],
    qualifiers={'Author': 'msokolof', 'DataType': 'LDST', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Reconstruction': 'reco15-dev', 'Date': '2015-02-06 12:16:40.744190', 'Application': 'Moorev23r2'},
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagUp_L0Processed_0xFF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_14.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:46:00.591309'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagDown_L0Processed_0xFF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_19.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:46:56.208205'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagUp_L0Processed_0xFF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagUp/juggled_1.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:36:01.576431'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagDown_L0Processed_0xFF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:37:22.269945'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagUp_L0Processed_0xFF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_19.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:27:59.918947'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagDown_L0Processed_0xFF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_14.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:28:48.020421'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagUp_L0Processed_0xFF66',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_7.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_8.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_9.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_10.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_11.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_12.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_13.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_14.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_15.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_16.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_17.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_18.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_19.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_20.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_21.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_22.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_23.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_24.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_25.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_26.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_27.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_28.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_29.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_30.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_31.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_32.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_33.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-17 17:04:06.766959'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagDown_L0Processed_0xFF66',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_0.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_1.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_2.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_3.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_4.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_5.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_6.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_7.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_8.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_9.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_10.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_11.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_12.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_13.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_14.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_15.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_16.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_17.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_18.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_19.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_20.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_21.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_22.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_23.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_24.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_25.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_26.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_27.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_28.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_29.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_30.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_31.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_32.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_33.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_34.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_35.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-17 17:05:02.109845'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagUp_L0Processed_0xFF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_0.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_1.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_2.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_3.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_4.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_5.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_6.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_7.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_8.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_9.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_10.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_11.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_12.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_13.dst','root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_14.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:46:00.591309'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagDown_L0Processed_0xFF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_19.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:46:56.208205'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagUp_L0Processed_0xFF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagUp/juggled_1.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:36:01.576431'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagDown_L0Processed_0xFF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_4.dst'],
    qualifiers={'Format': 'LDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:37:22.269945'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagUp_L0Processed_0xFF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_19.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:27:59.918947'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagDown_L0Processed_0xFF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_14.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-11 11:28:48.020421'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagUp_L0Processed_0xFF67',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_7.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_8.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_9.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_10.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_11.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_12.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_13.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_14.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_15.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_16.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_17.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_18.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_19.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_20.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_21.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_22.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_23.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_24.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_25.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_26.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_27.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_28.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_29.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_30.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_31.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_32.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_33.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-17 17:04:06.766959'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagDown_L0Processed_0xFF67',
    filenames=['root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_7.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_8.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_9.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_10.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_11.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_12.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_13.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_14.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_15.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_16.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_17.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_18.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_19.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_20.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_21.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_22.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_23.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_24.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_25.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_26.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_27.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_28.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_29.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_30.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_31.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_32.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_33.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_34.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_35.dst'],
    qualifiers={'Format': 'XDST', 'Author': 'apuignav', 'DataType': '2015', 'Processing': 'Sim08f-r2', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2014-11-17 17:05:02.109845'},
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC2015_MinBias_SPD_lt_420_md_4xKee_L0Filtered',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_000.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_001.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_002.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_003.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_004.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_005.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_006.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_007.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_008.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_009.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_010.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_011.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_012.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_013.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_014.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_015.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_016.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_017.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_018.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_019.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_020.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_021.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_022.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_023.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_024.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_025.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_026.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_027.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_028.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_029.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_030.ldst'],
    qualifiers={'Author': 'raaij', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Date': '2015-03-26 23:04:28.631412'},
    comment='MC2015 MinBias samples for 2015 HLT commissioning rate tests',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='MC2015_MinBias_SPD_lt_420_mu_4xKee_L0Filtered',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_000.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_001.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_002.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_003.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_004.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_005.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_006.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_007.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_008.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_009.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_010.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_011.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_012.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_013.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_014.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_015.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_016.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_017.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_018.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_019.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_020.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_021.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_022.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_023.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_024.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_025.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_026.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_027.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_028.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_029.ldst', 'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_030.ldst'],
    qualifiers={'Author': 'raaij', 'DataType': '2015', 'Format': 'LDST', 'Simulation': True, 'CondDB': 'sim-20140730-vc-mu100', 'DDDB': 'dddb-20140729', 'Date': '2015-03-26 23:11:09.961390'},
    comment='MC2015 MinBias samples for 2015 HLT commissioning rate tests',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='Tesla_Bsphiphi_MC12wTurbo',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/RemadeReports_HEAD_14-1-15_Bsphiphi_1k.dst'],
    qualifiers={'Format': 'DST', 'Author': 'sbenson', 'DataType': '2012', 'Processing': 'Sim08plusTurbo', 'Simulation': True, 'CondDB': 'sim-20130503-vc-md100', 'DDDB': 'dddb-20130503', 'Stripping': 20, 'Reconstruction': 14, 'Date': '2015-03-27 23:28:13.682936', 'Application': 'DaVinci', 'QMTests': 'tesla.containers,tesla.default'},
    comment='check resurrection of decay from raw bank',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2gammagamma_MagDown_L0Processed',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BsGammaGamma_Stripped_Truth_0xFF66/MC15Dev_0_Truth.SeqBs2GG_none.dst'],
    qualifiers={'Format': 'DST', 'Author': 'sbenson', 'DataType': '2015', 'Processing': 65382, 'Simulation': True, 'CondDB': 'sim-20140730-vc-md100', 'DDDB': 'dddb-20140729', 'Stripping': 'Custom', 'Reconstruction': 'Reco15Dev', 'Date': '2015-05-24 12:41:17.316214', 'Application': 'Moore', 'QMTests': 'Moore_Neutral'},
    comment='LHCbIntegrationTests',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2015HLTValidationData_L0filtered',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_0.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_1.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_2.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_3.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_4.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_5.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_7.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_8.mdf', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_9.mdf'],
    qualifiers={'Author': 'ashires', 'DataType': '2015', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20150617', 'DDDB': 'dddb-20150526', 'Stripping': None, 'Reconstruction': None, 'Date': '2015-07-22 11:43:23.139081', 'Application': 'Moore', 'QMTests': None},
    comment='2015 no bias data that has passed the L0 for tuning the HLT (0xEE63)',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='2015HLTValidationData_L0filtered_0x0050',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0x0050/2015NB_L0filtered_0x0050.mdf'],
    qualifiers={'Author': 'raaij', 'DataType': '2015', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20150617', 'DDDB': 'dddb-20150526', 'Date': '2015-07-22 18:06:43.143827', 'Application': 'Moore'},
    comment='Validation of Moore for 25 ns running in August of 2015',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='TeslaTest_TCK_0x022600a2',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/TeslaTest_TCK_0x022600a2.dst'],
    qualifiers={'Author': 'sbenson', 'DataType': '2015', 'Format': 'DST', 'Simulation': False, 'CondDB': 'cond-20150828', 'DDDB': 'dddb-20150724', 'Date': '2016-03-29 18:05:33.296452', 'Application': 'Moore'},
    comment='For Tesla output container checks',
    test_file_db=test_file_db
    )

testfiles(
    myname='TeslaTest_2015raw_0xFF66',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/TurboRaw_L0_0xFF66.mdf'],
    qualifiers={'Author': 'sbenson', 'DataType': '2015', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20150828', 'DDDB': 'dddb-20150724', 'Date': '2016-04-07 23:41:33.296452', 'Application': 'Moore'},
    comment='For integration persist reco. tests',
    test_file_db=test_file_db
    )

testfiles(
    myname='TeslaTest_2016raw_0x11361609_0x21361609',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2016raw_0x11361609_0x21361609/179348_0000000227.raw'],
    qualifiers={'Author': 'mvesteri', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20160522', 'DDDB': 'dddb-20150724', 'InitialTime': '2016-07-07 03:54:16 UTC', 'Date': '2016-07-21', 'Application': 'Moore'},
    comment='A turbo raw file for tesla testing',
    test_file_db=test_file_db
    )

testfiles(
    myname="Juggled_MC_2015_27163003_DstToD0pip_D0ToKmPip",
    filenames = ['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SignalMC/Juggled_MC_2015_27163003_Beam6500GeVJun2015MagDownNu1.6Pythia8_Sim08h/juggled.dst'],
    qualifiers={'Author': 'mvesteri', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100','DDDB': 'dddb-20130929-1' , 'Date': '2016-05-18', 'Application': 'Moore'},
    comment='Signal MC sample for D*->D0pi, D0->Kpi. To test efficiency version of Moore_RateTest.py',
    test_file_db=test_file_db
    )

testfiles(
    myname="MC_2015_27163003_DstToD0pip_D0ToKmPip",
    filenames = ['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SignalMC/MC_2015_27163003_Beam6500GeVJun2015MagDownNu1.6Pythia8_Sim08h/00046271_00000135_2.dst'],
    qualifiers={'Author': 'mvesteri', 'DataType': '2015', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20131023-vc-md100','DDDB': 'dddb-20130929-1' , 'Date': '2016-05-18', 'Application': 'Moore'},
    comment='Signal MC sample for D*->D0pi, D0->Kpi. To test efficiency version of Moore_RateTest.py',
    test_file_db=test_file_db
    )




testfiles(
    myname="2016NB_25ns_L0Filt0x160D",
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174819_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174822_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174823_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174824_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175266_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175266_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175269_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175269_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175359_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175363_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175364_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175364_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175385_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175431_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175431_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175434_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175492_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175492_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175497_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175499_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175580_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175585_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175585_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175589_01.mdf'],
     qualifiers={
        'Author': 'Conor Fitzpatrick',
        'DataType': '2016', 'Format': 'MDF', 'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-06-21',
        'Application': 'Moore'},
     comment='2016NB data that has been filtered through 0x160D with L0App',
     test_file_db=test_file_db
    )

testfiles(
    myname="2016NB_25ns_L0Filt0x1609_large",
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174819_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174822_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174823_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174824_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175266_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175266_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175269_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175269_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175359_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175363_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175364_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175364_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175385_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175431_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175431_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175434_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175492_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175492_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175497_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175499_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175580_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175585_01.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175585_02.mdf',
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175589_01.mdf'],
     qualifiers={
        'Author': 'Conor Fitzpatrick',
        'DataType': '2016', 'Format': 'MDF', 'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB':'cond-20160517',
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-06-21', 'Application': 'Moore'},
     comment='Larger sample of 2016NB data that has been filtered through 0x1609 with L0App',
     test_file_db=test_file_db
    )

testfiles(
    myname = "HltDAQ-routingbits_nobias",
    filenames = ['mdf:root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/HltDAQ-routingbits_nobias/174909_0000000095_nobias.raw'],
    qualifiers={'Author': 'Roel Aaij', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'DDDB': 'dddb-20150724', 'CondDB':'cond-20160517', 'InitialTime': '2016-05-21 01:42:00 UTC', 'Date': '2016-06-28', 'Application': 'LHCb'},
    comment='Test file for test HltDAQ.routingbits_nobias',
    test_file_db=test_file_db
    )

testfiles(
    myname = "HltDAQ-routingbits_full",
    filenames = ['mdf:root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/HltDAQ-routingbits_full/174375_0000000619_full.raw'],
    qualifiers={'Author': 'Roel Aaij', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'DDDB': 'dddb-20150724', 'CondDB':'cond-20160517', 'InitialTime': '2016-05-14 08:30:00 UTC', 'Date': '2016-06-28', 'Application': 'LHCb'},
    comment='Test file for test HltDAQ.routingbits_full',
    test_file_db=test_file_db
    )

testfiles(
    myname = "HltDAQ-routingbits_beamgas",
    filenames = ['mdf:root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/HltDAQ-routingbits_beamgas/174909_0000000705_beamgas.raw'],
    qualifiers={'Author': 'Roel Aaij', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'DDDB': 'dddb-20150724', 'CondDB':'cond-20160517', 'InitialTime': '2016-05-21 01:42:00 UTC', 'Date': '2016-06-28', 'Application': 'LHCb'},
    comment='Test file for test HltDAQ.routingbits_beamgas',
    test_file_db=test_file_db
    )

testfiles(
    myname = "HltDAQ-routingbits_turcal",
    filenames = ['mdf:root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/HltDAQ-routingbits_turcal/174892_0000000258_turcal.raw'],
    qualifiers={'Author': 'Roel Aaij', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'DDDB': 'dddb-20150724', 'CondDB':'cond-20160517', 'InitialTime': '2016-05-20 20:33:00 UTC', 'Date': '2016-06-28', 'Application': 'LHCb'},
    comment='Test file for test HltDAQ.routingbits_turcal',
    test_file_db=test_file_db
    )

testfiles(
    myname = "HltDAQ-routingbits_turbo",
    filenames = ['mdf:root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/HltDAQ-routingbits_turbo/174909_0000000760_turbo.raw'],
    qualifiers={'Author': 'Roel Aaij', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'DDDB': 'dddb-20150724', 'CondDB':'cond-20160517', 'InitialTime': '2016-05-21 01:42:00 UTC', 'Date': '2016-06-28', 'Application': 'LHCb'},
    comment='Test file for test HltDAQ.routingbits_turbo',
    test_file_db=test_file_db
    )

testfiles(
    myname = "HltServices-close_cdb_file",
    filenames = ['mdf:root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/HltServices-close_cdb_file/StripHLT_0x11291600.raw',
                 'mdf:root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/HltServices-close_cdb_file/StripHLT_0x11291603.raw'],
    qualifiers={'Author': 'Roel Aaij', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'DDDB': 'dddb-20150724', 'CondDB':'cond-20160420', 'InitialTime': '2016-05-17 05:26:16 UTC', 'Date': '2016-06-28', 'Application': 'LHCb'},
    comment='Test files for test HltServices.close_cdb_file',
    test_file_db=test_file_db
    )


testfiles(
    myname = "upgrade-Brunel-Baseline-DIGI",
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/UFT5x_13104012_MagDown_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-10ev-Extended.digi'],
    qualifiers={'Format': 'DIGI', 'Author': 'tnikodem', 'DataType': 'Upgrade', 'Simulation': True, 'CondDB': 'sim-20150716-vc-md100', 'DDDB': 'dddb-20160304','Date': '2016-06-30', 'Application': 'Brunel', 'QMTests': 'Brunel'},
    comment='Test file for upgrade Brunel qmtests',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='pPb2013_Pbp_MagUp_NoBias',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/0/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/1/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/2/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/3/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/4/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/5/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/6/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/7/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/8/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/9/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/10/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/11/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/12/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/13/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/14/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/15/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/16/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/17/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/18/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/19/output/pPb_NoBias.raw'],
    qualifiers={'Author': 'zhangy', 'DataType': '2012', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20120831', 'DDDB': 'dddb-20120831', 'Date': '2016-07-05 14:00:52.674188'},
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='pPb2013_pPb_MagDown_NoBias',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/0/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/1/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/2/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/3/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/4/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/5/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/6/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/7/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/8/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/9/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/10/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/11/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/12/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/13/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/14/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/15/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/16/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/17/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/18/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/19/output/pPb_NoBias.raw'],
    qualifiers={'Author': 'zhangy', 'DataType': '2012', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20120831', 'DDDB': 'dddb-20120831', 'Date': '2016-07-05 14:02:12.210359'},
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='pPb2013_pPb_MagUp_NoBias',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/0/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/1/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/2/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/3/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/4/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/5/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/6/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/7/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/8/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/9/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/10/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/11/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/12/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/13/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/14/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/15/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/16/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/17/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/18/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/19/output/pPb_NoBias.raw'],
    qualifiers={'Author': 'zhangy', 'DataType': '2012', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20120831', 'DDDB': 'dddb-20120831', 'Date': '2016-07-05 14:02:57.614386'},
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='pPb2013_Pbp_MagDown_NoBias',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/0/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/1/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/2/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/3/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/4/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/5/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/6/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/7/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/8/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/9/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/10/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/11/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/12/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/13/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/14/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/15/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/16/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/17/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/18/output/pPb_NoBias.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/19/output/pPb_NoBias.raw'],
    qualifiers={'Author': 'zhangy', 'DataType': '2012', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20120831', 'DDDB': 'dddb-20120831', 'Date': '2016-07-05 14:25:53.199139'},
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db
    )

testfiles(
    myname='2016NB_25ns_L0Filt0x1715',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1715/RedoL0.mdf'],
    qualifiers={'Author': 'sely', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20160517', 'DDDB': 'dddb-20150724', "InitialTime": "2016-05-31 10:40:00 UTC", 'Date': '2016-08-03', 'Application': 'L0App'},
    comment='Sample of 2016 No Bias data filtered with 0x1715 for Nightly test of Calibration microbias configuration',
    test_file_db=test_file_db
    )


### auto created ###
testfiles(
    myname='pPb2013_pPb_NoBias_L0Filter0x161B',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/pPb_L0TCK_0x161D_MagDown.raw'],
    qualifiers={'Author': 'zhangy', 'DataType': '2012', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20120831', 'DDDB': 'dddb-20120831', 'Date': '2016-08-18 10:33:38.493052'},
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='pPb2013_Pbp_NoBias_L0Filter0x161B',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/Pbp_L0TCK_0x161D_MagDown.raw', 'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/Pbp_L0TCK_0x161D_MagUp.raw'],
    qualifiers={'Author': 'zhangy', 'DataType': '2012', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20120831', 'DDDB': 'dddb-20120831', 'Date': '2016-08-18 10:35:19.150018'},
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db
    )

testfiles(
    myname='genFSR_2012_digi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/genFSR-2012/Boole-13114005-test1.digi', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/genFSR-2012/Boole-13114005-test2.digi'],
    qualifiers={'Author': 'dfazzini', 'DataType': '2012', 'Format': 'DIGI', 'Simulation': True, 'CondDB': 'sim-20130522-1-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2016-10-01'},
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db
    )

testfiles(
    myname='genFSR_2012_dst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/genFSR-2012/Brunel-13114005-test1.dst', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/genFSR-2012/Brunel-13114005-test2.dst'],
    qualifiers={'Author': 'dfazzini', 'DataType': '2012', 'Format': 'DST', 'Simulation': True, 'CondDB': 'sim-20130522-1-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2016-10-01'},
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db
    )

testfiles(
    myname='genFSR_2012_sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test1.sim', 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test2.sim'],
    qualifiers={'Author': 'dfazzini', 'DataType': '2012', 'Format': 'SIM', 'CondDB': 'sim-20130522-1-vc-md100', 'DDDB': 'dddb-20130929-1', 'Date': '2016-10-25'},
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-FT61-sim',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/SciFi-v61/Gauss-13104012-100ev-20170301.sim'],
    qualifiers={'Format': 'SIM',
                'Author': 'Jeroen van Tilburg',
                'DataType': 'Upgrade',
                'Simulation': True,
                'CondDB': 'sim-20170301-vc-md100',
                'DDDB': 'dddb-20170301',
                'Date': '2017-03-01',
                'QMTests': 'boole-upgrade-baseline.qmt'},
    comment='sim for testing upgrade Boole',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-FT61-digi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/SciFi-v61/Boole-13104012-100ev-20170301.digi'],
    qualifiers={'Format': 'DIGI',
                'Author': 'Jeroen van Tilburg',
                'DataType': 'Upgrade',
                'Simulation': True,
                'CondDB': 'sim-20170301-vc-md100',
                'DDDB': 'dddb-20170301',
                'Date': '2017-03-01',
                'QMTests': 'brunel-upgrade-baseline.qmt'},
    comment='digi for testing upgrade Brunel',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-FT61-xdigi',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/SciFi-v61/Boole-13104012-100ev-20170301-Extended.digi'],
    qualifiers={'Format': 'XDIGI',
                'Author': 'Jeroen van Tilburg',
                'DataType': 'Upgrade',
                'Simulation': True,
                'CondDB': 'sim-20170301-vc-md100',
                'DDDB': 'dddb-20170301',
                'Date': '2017-03-01',
                'QMTests': 'boole-reprocess-xdigi.qmt'},
    comment='xdigi for testing upgrade Boole',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-baseline-FT61-xdst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/SciFi-v61/Brunel-13104012-100ev-20170301.xdst'],
    qualifiers={'Format': 'XDST',
                'Author': 'Jeroen van Tilburg',
                'DataType': 'Upgrade',
                'Simulation': True,
                'CondDB': 'sim-20170301-vc-md100',
                'DDDB': 'dddb-20170301',
                'Date': '2017-03-01',
                'QMTests': 'boole-reprocess-xdst.qmt'},
    comment='xdst for testing upgrade Boole',
    test_file_db=test_file_db
    )

testfiles(
    myname='2015_Leptonic.MDST',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/LEPTONIC.MDST/00048279/0004/00048279_00041826_1.leptonic.mdst'],
    qualifiers={'Format': 'MDST',
                'Author': 'pseyfert',
                'DataType': '2015',
                'Processing': 'Stripping24',
                'Simulation': False,
                'CondDB': 'cond-20150828',
                'DDDB': 'dddb-20150724',
                'Stripping': '24',
                'Reconstruction': 'Reco15a',
                'Date': '2017-03-13 17:39:29.697135',
                'Application': 'DaVinci_v38r1p1',
                'QMTests': 'DaVinciTests.trackrefitting.read_2015_mdst'},
    comment='MDST output of Stripping24 (test if DV can read data from Stripping24 MDST files)',
    test_file_db=test_file_db
    )

testfiles(
    myname='2016_turboraw_run178631',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016TurboRaw/fill5048_run178631_0000001751.raw'],
    qualifiers={'Author'      : 'Olli Lupton',
                'Format'      : 'MDF',
                'DataType'    : '2016',
                'Simulation'  : False,
                'CondDB'      : 'cond-20160522',
                'DDDB'        : 'dddb-20150724',
                'Date'        : '2016-06-28'},
    comment='Proton-Proton collisition data raw data TURBORAW stream, run 178631 from 28th June 2016. HLT1 TCK 0x11341609. HLT2 TCK 0x21361609. LFN /lhcb/data/2016/RAW/TURBORAW/LHCb/COLLISION16/178631/178631_0000001751.raw',
    test_file_db=test_file_db
    )

testfiles(
    myname='2016_turboraw_run179597',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016TurboRaw/fill5085_run179597_0000001921.raw'],
    qualifiers={'Author'      : 'Olli Lupton',
                'Format'      : 'MDF',
                'DataType'    : '2016',
                'Simulation'  : False,
                'CondDB'      : 'cond-20160522',
                'DDDB'        : 'dddb-20150724',
                'Date'        : '2016-07-11'},
    comment='Proton-Proton collisition data raw data TURBORAW stream, run 179597 from 11th July 2016. HLT1 TCK 0x11361609. HLT2 TCK 0x21361609. LFN /lhcb/data/2016/RAW/TURBORAW/LHCb/COLLISION16/179597/179597_0000001921.raw',
    test_file_db=test_file_db
    )

testfiles(
    myname='2016_turboraw_run180546',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016TurboRaw/fill5111_run180546_0000001837.raw'],
    qualifiers={'Author'      : 'Olli Lupton',
                'Format'      : 'MDF',
                'DataType'    : '2016',
                'Simulation'  : False,
                'CondDB'      : 'cond-20160522',
                'DDDB'        : 'dddb-20150724',
                'Date'        : '2016-07-24'},
    comment='Proton-Proton collisition data raw data TURBORAW stream, run 180546 from 24th July 2016. HLT1 TCK 0x11361609. HLT2 TCK 0x21361609. LFN /lhcb/data/2016/RAW/TURBORAW/LHCb/COLLISION16/180546/180546_0000001837.raw',
    test_file_db=test_file_db
    )
testfiles(
    myname='2016NB_L0Filt0x1701',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184247_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184249_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184251_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184253_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184254_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184255_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184256_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184257_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184258_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184261_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184263_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184264_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184266_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184267_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184268_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184269_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184270_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184271_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184272_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184273_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184274_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184275_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184276_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184285_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184289_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184290_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184291_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184292_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184293_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184294_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184296_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184299_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184300_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184301_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184302_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184303_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184304_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185312_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185313_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185318_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185319_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185320_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185321_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185322_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185323_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185324_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185325_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185326_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185327_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185328_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185329_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185330_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185331_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185332_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185333_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185335_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185336_01.mdf'],
    qualifiers={'Author': 'cofitzpa', 'DataType': '2016', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20170325', 'DDDB': 'dddb-20150724', 'Date': '2017-04-10', 'Application': 'L0App', "InitialTime": "2016-10-22 12:00:00 UTC", },
    comment='Sample of 2016 No Bias data filtered with 0x1701 for rate tests and 2017 commissioning',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017NB_L0Filt0x1706',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_01.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_02.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_03.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_04.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_05.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_06.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_07.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_08.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_09.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_100.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_101.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_102.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_103.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_104.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_105.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_106.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_107.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_108.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_109.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_10.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_110.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_111.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_112.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_113.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_114.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_115.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_116.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_117.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_118.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_119.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_11.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_120.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_12.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_13.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_14.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_15.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_16.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_17.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_18.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_19.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_20.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_21.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_22.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_23.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_24.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_25.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_26.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_27.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_28.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_29.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_30.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_31.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_32.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_33.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_34.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_35.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_36.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_37.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_38.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_39.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_40.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_41.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_42.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_43.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_44.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_45.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_46.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_47.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_48.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_49.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_50.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_51.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_52.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_53.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_54.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_55.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_56.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_57.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_58.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_59.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_60.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_61.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_62.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_63.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_64.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_65.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_66.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_67.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_68.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_69.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_70.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_71.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_72.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_73.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_74.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_75.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_76.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_77.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_78.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_79.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_80.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_81.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_82.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_83.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_84.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_85.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_86.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_87.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_88.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_89.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_90.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_91.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_92.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_93.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_94.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_95.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_96.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_97.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_98.mdf',
'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_99.mdf'],
    qualifiers={'Author': 'cofitzpa', 'DataType': '2017', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20170510', 'DDDB': 'dddb-20150724', 'Date': '2017-06-20', 'Application': 'L0App', },
    comment='Sample of 2017 No Bias data filtered with 0x1706 for rate tests and 2017 commissioning',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017NB_L0Filt0x1707',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1707/2017NB_0x1707_194920_{0:02}.mdf'.format(i) for i in range(1, 29)],
    qualifiers={
        'Author': 'rmatev', 'Date': '2018-01-24',
        'DataType': '2017', 'Format': 'MDF',
        'Simulation': False, 'CondDB': 'cond-20170510', 'DDDB': 'dddb-20150724',
        'InitialTime': '2017-07-14 09:41:00 UTC',  # run 194920
        },
    comment='Sample of 2017 No Bias data filtered with 0x1707 for rate tests and 2017 commissioning',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017_Hlt1_0x11611709',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017HLT1/Run_0201852_20171106-{0}.hltf0111.mdf'.format(i)
               for i in [222031,222252,222426,222513,222644,222905,223039,223259,
                         223431,223517,223649,223906,224040,224125,224256,224339,
                         224511,224729,224901,225119,225251,225337,225507,225725,
                         225857,225942,230116,230203,230335,230554,230724,230811,
                         230942,231114,231158,231331,231549,231807,231938]],
    qualifiers={
        'Author': 'rmatev', 'Date': '2018-02-27',
        'DataType': '2017', 'Format': 'MDF',
        'Simulation': False, 'CondDB': 'cond-20170724', 'DDDB': 'dddb-20150724',
        'InitialTime': '2017-11-06 21:20:00 UTC',  # run 201852
        },
    comment='Sample of 2017 HLT1-accepted data with 0x11611709',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017NB',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB/{0}'.format(i)
               for i in ['Run_0197458_20170818-150239.hlte0112.mdf'
                         'Run_0197458_20170818-150240.hlta0105.mdf'
                         'Run_0197458_20170818-150240.hlte0109.mdf'
                         'Run_0197458_20170818-150240.hlte0110.mdf'
                         'Run_0197458_20170818-150241.hlte0105.mdf'
                         'Run_0197458_20170818-150241.hlte0106.mdf'
                         'Run_0197458_20170818-150241.hlte0107.mdf'
                         'Run_0197458_20170818-150241.hlte0108.mdf'
                         'Run_0197458_20170818-150242.hlte0111.mdf'
                         'Run_0197458_20170818-150249.hlte0117.mdf'
                         'Run_0197458_20170818-150249.hlte0118.mdf'
                         'Run_0197458_20170818-150249.hltf0106.mdf'
                         'Run_0197458_20170818-150249.hltf0108.mdf'
                         'Run_0197458_20170818-150251.hltf0105.mdf'
                         'Run_0197458_20170818-150251.hltf0109.mdf'
                         'Run_0197458_20170818-150252.hlte0119.mdf'
                         'Run_0197458_20170818-150252.hltf0107.mdf'
                         'Run_0197458_20170818-150252.hltf0111.mdf'
                         'Run_0197458_20170818-150252.hltf0112.mdf'
                         'Run_0197458_20170818-150253.hltf0110.mdf'
                         'Run_0197458_20170818-150254.hlte0116.mdf'
                         'Run_0197458_20170818-150257.hlte0115.mdf']],
    qualifiers={
        'Author': 'rmatev', 'Date': '2018-03-21',
        'DataType': '2017', 'Format': 'MDF',
        'Simulation': False, 'CondDB': 'cond-20170724', 'DDDB': 'dddb-20150724',
        'InitialTime': '2017-08-18 13:03:00 UTC',  # run 197458
        },
    comment='Sample of 2017 NoBias events',
    test_file_db=test_file_db
    )

testfiles(
    myname='2017NB_L0Filt0x18A1',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2018CommissioningDatasets/2017NB_0x18A1/2017NB_0x18A1_197458_{0:02}.mdf'.format(i) for i in range(1, 22)],
    qualifiers={
        'Author': 'rmatev', 'Date': '2018-04-05',
        'DataType': '2017', 'Format': 'MDF',
        'Simulation': False, 'CondDB': 'cond-20170724', 'DDDB': 'dddb-20150724',
        'InitialTime': '2017-08-18 13:03:00 UTC',  # run 197458
        },
    comment='Sample of 2017 No Bias data filtered with 0x18A1 for rate tests and 2018 commissioning',
    test_file_db=test_file_db
    )

testfiles(
    myname='2018_Hlt1_0x11751801',
    filenames=['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2018CommissioningDatasets/2018HLT1/2018_Hlt1_0x11751801_209933_{0:02}.mdf'.format(i) for i in range(68)],
    qualifiers={
        'Author': 'mramospe', 'Date': '2018-06-07',
        'DataType': '2018', 'Format': 'MDF',
        'Simulation': False, 'CondDB': 'cond-20180202', 'DDDB': 'dddb-20171030-3',
        'InitialTime': '2018-06-06 15:10:00 UTC', # run 209933
        },
    comment='Sample of 2018 HLT1-accepted data with 0x11751801',
    test_file_db=test_file_db
    )

### auto created ###
testfiles(
    myname='upgrade_integration_test',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/LHCbIntegrationTest/Gauss-30000000-10ev-20171212.sim'],
    qualifiers={'Author': 'sstahl', 'DataType': 'SIM', 'Format': 'SIM', 'Simulation': True, 'CondDB': 'sim-20171127-vc-md100', 'DDDB': 'dddb-20171126', 'Date': '2018-01-11 13:46:49.273953'},
    comment='A sample of 10 events, privately produced, to test the integration of applications from Boole to DaVinci in Upgrade conditions',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-throughput-minbias-nogec-mdf',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade/MINBIASTESTSAMPLE_big.mdf'],
    qualifiers={'Author' : 'gligorov', 'DataType': 'Upgrade', 'Format' : 'MDF', 'Simulation' : True, 'CondDB' : 'sim-20171127-vc-md100', 'DDDB' : 'dddb-20171126', 'Date' : '2018-02-06'},
    comment='A sample of upgrade events in MDF format with no GEC applied for throughput tests',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-throughput-minbias-mdf',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/upgrade/MINBIASTESTSAMPLE_big_GEC.mdf'],
    qualifiers={'Author' : 'gligorov', 'DataType': 'Upgrade', 'Format' : 'MDF', 'Simulation' : True, 'CondDB' : 'sim-20171127-vc-md100', 'DDDB' : 'dddb-20171126', 'Date' : '2018-02-06'},
    comment='A sample of upgrade events in MDF format with a GEC at 11000 FT hits applied for throughput tests',
    test_file_db=test_file_db
    )

testfiles(
    myname='upgrade-magdown-sim09c-up02-reco-up01-minbias-ldst',
    filenames=['root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeMinbias/00069155_00000021_2.ldst'],
    qualifiers={
      'Format': 'LDST', 'Author': 'olupton', 'DataType': 'Upgrade', 'Processing': 'Sim09c-Up02', 'Simulation': True,
      'CondDB': 'sim-20171127-vc-md100', 'DDDB': 'dddb-20171126', 'Reconstruction': 'Reco-Up01', 'Date': '2018-04-25'},
    comment='Upgrade minbias MC in LDST format, used for TCK creation for upgrade MC filtering for selection studies',
    test_file_db=test_file_db
    )

testfiles(
    "2015_DaVinciTests.davinci.gaudipython_algs",
    [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00008700_1.rdst",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00008701_1.rdst"
    ],
    {
    "Author": "Eduardo Rodrigues",
    "Format": "DST",
    "DataType": "2015",
    "Date": "2018.05.30",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["DaVinciTests.davinci.gaudipython_algs"],
    },
    "Proton-Proton collision data, Reco15a, run 164668",
    test_file_db
    )

testfiles(
    "2016_DaVinciTests.stripping28X_collision16_reco16",
    [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/RDST/00053196/0010/00053196_00100516_1.rdst",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/RDST/00053196/0009/00053196_00099798_1.rdst"
    ],
    {
    "Author": "Eduardo Rodrigues",
    "Format": "DST",
    "DataType": "2016",
    "Date": "2018.06.01",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": [""],
    },
    "Proton-Proton collision data from 2016",
    test_file_db
    )

testfiles(
    "2017_DaVinciTests.stripping29r2_collision17_reco17",
    [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/RDST/00064899/0006/00064899_00065694_1.rdst",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/RDST/00064899/0006/00064899_00065751_1.rdst"
    ],
    {
    "Author": "Eduardo Rodrigues",
    "Format": "DST",
    "DataType": "2017",
    "Date": "2018.06.04",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["DaVinciTests.stripping29r2_collision17_reco17"],
    },
    "Proton-Proton collision data from 2017",
    test_file_db
    )

testfiles(
    "2012_DaVinciTests.stripping.test_express_appconfig",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00047243_1.full.dst"],
    {
    "Author": "Eduardo Rodrigues",
    "Format": "DST",
    "DataType": "2012",
    "Date": "2018.06.01",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["DaVinciTests.stripping.test_express_appconfig"],
    },
    "Proton-Proton collision data from 2012",
    test_file_db
    )

testfiles(
    "2015_DaVinciTests.trackrefitting.read_2015_mdst",
    ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/LEPTONIC.MDST/00048279/0000/00048279_00001457_1.leptonic.mdst"],
    {
    "Author": "Eduardo Rodrigues",
    "Format": "MDST",
    "DataType": "2015",
    "Date": "2018.06.01",
    "Application": "DaVinci",
    "Simulation" : False,
    "QMTests": ["DaVinciTests.trackrefitting.read_2015_mdst"],
    },
    "Stripping output from 2015 pp collision data",
    test_file_db
    )

testfiles(
    myname='TeslaTest_2018raw_0x11751801_0x21761801_TURBO',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2018raw_0x11751801_0x21761801/210224_0000000252.raw'],
    qualifiers={'Author': 'rmatev', 'DataType': '2018', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20180202', 'DDDB': 'dddb-20171030-3', 'InitialTime': '2018-06-12 04:47:00 UTC', 'Date': '2018-06-18', 'Application': 'Moore'},
    comment='A 2018 turbo raw file for tesla testing',
    test_file_db=test_file_db
    )

testfiles(
    myname='TeslaTest_2018raw_0x11751801_0x21761801_TURCAL',
    filenames = ['mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2018raw_0x11751801_0x21761801/210224_0000000247.raw'],
    qualifiers={'Author': 'rmatev', 'DataType': '2018', 'Format': 'MDF', 'Simulation': False, 'CondDB': 'cond-20180202', 'DDDB': 'dddb-20171030-3', 'InitialTime': '2018-06-12 04:47:00 UTC', 'Date': '2018-06-18', 'Application': 'Moore'},
    comment='A 2018 turbo raw file for tesla testing',
    test_file_db=test_file_db
    )
