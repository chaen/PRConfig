class Iterator(object):
    ## This module can either return a python string or a function that takes no
    ## arguments and returns the correct string.
    def __init__(self):
        pass

    def __call__(self):
        return "import TrAligIterator; TrAligIterator.doIt('VeloAlignment')"

    def alignmentType(self):
        return 'Velo'

    def gaudi(self):
        return True

class Analyzer(object):
    ## This module can either return a python string or a function that takes a
    ## single integer argument and returns the correct string.
    def __init__(self, input_files):
        self.__input_files = input_files

    def __call__(self, n):
        python_string =  "import TrAligAnalyzer; TrAligAnalyzer.doIt('{0}', 'VeloAlignment', {1})"
        return python_string.format(self.__input_files[n], n)

    def gaudi(self):
        return True
