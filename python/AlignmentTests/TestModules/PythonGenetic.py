class Iterator(object):
    ## This module can either return a python string or a function that takes no
    ## arguments and returns the correct string.
    def __init__(self):
        pass

    def __call__(self):
        return "from PyGeneticOnline import Iterator; Iterator.run()"

    def gaudi(self):
        return False

class Analyzer(object):
    ## This module can either return a python string or a function that takes a
    ## single integer argument and returns the correct string.
    def __init__(self, input_files):
        self.__input_files = input_files

    def __call__(self, n):
        python_string = "from PyGeneticOnline import Analyzer; Analyzer.run({0})"
        return python_string.format(n)

    def gaudi(self):
        return False
