class Iterator(object):
    ## This module can either return a python string or a function that takes no
    ## arguments and returns the correct string.
    def __init__(self):
        pass

    def __call__(self):
#        return "from PyMirrAlignOnline import Iterator; Iterator.run()"
        return "from PyMirrAlignOnline import Iterator; Iterator.run(2)"
    
    def alignmentType(self):
        return 'Rich'

    def gaudi(self):
        return False

class Analyzer(object):
    ## This module can either return a python string or a function that takes a
    ## single integer argument and returns the correct string.
    def __init__(self, input_files):
        self.__input_files = input_files

    def __call__(self, n):
#        python_string = "from PyMirrAlignOnline import Analyzer; Analyzer.run({0})"
#        return python_string.format(n)
        python_string = "from PyMirrAlignOnline import RichAnalyzer; RichAnalyzer.doIt('{0}', {1})"
        return python_string.format(self.__input_files[n], n) 

    def gaudi(self):
        return True # true to run  , see TrackerAlignment for rest [change to take filename and index of proccess]
