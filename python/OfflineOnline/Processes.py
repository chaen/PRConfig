import os
import subprocess
import socket
import atexit
import pydim
import time
import shlex
from operator import itemgetter


class State(object):
    UNKNOWN = "UNKNOWN"
    ERROR = "ERROR"
    NOT_READY = "NOT_READY"
    READY = "READY"
    RUNNING = "RUNNING"
    PAUSED = "PAUSED"


_fifo = None
_tmpdir = None
_processes = []


def __remove_tmpdir():
    if not _tmpdir:
        return
    import shutil
    if os.path.exists(_tmpdir) and os.path.isdir(_tmpdir):
        shutil.rmtree(_tmpdir)

# atexit.register(__remove_tmpdir)


def __killall():
    for p in _processes:
        pid = p.pid
        if p.poll() is None:
            p.terminate()
        subprocess.call(['kill', '-9', str(pid)])
    subprocess.Popen(['pkill', '-9', '-u', str(os.getuid()), 'gentest.exe'])


atexit.register(__killall)


def get_tmpdir():
    global _tmpdir
    if not _tmpdir:
        import tempfile
        _tmpdir = tempfile.mkdtemp(prefix="test_")
    return _tmpdir


def _get_fifo(suffix=""):
    f = 'logTest%s.fifo' % suffix
    return os.path.join(get_tmpdir(), f)


class Launcher(object):

    def __init__(self, partition, logger, partition_id=0):
        self.__partition = partition
        self.__partition_id = partition_id
        self.__logger = logger

    def partition(self):
        return self.__partition

    def partition_id(self):
        return self.__partition_id

    def logger(self):
        return self.__logger

    def launch(self, utgid, command, env, do_exec=True, node=0):
        launch_env = os.environ.copy()
        launch_env.update(env)
        launch_env.update({'UTGID': utgid, "LC_ALL": "C"})

        def __launch(cmd, **kwargs):
            try:
                p = subprocess.Popen(cmd, **kwargs)
                return p
            except OSError:
                executable = kwargs.get('executable', None)
                if 'executable' in kwargs:
                    print ('\nERROR: Failed to launch as %s: %s\n' %
                           (cmd[0], ' '.join([executable] + cmd[1:])))
                else:
                    print '\nERROR: Failed to launch %s\n' % ' '.join(cmd)
                raise

        if type(command) == str:
            if do_exec:
                command = shlex.split(os.path.expandvars(command))
                cmd = [utgid] + command[1:]
                print 'normal launch %s as %s: %s' % (command[0], utgid,
                                                      ' '.join(cmd[1:]))
                p = __launch(cmd, executable=command[0], env=launch_env)
            else:
                print 'normal launch:', command
                p = __launch(shlex.split(command), env=launch_env)
        else:
            command = [os.path.expandvars(c) for c in command]
            if do_exec:
                command = ' '.join(command)
                cmd = ['sh', '-c', 'exec -a %s %s' % (utgid, command)]
                print 'normal launch %s' % cmd
                p = __launch(cmd, env=launch_env)
            else:
                print 'normal launch:', command
                p = __launch(command, env=launch_env)

        _processes.append(p)
        return p


class NumaLauncher(Launcher):

    def __init__(self, partition, logger, partition_id=0):
        Launcher.__init__(self, partition, logger, partition_id)

    def launch(self, utgid, command, env, do_exec=True, node=0):
        cmd = ['/usr/bin/numactl', '--cpunodebind=%d' % node,
               '--membind=%d' % node]
        command = os.path.expandvars(command)
        if do_exec:
            if type(command) != str:
                command = ' '.join(command)
            cmd += ['sh', '-c', 'exec -a %s %s' % (utgid, command)]
        else:
            if type(command) == str:
                cmd += shlex.split(command)
            else:
                cmd += [command]
        launch_env = os.environ.copy()
        launch_env.update(env)
        launch_env.update({'UTGID': utgid, "LC_ALL": "C"})
        self.logger().debug("Launching: " + ' '.join(cmd))
        print ' '.join(cmd)
        p = subprocess.Popen(cmd, env=launch_env)
        _processes.append(p)
        return p


class TestProcess(object):

    def __init__(self, logger, launcher, priority):
        from multiprocessing import Lock, Condition
        self.__launcher = launcher
        self.__logger = logger
        self.__process = None
        self.__condition = Condition(Lock())
        self.__priority = priority

    def priority(self):
        return self.__priority

    def condition(self):
        return self.__condition

    def partition(self):
        return self.__launcher.partition()

    def partition_id(self):
        return self.__launcher.partition_id()

    def logger(self):
        return self.__logger

    def launcher(self):
        return self.__launcher

    def log_fifo(self):
        return _get_fifo('_' + self.partition())

    def hostname(self):
        return socket.gethostname().split('.')[0]

    def utgid(self, suffix):
        hn = self.hostname().split('-')[-1]
        return '_'.join((self.partition(), hn.upper(), suffix))

    def terminate(self):
        self._terminate(self.__process)

    def _terminate(self, process):
        if not process:
            return
        self.logger().debug("Stopping process %d.", process.pid)
        process.terminate()
        for i in range(5):
            time.sleep(1)
            if process.poll() is not None:
                break
        else:
            self.logger().warning("Sending KILL signal to %d" % process.pid)
            process.kill()
        self.__process = None

    def process(self):
        return self.__process

    def launch(self, utgid, command, env, do_exec=True, node=0):
        self.__process = self.__launcher.launch(
            utgid, command, env, do_exec, node)
        return self.__process

    def results(self):
        return None

    def run(self):
        return None

    def stop(self):
        return None

    def stop_counting(self):
        return None

    def finalize(self):
        return None

    def wait(self):
        return None

    def generate_options(self, filename, options):
        if not filename.endswith('.py'):
            filename = self.partition() + "_" + filename
        option_file = os.path.join(get_tmpdir(), filename)
        if not os.path.exists(option_file):
            self.logger().debug('creating option file %s' % option_file)
            with open(option_file, "w") as of:
                of.write(options)
        else:
            self.logger().debug(
                'option file %s already exists, returning it' % option_file)
        return option_file

    def info_opts(self):
        opts = ['#pragma print off',
                'OnlineEnv.PartitionID      = %d;' % self.partition_id(),
                'OnlineEnv.PartitionIDName  = "%s";' % (
                    '%04x' % self.partition_id()).upper(),
                'OnlineEnv.PartitionName    = "%s";' % self.partition(),
                'OnlineEnv.OutputLevel      = 3;',
                'OnlineEnv.DeferredRuns     = {"*"};',
                'MessageSvc.OutputLevel     = @OnlineEnv.OutputLevel;',
                'MessageSvc.fifoPath        = "%s";' % self.log_fifo()]
        return self.generate_options('INFO_options.opts', '\n'.join(opts))

    def mbm_opts(self):
        opts = ['#pragma print off',
                ('OnlineEnv.MBM_setup = "-s=100000 -e=1000 -u=50 -b=17 -f'
                 ' -i=Events -c -s=30000 -e=150 -u=50 -f -i=Output -c";'),
                'OnlineEnv.PartitionBuffers = true;',
                'OnlineEnv.MEPrx_Output   = "Events";',
                'OnlineEnv.MEPrx_Buffers  = {"Events"};',
                'OnlineEnv.Moore1_Input   = "Events";',
                'OnlineEnv.Moore1_Output  = "Output";',
                'OnlineEnv.Moore1_Buffers = {"Events", "Output"};',
                'OnlineEnv.DskWriter_Input   = "Output";',
                'OnlineEnv.DskWriter_Buffers = {"Output"};']
        return self.generate_options('MBM_setup.opts', '\n'.join(opts))

    def _notify(self):
        self.__condition.acquire()
        self.__condition.notify()
        self.__condition.release()

    def _wait(self):
        self.__condition.acquire()
        self.__condition.wait()
        self.__condition.release()


class GaudiTestProcess(TestProcess):

    def __init__(self, logger, launcher, priority):
        TestProcess.__init__(self, logger, launcher, priority)
        self.__state = State.UNKNOWN
        self._stopping = False

    def stopping(self):
        return self._stopping

    def state(self):
        return self.__state

    def set_state(self, state):
        self.__state = state

    def results(self):
        return None

    def run(self):
        self._stopping = False

    def stop(self):
        self._stopping = True

    def daemon_opts(self):
        opts = ['#pragma print off',
                '#include "$GAUDIONLINEROOT/options/Main.opts"',
                'Runable.Wait                         = 3;',
                'ApplicationMgr.ExtSvc                = {"IncidentSvc"};',
                'MessageSvc.OutputLevel               = 3;']
        return self.generate_options('Daemon.opts', '\n'.join(opts))

    def main_opts(self):
        opts = ['#pragma print off',
                'ApplicationMgr.EventLoop = "LHCb::OnlineRunable/EmptyEventLoop";',
                'ApplicationMgr.Runable   = "LHCb::OnlineRunable/Runable";',
                'ApplicationMgr.HistogramPersistency  = "NONE";',
                'ApplicationMgr.EvtSel  = "NONE";',
                'MessageSvc.fifoPath    = "%s";' % self.log_fifo(),
                'MessageSvc.OutputLevel = 3;',
                'Runable.Wait           = 1;']
        return self.generate_options('Main.opts', '\n'.join(opts))

    def mon_opts(self):
        opts = ['Monitoring.PartitionName = @OnlineEnv.PartitionName;',
                'Monitoring.UniqueServiceNames = 1;',
                'Monitoring.ExpandCounterServices = 1;',
                'Monitoring.ExpandNameInfix = "<proc>/";']
        return self.generate_options('Monitoring.opts', '\n'.join(opts))

    def online_env(self):
        opts = {"partition_id": self.partition_id(),
                "partition_hex": ('%04x' % self.partition_id()).upper(),
                "partition": self.partition()}
        code = """
PartitionID              = %(partition_id)d
PartitionIDName          = "%(partition_hex)s"
PartitionName            = "%(partition)s"
Activity                 = "COLLISION"
TAE                      = 0
OutputLevel              = 3
# ---------------- Trigger parameters:
LumiPars                 = [1.0, 0.214286, 0.142857, 0.0714286]
DataflowVersion          = ""
GaudiVersion             = ""
DeferredRuns             = ["*"]
MooreStartupMode         = 1
""" % opts
        return self.generate_options('OnlineEnvBase.py', code)

    def mep_init_opts(self):
        opts = ['#pragma print off',
                '#include "%s"' % self.info_opts(),
                '#include "%s"' % self.mbm_opts(),
                'Manager.Setup               = {"Dataflow_MBMServer/MEPManager"};',
                'MEPManager.PartitionBuffers          = @OnlineEnv.PartitionBuffers;',
                'MEPManager.PartitionName             = @OnlineEnv.PartitionName;',
                'MEPManager.InitFlags                 = @OnlineEnv.MBM_setup;',
                'Logger.OutputLevel               = @OnlineEnv.OutputLevel;']
        return self.generate_options('MEPInit.opts', '\n'.join(opts))

    def send_command(self, utgid, command, wait=True):
        if wait:
            self.condition().acquire()
            pydim.dic_cmnd_service(utgid, (command,), "C")
            self.condition().wait()
            self.condition().release()
        else:
            pydim.dic_cmnd_service(utgid, (command,), "C")


class TestViewer(TestProcess):

    def __init__(self, logger, launcher, priority):
        TestProcess.__init__(self, logger, launcher, priority)
        self.__viewer = None

    def launchViewer(self, utgid, command, env, title, geometry='132x65',
                     login_shell=True, extra_args=[]):
        command = (['xterm', '-132', '-geometry', geometry, '-title', title]
                   + list(extra_args) +
                   ['-e', ('exec -a %s ' % utgid) + ' '.join(command)])
        if login_shell:
            command.insert(1, '-ls')
        self.__viewer = self.launcher().launch(
            utgid, command, env, do_exec=False)
        return self.__viewer

    def launch(self, utgid, command, env, extra_args=[]):
        self.__viewer = self.launcher().launch(
            utgid, command + extra_args, env, do_exec=False)
        return self.__viewer

    def finalize(self):
        pass

    def terminate(self):
        self._terminate(self.__viewer)
        self.__viewer = None

    def viewer(self):
        return self.__viewer


class MBMMon(TestViewer):

    def __init__(self, logger, launcher, **kwargs):
        TestViewer.__init__(self, logger, launcher, kwargs.pop('priority'))

    def initialize(self):
        utgid = self.utgid("MBMMon")
        ld_path = os.environ['LD_LIBRARY_PATH'].split(':')
        f = None
        for d in ld_path:
            f = os.path.join(d, 'libOnlineKernel.so')
            if os.path.exists(f):
                break
        assert(f)
        env = {"MBM_SETUP_OPTIONS": self.mbm_opts(),
               "INFO_OPTIONS": self.info_opts(),
               'LOGFIFO': self.log_fifo()}
        command = ['`which gentest.exe`', f, 'mbm_mon',
                   '-p={}'.format(self.partition())]
        self.launchViewer(utgid, command, env, 'MBMMon')

    def terminate(self):
        TestViewer.terminate(self)
        subprocess.Popen(
            ['pkill', '-9', '-u', str(os.getuid()), 'gentest.exe'])


class LogViewer(TestViewer):

    def __init__(self, logger, launcher, **kwargs):
        TestViewer.__init__(self, logger, launcher, kwargs.pop('priority'))
        self.__output_file = kwargs.pop('output_file', None)
        if self.__output_file:
            self.__split_regex = kwargs.pop('split_regex')
            self.__timing_width = kwargs.pop('timing_width')
        else:
            self.__split_regex = None
            self.__timing_width = None

    def output_file(self, suffix=''):
        if not self.__output_file:
            return None
        if os.path.sep in self.__output_file:
            d, f = self.__output_file.rsplit(os.path.sep, 1)
            if not os.path.exists(d):
                os.makedirs(d)
        else:
            d = '.'
            f = self.__output_file
        s = f.rsplit('.', 1)
        if len(s) == 1:
            of = f + ('_%s' % suffix)
        else:
            b, e = s
            if suffix:
                b += suffix
            of = '%s.%s' % (b, e)
        return os.path.join(d, of)

    def initialize(self):
        utgid = self.utgid("LogViewer")
        env = {'LOGFIFO': self.log_fifo()}
        command = ['/opt/FMC/bin/logViewer', '-sl', '20000', '-N', 'localhost',
                   '-m', self.hostname(), '-l', '2', '-S',
                   '-s', self.partition().lower()]
        output_file = self.output_file()
        if output_file:
            command += ['-O', output_file]
            self.launch(utgid, command, env)
        else:
            self.launchViewer(utgid, command, env, 'LogViewer',
                              geometry='170x50', extra_args=('-bg', 'black'))

    def terminate(self):
        self.__split_logs()
        if self.__output_file:
            self._terminate(self.viewer())

    def __split_logs(self):
        output_file = self.output_file()
        if not output_file:
            return
        if not self.__split_regex:
            return

        logs = {}
        import re
        split_re = re.compile(self.__split_regex)
        if not os.path.exists(output_file):
            return
        n_keys = self.__split_regex.count("\d")
        with open(output_file) as log:
            for line in log:
                m = split_re.search(line)
                if not m:
                    continue
                key = tuple(m.group(i) for i in range(1, n_keys + 1))
                if key not in logs:
                    log = logs[key] = open(
                        self.output_file(('_%s' * n_keys) % key), 'w')
                else:
                    log = logs[key]
                line = line[m.end() + 3:]
                if line.startswith('TIMER'):
                    loc = line.find('|')
                    if loc > self.__timing_width:
                        line = line[: self.__timing_width] + line[loc:]
                log.write(line)

        for log in logs.itervalues():
            log.close()


class LogServer(TestProcess):

    def __init__(self, logger, launcher, **kwargs):
        TestProcess.__init__(self, logger, launcher, kwargs.pop('priority'))
        self.__dim_svc = None

    def initialize(self):
        service = "/FMC/%s/logger/%s/server_version" % (
            self.hostname().upper(), self.partition().lower())
        self.logger().debug("Subscribing to logger service: %s" % service)
        self.__dim_svc = pydim.dic_info_service(
            service, "C", self.__callback, pydim.MONITORED, 0, 0, None)
        self.condition().acquire()
        if os.path.exists("/opt/FMC/sbin/logSrv"):
            cmd = "/opt/FMC/sbin/logSrv"
        else:
            cmd = "/opt/FMC/sbin64/logSrv"
        command = [cmd, "-N", "localhost", "-v", "-s",
                   self.partition().lower(), "-l", "1",  "-S",  "1",
                   "-p", self.log_fifo()]
        self.__log_server = self.launch(
            self.utgid("LogSrv"), command, env={'LOGFIFO': self.log_fifo()})
        self.condition().wait()
        self.condition().release()

    def finalize(self):
        if self.__dim_svc:
            pydim.dic_release_service(self.__dim_svc)
            self.__dim_svc = None

    def terminate(self):
        self._terminate(self.process())

    def __callback(self, tag, val):
        if not val:
            return
        self._notify()


class MBM(GaudiTestProcess):

    def __init__(self, logger, launcher, **kwargs):
        GaudiTestProcess.__init__(
            self, logger, launcher, kwargs.pop('priority'))
        self.__dim_svc = None
        self.__utgid = None

    def initialize(self):
        self.__utgid = self.utgid("MEPInit_01")
        service = self.__utgid + "/status"
        self.logger().debug("Subscribing to mbm service: %s" % service)
        self.__dim_mon_svc = pydim.dic_info_service(
            service, "C", self.__callback, pydim.MONITORED, 0, 0, None)
        env = {'LC_ALL': 'C', 'UTGID': self.__utgid}
        command = ['gentest.exe', 'libDataflow.so', 'dataflow_run_task',
                   '-msg=Dataflow_FmcLogger', '-mon=Dataflow_DIMMonitoring',
                   '-class=Class0', '-opts=%s' % self.mep_init_opts()]

        # Launch MBM and wait for NOT_READY state
        self.condition().acquire()
        self.__mbms = self.launch(self.__utgid, command, env)
        self.condition().wait()
        self.condition().release()

        self.send_command(self.__utgid, 'configure')
        self.send_command(self.__utgid, 'start')

    def stop(self):
        self._stopping = True
        self.send_command(self.__utgid, 'stop')

    def finalize(self):
        if not self._stopping:
            return

        self.send_command(self.__utgid, 'reset')
        if self.__dim_svc:
            pydim.dic_release_service(self.__dim_svc)
            self.__dim_svc = None

    def terminate(self):
        self._terminate(self.process())

    def __callback(self, tag, val):
        if not val:
            return
        try:
            if (self.state() == State.UNKNOWN
                    and val.strip().startswith("NOT_READY")):
                self.logger().info("MBM went to NOT_READY")
                self.set_state(State.NOT_READY)
                self._notify()
            elif (self.state() == State.NOT_READY
                  and val.strip().startswith("READY")):
                self.logger().info("MBM went to READY")
                self.set_state(State.READY)
                self._notify()
            elif (self.state() == State.READY
                  and val.strip().startswith("RUNNING")):
                self.logger().info("MBM went to RUNNING")
                self.set_state(State.RUNNING)
                self._notify()
            elif (self.state() == State.RUNNING
                  and val.strip().startswith("READY")):
                self.logger().info("MBM went to READY")
                self.set_state(State.READY)
                self._notify()
            elif (self.state() == State.READY
                  and val.strip().startswith("NOT_READY")):
                self.logger().info("MBM went to NOT_READY")
                self.set_state(State.NOT_READY)
                self._notify()
            else:
                self.logger().debug("MBM in state %s." % val.strip())
        except Exception:
            self.logger().debug("exception while waiting for "
                                "MBM to start up ...", exc_info=True)


class MEPReader(GaudiTestProcess):

    def __init__(self, logger, launcher, **kwargs):
        GaudiTestProcess.__init__(
            self, logger, launcher, kwargs.pop('priority'))
        self.__directory = kwargs.pop('directory')
        self.__delete = kwargs.pop('delete_files')
        self.__prefix = kwargs.pop('file_prefix', 'Run_')
        self.__dim_svc = None
        self.__utgid = None
        from multiprocessing import Lock
        self.__callback_lock = Lock()

    def directory(self):
        return self.__directory

    def initialize(self):
        self.__utgid = self.utgid("MEPReader_01")
        service = self.__utgid + "/status"
        self.logger().info("Starting MEP reader")
        self.__dim_svc = pydim.dic_info_service(
            service, "C", self.__callback, pydim.MONITORED, 0, 0, None)
        env = {'LOGFIFO': self.log_fifo()}
        command = ['gentest.exe', 'libDataflow.so', 'dataflow_run_task',
                   '-msg=Dataflow_FmcLogger', '-mon=Dataflow_DIMMonitoring',
                   '-class=Class2', '-opts=%s' % self.reader_opts()]

        # Launch the MEP reader and wait for the NOT_READY state
        self.condition().acquire()
        self.launch(self.__utgid, command, env)
        self.condition().wait()
        self.condition().release()

        self.send_command(self.__utgid, "configure")
        self.send_command(self.__utgid, "start")

    def stop(self):
        # Then stop process
        self._stopping = True
        self.send_command(self.__utgid, "stop")

    def finalize(self):
        if not self._stopping:
            return

        self.send_command(self.__utgid, "reset")

    def terminate(self):
        self._terminate(self.process())

    def reader_opts(self):
        deleteFiles = ('true' if self.__delete else 'false')
        opts = ['#pragma print off',
                '#include "%s"' % self.info_opts(),
                '#include "%s"' % self.mbm_opts(),
                '#include "%s"' % self.mon_opts(),
                'Manager.Services = {"Dataflow_MBMClient/MEPManager",',
                '"Dataflow_HltReader/Reader",',
                '"Dataflow_RunableWrapper/Wrap"};',
                'Manager.Runable    = "Wrap";',
                'Wrap.Callable      = "Reader";',
                'Reader.Buffer      = @OnlineEnv.MEPrx_Output;',
                'Reader.BrokenHosts = "%s";' % self.broken_opts(),
                'Reader.Directories = {"%s"};' % self.directory(
                ),
                'Reader.AllowedRuns = @OnlineEnv.DeferredRuns;',
                'Reader.DeleteFiles = %s;' % deleteFiles,
                'Reader.SaveRest    = false;',
                'Reader.FilePrefix  = "%s";' % self.__prefix,
                'Reader.PauseSleep  = 5;',
                'Reader.InitialSleep = 1;',
                'Reader.MaxPauseWait = 1;',
                'MEPManager.PartitionBuffers = @OnlineEnv.PartitionBuffers;',
                'MEPManager.PartitionName    = @OnlineEnv.PartitionName;',
                'MEPManager.PartitionID      = @OnlineEnv.PartitionID;',
                'MEPManager.Buffers          = @OnlineEnv.MEPrx_Buffers;'
                'Logger.OutputLevel          = @OnlineEnv.OutputLevel;']
        return self.generate_options('Reader.opts', '\n'.join(opts))

    def broken_opts(self):
        return self.generate_options('brokenhosts', '')

    def wait(self):
        self._wait()

    def __callback(self, tag, val):
        if not val:
            return
        self.__callback_lock.acquire()
        try:
            if (self.state() == State.UNKNOWN
                    and val.strip().startswith("NOT_READY")):
                self.logger().info("MEPReader %d went to NOT_READY" % tag)
                self.set_state(State.NOT_READY)
                self._notify()
            elif (self.state() == State.NOT_READY
                  and val.strip().startswith("READY")):
                self.logger().info("MEPReader %d went to READY" % tag)
                self.set_state(State.READY)
                self._notify()
            elif (self.state() == State.READY
                  and val.strip().startswith("RUNNING")):
                self.logger().info("MEPReader %d went to RUNNING" % tag)
                self.set_state(State.RUNNING)
                self._notify()
            elif (self.stopping() and self.state() in
                  [State.RUNNING, State.PAUSED]
                  and val.strip().startswith("READY")):
                self.logger().info("MEPReader %d went to READY" % tag)
                self.set_state(State.READY)
                self._notify()
            elif (self.stopping() and self.state() == State.READY
                  and val.strip().startswith("NOT_READY")):
                self.logger().info("MEPReader %d went to NOT_READY" % tag)
                self.set_state(State.NOT_READY)
                self._notify()
            elif (self.__delete and self.state() == State.RUNNING
                  and val.strip().startswith("PAUSED")):
                self.logger().info("MEPReader %d went to PAUSED" % tag)
                self.set_state(State.PAUSED)
                self._notify()
            elif val.strip().startswith(self.state()):
                pass
            else:
                self.logger().warning("Unhandled transtion of MEPReader"
                                      " to state %s." % val.strip())
        except Exception:
            self.logger().debug("exception while waiting for MEPReader "
                                "to start up ...", exc_info=True)
        self.__callback_lock.release()


class MDFWriter(GaudiTestProcess):

    def __init__(self, logger, launcher, **kwargs):
        GaudiTestProcess.__init__(
            self, logger, launcher, kwargs.pop('priority'))
        self.__output_directory = kwargs.pop("output_directory")
        self.__streams = kwargs.pop(
            'streams', {'All': ['0xffffffff', '0xffffffff',
                                '0xffffffff', '0xffffffff']})
        self.__dim_svcs = []
        from multiprocessing import Lock
        self.__callback_lock = Lock()
        self.__writers = {}
        self.__states = {}

    def output_directory(self):
        return self.__output_directory

    def initialize(self):
        from copy import deepcopy
        env = {'LOGFIFO': self.log_fifo()}

        # Check if we need to use the device list
        n_running = 0
        for i, (suffix, bits) in enumerate(self.__streams.iteritems()):
            utgid = self.utgid("MDFWriter_%s" % suffix)
            service = utgid + "/status"
            self.logger().info("Starting MDF writer %d" % (i + 1))
            self.__dim_svcs += [pydim.dic_info_service(
                service, "C", self.__callback, pydim.MONITORED, 0, i, None)]
            writer_opts = self.writer_opts(suffix, i, bits)
            command = ['gentest.exe', 'libDataflow.so', 'dataflow_run_task',
                       '-msg=Dataflow_FmcLogger',
                       '-mon=Dataflow_DIMMonitoring', '-class=Class2',
                       '-opts=%s' % writer_opts]
            e = deepcopy(env)
            e.update({'UTGID': utgid})

            self.condition().acquire()
            self.__states[i] = State.UNKNOWN
            self.__writers[i] = (utgid, self.launch(utgid, command, e))
            self.condition().wait()
            self.condition().release()

            self.send_command(utgid, "configure")
            self.send_command(utgid, "start")

            n_running += 1
            self.logger().debug("%d Writers running out of %d" %
                                (n_running, len(self.__writers)))

        self.logger().info("All writers running.")
        self.set_state(State.RUNNING)

    def launch(self, utgid, command, env, do_exec=True, node=0):
        return self.launcher().launch(utgid, command, env, do_exec, node)

    def __utgids(self, reverse):
        return [e[1][0] for e in sorted(self.__writers.items(),
                                        key=itemgetter(0),
                                        reverse=reverse)]

    def stop(self):
        # Then stop process
        self._stopping = True
        for utgid in self.__utgids(True):
            self.send_command(utgid, "stop")
        self.set_state(State.READY)

    def finalize(self):
        if not self._stopping:
            return
        for utgid in self.__utgids(True):
            self.send_command(utgid, "reset")

        self.set_state(State.NOT_READY)

    def terminate(self):
        writers = sorted(self.__writers.values(),
                         key=itemgetter(0), reverse=True)
        for _, p in writers:
            self._terminate(p)

    def writer_opts(self, stream, i, bits):
        requirement = ("EvType=2;TriggerMask=%s;VetoMask=0,0,0,0;"
                       "MaskType=ANY;UserType=VIP;Frequency=PERC;"
                       "Perc=100.0" % ','.join(bits))
        opts = ['#pragma print off',
                '#include "%s"' % self.info_opts(),
                '#include "%s"' % self.mbm_opts(),
                '#include "%s"' % self.mon_opts(),
                'Manager.Services = {"Dataflow_MBMClient/MBM",',
                '                    "Dataflow_FileWriter/DskWriter",',
                '                    "Dataflow_FileWriterMgr/WrManager",',
                '                    "Dataflow_RunableWrapper/Wrap"};',
                'Manager.Runable = "Wrap";',
                'Wrap.Callable = "WrManager";',

                'MBM.PartitionBuffers = @OnlineEnv.PartitionBuffers;',
                'MBM.PartitionName    = @OnlineEnv.PartitionName;',
                'MBM.PartitionID      = @OnlineEnv.PartitionID;',
                'MBM.Buffers          = @OnlineEnv.DskWriter_Buffers;',
                'WrManager.Processors += {"DskWriter"};',
                'WrManager.Input       = @OnlineEnv.DskWriter_Input;',
                'DskWriter.SizeLimit   = 400;',
                'DskWriter.NodePattern = ".*";',
                'DskWriter.Requirements = {"%s"};' % requirement,
                'MessageSvc.fifoPath    = "%s";' % self.log_fifo(),
                'MessageSvc.LoggerOnly  = 1;',
                'MessageSvc.OutputLevel = 2;']

        od = os.path.realpath(self.__output_directory).strip('/')
        base_path, rest = od.split('/', 1)
        args = (rest, stream)
        opts += ['DskWriter.DeviceList = {"/%s"};' % base_path,
                 'DskWriter.FilePrefix = "/%s/%s_Run_";' % args]

        return self.generate_options('Writer_%02d.opts' % (i + 1),
                                     '\n'.join(opts))

    def __callback(self, tag, val):
        if not val:
            return
        self.__callback_lock.acquire()

        def set_state(tag, state):
            self.__states[tag] = state

        def __msg(tag, state):
            utgid = self.__writers[tag][0]
            return "%s went to %s." % (utgid, state)

        if (self.state(tag) == State.UNKNOWN
                and val.strip().startswith("NOT_READY")):
            self.logger().info(__msg(tag, "NOT_READY"))
            set_state(tag, State.NOT_READY)
            self._notify()
        elif (self.state(tag) == State.NOT_READY
              and val.strip().startswith("READY")):
            self.logger().info(__msg(tag, "READY"))
            set_state(tag, State.READY)
            self._notify()
        elif (self.state(tag) == State.READY
              and val.strip().startswith("RUNNING")):
            self.logger().info(__msg(tag, "RUNNING"))
            set_state(tag, State.RUNNING)
            self._notify()
        elif (self.state(tag) == State.RUNNING
              and val.strip().startswith("READY")):
            self.logger().info(__msg(tag, "READY"))
            set_state(tag, State.READY)
            self._notify()
        elif (self.state(tag) == State.READY
              and val.strip().startswith("NOT_READY")):
            self.logger().info(__msg(tag, "NOT_READY"))
            set_state(tag, State.NOT_READY)
            self._notify()
        else:
            self.logger().debug(__msg(tag, val.strip()))
        self.__callback_lock.release()

    def state(self, tag=None):
        if tag is None:
            return super(MDFWriter, self).state()
        return self.__states[tag]


class MEPRx(GaudiTestProcess):

    def __init__(self, logger, launcher, **kwargs):
        GaudiTestProcess.__init__(
            self, logger, launcher, kwargs.pop('priority'))
        self.__dim_svc = None
        self.__data_interface = kwargs.pop('data_interface', 'mona0801-d1')
        self.__utgid = None

        from multiprocessing import Lock
        self.__callback_lock = Lock()

    def initialize(self):
        self.__utgid = self.utgid("MEPRx_01")
        service = self.__utgid + "/status"
        self.logger().info("Starting MEPRx")
        self.__dim_svc = pydim.dic_info_service(
            service, "C", self.__callback, pydim.MONITORED, 0, 0, None)
        python_path = os.environ['PYTHONPATH']
        online_env = self.online_env()
        env = {'LOGFIFO': self.log_fifo(),
               'DATAINTERFACE': self.__data_interface,
               'TAN_NODE': self.__data_interface,
               'TAN_PORT': 'YES',
               'PYTHONPATH': os.path.dirname(online_env) + ":" + python_path}
        command = ['${GAUDIONLINEROOT}/../../InstallArea/${CMTCONFIG}/bin/GaudiOnlineExe.exe',
                   '${GAUDIONLINEROOT}/../../InstallArea/${CMTCONFIG}/lib/libGaudiOnline.so',
                   'OnlineTask', '-tasktype=LHCb::Class1Task', '-msgsvc=LHCb::FmcMessageSvc',
                   '-main=%s' % self.main_opts(), '-opt=%s' % self.reader_opts(), '-auto']
        self.__meprx = self.launch(self.__utgid, command, env)
        self._wait()

    def stop(self):
        # Then stop process
        self._stopping = True
        self.send_command(self.__utgid, "stop")

    def finalize(self):
        if not self._stopping:
            return
        self.send_command(self.__utgid, "reset")

        if self.__dim_svc:
            pydim.dic_release_service(self.__dim_svc)
            self.__dim_svc = None

    def terminate(self):
        self._terminate(self.process())

    def reader_opts(self):
        opts = ["from MooreTests import EventRequest",
                "EventRequest.runNetCons(source='MONA0801::LHCb_MONA0801_EventSrv')"]
        return r'command=%s ' % '; '.join(opts)

    def wait(self):
        self._wait()

    def __callback(self, tag, val):
        if not val:
            return
        self.__callback_lock.acquire()
        if self.state() == State.UNKNOWN and val.strip().startswith("RUNNING"):
            self.logger().info("MEPRx %d went to RUNNING" % tag)
            self.set_state(State.RUNNING)
            self._notify()
        elif (self.stopping() and self.state() == State.RUNNING
              and val.strip().startswith("READY")):
            self.logger().info("MEPRx %d went to READY" % tag)
            self.set_state(State.READY)
            self._notify()
        elif (self.stopping() and self.state() == State.READY
              and val.strip().startswith("NOT_READY")):
            self.logger().info("MEPRx %d went to NOT_READY" % tag)
            self.set_state(State.NOT_READY)
            self._notify()
        else:
            self.logger().debug(
                "MEPRX in state %s. Waiting for RUNNING" % val.strip())
        self.__callback_lock.release()


class Adder(GaudiTestProcess):

    def __init__(self, logger, launcher, **kwargs):
        GaudiTestProcess.__init__(
            self, logger, launcher, kwargs.pop('priority'))
        self.__dim_svc = None
        self.__utgid = None

        from multiprocessing import Lock
        self.__callback_lock = Lock()

    def initialize(self):
        self.__utgid = self.utgid("Adder_01")
        service = self.__utgid + "/status"
        self.logger().info("Starting Adder")
        self.__dim_svc = pydim.dic_info_service(
            service, "C", self.__callback, pydim.MONITORED, 0, 0, None)
        env = {'LOGFIFO': self.log_fifo()}
        command = ['${GAUDIONLINEROOT}/../../InstallArea/${CMTCONFIG}/bin/GaudiOnlineExe.exe',
                   '${GAUDIONLINEROOT}/../../InstallArea/${CMTCONFIG}/lib/libGaudiOnline.so',
                   'OnlineTask', '-tasktype=LHCb::Class1Task', '-msgsvc=LHCb::FmcMessageSvc',
                   '-main=%s' % self.main_opts(),
                   '-opt=%s' % self.adder_opts(), '-auto']
        self.__meprx = self.launch(self.__utgid, command, env)
        self._wait()

    def finalize(self):
        if not self._stopping:
            return

        self.send_command(self.__utgid, 'reset')

        if self.__dim_svc:
            pydim.dic_release_service(self.__dim_svc)
            self.__dim_svc = None

    def stop(self):
        # Then stop process
        self._stopping = True
        self.send_command(self.__utgid, 'stop')

    def terminate(self):
        self._terminate(self.process())

    def adder_opts(self):
        opts = ['#pragma print off',
                '#include "%s"' % self.info_opts(),
                'ApplicationMgr.ExtSvc                += {"CntrPub/CounterPublisher"};',
                'ApplicationMgr.EventLoop             = "LHCb::OnlineRunable/EmptyEventLoop";',
                'ApplicationMgr.Runable               = "LHCb::OnlineRunable/Runable";',
                'ApplicationMgr.HistogramPersistency  = "NONE";',
                'ApplicationMgr.EvtSel                = "NONE";',
                'Runable.Wait                         = 3;',
                'HistogramPersistencySvc.Warnings   = false;',
                'CounterPublisher.MyName              = "<part>_Adder";',
                'CounterPublisher.PartitionName       = @OnlineEnv.PartitionName;',
                'CounterPublisher.TaskPattern         = "<part>_(.*)Moore[12]_[0-9](?!00)[0-9]{2}";',
                'CounterPublisher.ServicePattern      = "MON_<part>_(.*)/Counter/";',
                'CounterPublisher.AdderClass          = "counter";',
                'CounterPublisher.CounterPattern      = "(.*)(Runable|EventSelector)(.*)";',
                'CounterPublisher.InDNS               = "<node>";',
                'CounterPublisher.TrendingOn          = false;',
                'MessageSvc.fifoPath    = "$LOGFIFO";',
                'MessageSvc.OutputLevel = @OnlineEnv.OutputLevel;',
                'EventLoopMgr.OutputLevel             = 5;',
                'EventLoopMgr.Warnings                = false;',
                'HistogramPersistencySvc.OutputLevel  = 5;',
                'HistogramPersistencySvc.Warnings     = false;']
        return self.generate_options('Adder.opts', '\n'.join(opts))

    def __callback(self, tag, val):
        if not val:
            return
        self.__callback_lock.acquire()
        try:
            if (self.state() == State.UNKNOWN
                    and val.strip().startswith("RUNNING")):
                self.logger().info("Adder %d went to RUNNING" % tag)
                self.set_state(State.RUNNING)
                self._notify()
            elif (self.stopping() and self.state() == State.RUNNING
                  and val.strip().startswith("READY")):
                self.logger().info("Adder %d went to READY" % tag)
                self.set_state(State.READY)
                self._notify()
            elif (self.stopping() and self.state() == State.READY
                  and val.strip().startswith("NOT_READY")):
                self.logger().info("Adder %d went to NOT_READY" % tag)
                self.set_state(State.NOT_READY)
                self._notify()
            else:
                self.logger().debug(
                    "MEPRX in state %s. Waiting for RUNNING" % val.strip())
        except Exception:
            self.logger().debug(
                "exception while waiting for MEPRX to start up ...",
                exc_info=True)
        self.__callback_lock.release()
