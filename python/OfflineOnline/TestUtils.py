def mdf_mep(filename):
    fn, ext = filename.rsplit('.', 1)
    return ext.upper() if ext.upper() in ('MDF', 'MEP') else 'REST'

def partition(seq, key):
    from collections import defaultdict
    d = defaultdict(set)
    for x in seq:
        d[key(x)].add(x)
    return d

def setup_loggin(name, log_file):
    import sys
    import logging
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler(log_file)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)
    return logger
