import exceptions
import psutil

class BenchmarkProcessBase( object ):
    def __init__(self, logger, target, can_wait = False):
        from multiprocessing import Condition, Pipe
        self.__condition = Condition()
        self.__process_end, self.__master_end = Pipe()
        self.__can_wait = can_wait

        from multiprocessing import Process
        self.__process = Process(target = target)

        self.__logger = logger

    def condition(self):
        return self.__condition

    def process(self):
        return self.__process

    def start( self ):
        self.__process.start()


    def join( self ):
        self.__process.join()

    def can_wait(self):
        return self.__can_wait

    def logger(self):
        return self.__logger

    def has_command(self, *args):
        if len(args) == 0:
            return self.__process_end.poll()
        elif len(args) == 1:
            return self.__process_end.poll(args[0])
        else:
            raise ValueError("wrong number of arguments, must be 0 or 1.")

    def get_command(self):
        return self.__process_end.recv()

    def put_command(self, data):
        self.__master_end.send(data)

    def close_command(self):
        self.__master_end.close()

    def has_status(self, *args):
        if len(args) == 0:
            return self.__master_end.poll()
        elif len(args) == 1:
            return self.__master_end.poll(args[0])
        else:
            raise ValueError("wrong number of arguments, must be 0 or 1.")

    def get_status( self ):
        return self.__master_end.recv()

    def put_status(self, data):
        self.__process_end.send(data)

    def close_status(self):
        self.__process_end.close()

class CPUMeasurer(BenchmarkProcessBase):
    def __init__(self, logger):
        BenchmarkProcessBase.__init__(self, logger, self.run)
        self.__results = []
        self.__running = False
        self.__time_stamps = []

    def process_id(self):
        return "CPU"

    def initialize(self):
        self.put_status("initialized")

    def run( self ):
        ## old_stdout = os.dup( sys.stdout.fileno() )
        ## fd = os.open( '%s.log' % self._name, os.O_CREAT | os.O_WRONLY )
        ## os.dup2( fd, sys.stdout.fileno() )
        import time
        self.initialize()
        while True:
            if not self.__running:
                command = self.get_command()
                if command == "run":
                    self.__running = True
                else:
                    self.__stop()
                    break
            else:
                has_command = self.has_command()
                if has_command:
                    command = self.get_command()
                    if command == "stop":
                        self.__running = False
                        if len(self.__results) != 0:
                            self.__results.pop(0)
                        if len(self.__results) != 0:
                            self.__results.pop()
                        self.__stop()
                        break
                if self.__running:
                    self.__results.append(psutil.cpu_percent(percpu = True))
                    self.__time_stamps.append(time.time())
                    time.sleep(10)

        ## os.close( fd )
        ## os.dup2( old_stdout, sys.stdout.fileno() )

    def terminate(self):
        pass

    def __stop(self):
        self.put_status({'time_stamps' : self.__time_stamps,
                         'CPU'         : self.__results})
        self.logger().info("%s is done" % self.process_id())
        self.put_status("done")
        self.close_status()
