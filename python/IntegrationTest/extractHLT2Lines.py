from TCKUtils.utils import *

settings = {
     'TCKData': 'TCKData/config.cdb',
     'TCK': 0x2AA11609,
     'outputFile': 'hlt2list.py',
}

# Open the cdb
cas = ConfigAccessSvc(File=settings['TCKData'], Mode='ReadOnly')
# Get the HLT2 lines referenced by this TCK
hlt2_lines = getHlt2Lines(settings['TCK'], cas=cas)
# Write the output list to outputFile
with open(settings['outputFile'], 'w') as _file:
    # Store the string rep in a file so we can use it in another script
    _file.write(str(hlt2_lines))
