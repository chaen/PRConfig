#!/usr/bin/env python
from __future__ import print_function
from os import getcwd, path, makedirs
import optparse
import subprocess
from functools import partial

# These are some global parameters which are used throughout the script
# TODO move this to a global struct?
TLD = getcwd()

OutputPath = None
WorkPath = None
TCKPath = None

debugFlag = False

ScriptPath = path.abspath(path.dirname(__file__))

MooreEnv = None
DaVinciEnv = None

def setDEBUG(debug):
    """"
    This sets the global flag for debugging. When true all stdout which is generated is dumped to the stdout whilst running
    Args:
        debug (bool): Boolean to set the debug flag
    """
    if debug:
        global debugFlag
        debugFlag = True

def flush_streams(pipe, outputFile=None):
    '''
    This flushes the stdout/stderr streams to the outputFile which is provided correctly
    This is accomplished by looping over both the stdout and stderr whilst they're active and taking the output from each pipe as it's flushed and redirecting it
    Args:
        pipe (subprocess.PIPE): pipe object from subprocess.PIPE
        outputFile (file): file object which should have the streams written to it
    Returns:
        list: stdout, stderr, 2 strings which contain the stdout and stderr from the process which was run
    '''
    stdout, stderr = '', ''

    def printFlush(next_line, target):
        print("%s" % next_line, file=target, end='')
        target.flush()

    with pipe.stdout or pipe.stderr:
        for stream in pipe.stdout, pipe.stderr:
            if stream:
                for next_line in iter(stream.readline, b''):
                    if outputFile:
                        printFlush(next_line, outputFile)
                    if debugFlag:
                        printFlush(next_line, sys.stdout)
                    stdout += next_line
    return stdout, stderr

def setOutputPath(outputPath):
    """
    This sets the output path of the test
    Args:
        outputPath (str): This path is the path where all of the generated files for the test should be stored
    """
    global OutputPath
    OutputPath = outputPath
    if not path.exists(OutputPath):
        makedirs(OutputPath)

def setWorkPath(workPath):
    """
    This sets the working dir of the test
    Args:
        workPath (str): This path is the path where all of the generated files for the test should be stored
    """
    global WorkPath
    WorkPath = workPath
    if not path.exists(WorkPath):
        makedirs(WorkPath)

def setTCKPath(tckPath):
    """
    This method sets the TCK path as appropriate
    Args:
        tckPath (str): This is the path where the config.cdb should be found
    """
    global TCKPath
    TCKPath = tckPath

def moore_exec(cmd, logfile=None):
    """
    This is a short wrapper script for executing a given command in the Moore nightly path
    Args:
        cmd (str): This is command we want to run
        logfile (str): This is the name of the log file which will be created with the stdout/err
    """

    full_cmd = MooreEnv + " "
    full_cmd += cmd
    return sys_exec(full_cmd, logfile, WorkPath)

def davinci_exec(cmd, logfile=None):
    """
    This is a short wrapper script for executing a given command in the DaVinci nightly path
    Args:
        cmd (str): This is command we want to run
        logfile (str): This is the name of the log file which will be created with the stdout/err
    """

    full_cmd = DaVinciEnv + " "
    full_cmd += cmd
    return sys_exec(full_cmd, logfile, WorkPath)


def sys_exec(command, outputLog=None, cwd=None, **kwds):
    """
    This is a wrapper method for subprocess.Popen which configures it in a useful way for running tests
    Args:
        command (str): This is the string of the command we want to run within a new shell
        outputLog (str): This is the path of the log file which is to be created which will get _both_ stdout and stderr
        cwd (str): This is the directory where the command should be run
        kwds (dict): These are additional kwds which can be passed to subprocess.Popen if wanted
    Returns:
        str: This is the standard output of the command
    """
    if not cwd:
        cwd = TLD
    cmd = subprocess.Popen(command, shell=True, cwd=cwd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **kwds)
    if outputLog:
        with open(outputLog, 'w') as outputFile:
            stdout, stderr = flush_streams(cmd, outputFile)
    else:
        stdout, stderr = flush_streams(cmd)
    cmd.wait()
    if cmd.returncode != 0:
        err_msg = 'Error executing command: "%s"' % command
        if stderr:
            err_msg += "\nError message was:\n%s\n" % str(stderr)
        err_msg += "\nReturned Code was: %s" % cmd.returncode
        raise Exception(err_msg)
    return stdout


def prepareTCKOptions(HLT1, HLT2):
    """
    This method edits the `TCKOptions.py` file which is used for TCK construction to match the job requirements
    Args:
        HLT1 (str): This is the string of the TCK for HLT1 we want to use
        HLT2 (str): This is the string of the TCK for HLT2 we want to use
    """

    L0 = '0x' + HLT1[-4:]

    TCKOptions = path.join(OutputPath, 'TCKOptions.py')
    moore_exec("cp -v '$MOOREROOT/tests/options/TCK/TCKOptions.py' '%s'" % TCKOptions)
    with open(TCKOptions, 'r') as _file:
        filedata = _file.read()
    filedata = filedata.replace(getLine(TCKOptions, 'TCKData'), '"TCKData": "%s",' % TCKPath)
    filedata = filedata.replace(getLine(TCKOptions, 'L0TCK'), '"L0TCK": "%s",' % L0)
    filedata = filedata.replace(getLine(TCKOptions, 'Hlt1TCK'), '"Hlt1TCK": "%s",' % HLT1)
    filedata = filedata.replace(getLine(TCKOptions, 'Hlt2TCK'), '"Hlt2TCK": "%s",' % HLT2)
    with open(TCKOptions, 'w') as _file:
        _file.write(filedata)

def buildTCK1():
    """
    This method constructs a new HLT1 TCK
    """

    print("\nBuilding TCK1")

    print("Re-run L0")
    moore_exec("gaudirun.py '$MOOREROOT/tests/options/TCK/RedoL0.py' '%s/TCKOptions.py'" % OutputPath, path.join(OutputPath, 'RedoL0.log'))

    print("Getting the TCK database")
    if not path.exists(TCKPath):
        makedirs(TCKPath)
    #moore_exec("svn export svn+ssh://svn.cern.ch/reps/lhcb/DBASE/trunk/TCK/HltTCK/config.cdb %s/config.cdb" % TCKPath)
    moore_exec("cp -v '$HLTTCKROOT/config.cdb' '%s/config.cdb'" % TCKPath)

    print("Creating TCK1")
    moore_exec("gaudirun.py '$MOOREROOT/tests/options/TCK/CreateTCK1.py' '%s/TCKOptions.py'" % OutputPath, path.join(OutputPath, 'CreateTCK1.log'))

    print("Mapping Configuration to TCK number")
    moore_exec("python '$MOOREROOT/tests/options/TCK/CreateTCKEntry.py' '%s' Hlt1TCK --options '%s/TCKOptions.py'" % (path.join(OutputPath, 'CreateTCK1.log'), OutputPath),
		path.join(OutputPath, 'CreateTCKEntry1.log'))

    print("Testing TCK1")
    moore_exec("gaudirun.py '$MOOREROOT/tests/options/TCK/TestTCK1.py' '%s/TCKOptions.py'" % OutputPath, path.join(OutputPath, 'TestTCK1.log'))


def buildTCK2():
    """
    This method constructs a new HLT2 TCK
    """

    print("\nBuilding TCK2")

    print("Creating TCK2")
    moore_exec("gaudirun.py '$MOOREROOT/tests/options/TCK/CreateTCK2.py' '%s/TCKOptions.py'" % OutputPath, path.join(OutputPath, 'CreateTCK2.log'))

    print("Mapping Configuration to TCK number")
    moore_exec("python '$MOOREROOT/tests/options/TCK/CreateTCKEntry.py' '%s' Hlt2TCK --options '%s/TCKOptions.py'" % (path.join(OutputPath, 'CreateTCK2.log'), OutputPath),
                path.join(OutputPath, 'CreateTCKEntry2.log'))

    print("Testing TCK2")
    moore_exec("gaudirun.py '$MOOREROOT/tests/options/TCK/TestTCK2.py' '%s/TCKOptions.py'" % OutputPath, path.join(OutputPath, 'TestTCK2.log'))


def getLine(inputFile, searchStr):
    """
    This function returns the (first) line in a file which contains the string searchStr
    Args:
        inputFile (str): This is the file to search for the substring
        searchStr (str): This is the string to search for in the file
    Returns:
        str: This retuns the contents of the line of the file which contains the searchStr
    """
    with open(inputFile, 'r') as _file:
        filedata = _file.read()
    return findLine(filedata, searchStr)


def findLine(input_str, searchStr):
    """
    This function returns the (first) line in a multi-line string which contains the string searchStr
    Args:
        input_str (str): This is the string which is searched for the substring
        searchStr (str): This is the string which is searched for within the file
    Returns:
        str: This returns the contents of the line from the string which contains the searchStr
    """
    for _line in input_str.split('\n'):
        if searchStr in _line:
            return _line
    raise Exception("String: \"%s\" not found!" % (searchStr,))


def runSplitRateTestHLT1(TFDB, TCK, evtMax):
    """
    This method runs the Moore Rate test for HLT1
    Args:
        TFDB (str): This is the TFDB dataset to use for running the HLT1 rate test
        TCK (str): This is the TCK dataset for running the 
        evtMax (str): This is the max number of files to pass through the HLT1 TCK
    """

    rateTestCmd = "python '$PRCONFIGROOT/python/MooreTests/Moore_RateTest.py' --evtmax=%s " % evtMax
    rateTestCmd += "--TFDB=%s --tuplefile='%s/tuples_hlt1.root' --outputFile='%s/MooreRateTestOutput_Hlt1.dst' " % (TFDB, OutputPath, OutputPath)
    rateTestCmd += "--L0TCK=0x%s --TCK=%s --TCKData='%s' --split=Hlt1 " % (TCK[-4:], TCK, TCKPath)

    print("\n\nRunning: %s" % rateTestCmd)
    moore_exec(rateTestCmd, path.join(OutputPath, 'Moore_RateTest_HLT1.log'))


def runSplitRateTestHLT2(TFDB, TCK, evtMax):
    """
    This method runs the Moore Rate test for HLT2
    Args:
        TFDB (str): This is the TFDB dataset to use for running the HLT2 TCK
        TCK (str): This is the TCK which corresponds to the HLT2
        evtMax (str): This is the maximum number of events to use in the rate test
    """

    rateTestCmd = "python '$PRCONFIGROOT/python/MooreTests/Moore_RateTest.py' --evtmax=%s " % evtMax
    rateTestCmd += "--TFDB=%s --tuplefile='%s/tuples_hlt2.root' --inputdata='%s/MooreRateTestOutput_Hlt1.dst' " % (TFDB, OutputPath, OutputPath)
    rateTestCmd += "--outputFile='%s/MooreRateTestOutput_Hlt2.dst' " % OutputPath
    rateTestCmd += "--L0TCK=0x%s --TCK=%s --TCKData='%s' --split=Hlt2 " % (TCK[-4:], TCK, TCKPath)

    print("\n\nRunning: %s" % rateTestCmd)
    moore_exec(rateTestCmd, path.join(OutputPath, 'Moore_RateTest_HLT2.log'))


def extractHlt2(TCK):
    """
    This function gets the full list of HLT2 lines from a given TCK
    Inputs:
        TCK (str): This is the TCK to look for HLT2 decisons associated with
    Returns:
        str containing the List of Hlt2 decisions associayted with this TCK
    """

    trigger_list_path = path.join(OutputPath, 'hlt2list.py')

    if not path.exists(trigger_list_path):

        print("\nExtracting HLT2 lines from TCK: %s" % TCK)

        with open(path.join(ScriptPath, 'extractHLT2Lines.py'), 'r') as _file:
            listHlt2_str = _file.read()

        TCKdata = "'TCKData': '%s/config.cdb'," % TCKPath
        hlt2_key = "'TCK': %s," % TCK
        outputFile = "'outputFile': '%s'," % trigger_list_path

        replace_dict = {"'TCKData':": TCKdata, "'TCK':": hlt2_key, "'outputFile':": outputFile}

        listHlt2_str = constructScript(listHlt2_str, replace_dict)

        extractHlt2Script = path.join(OutputPath, 'extractHLT2Lines.py')

        with open(extractHlt2Script, 'w') as _file:
            _file.write(listHlt2_str)

        moore_exec("python '%s'" % extractHlt2Script, path.join(OutputPath, 'extractHLT2.log'))

    else:
        print("Using trigger lines from: %s" % trigger_list_path)


def runTesla(TFDB, evtMax):
    """
    Construct/Edit the Tesla script and run tesla once over the data to 'resurrect' things
    Args:
        TFDB (str): This is the TFDB that was used as input for running the trigger over
        TCK (str): This is the TCK which was used for running the HLT2
        evtMax (str): This is the number of events to run through Tesla
    """

    with open(path.join(ScriptPath, 'runTeslaOnly.py'), 'r') as _file:
        runTeslaOnlyScript = _file.read()

    trigger_list_path = path.join(OutputPath, 'hlt2list.py')
    # Read in the Hlt2 lines associated with the TCK
    with open(trigger_list_path, 'r') as _file:
        trigger_lines = _file.read()

    TFDB_input = "'TFDB': '%s'," % TFDB
    config_cdb = "'configCDB': '%s/config.cdb'," % TCKPath
    input_file = "'inputFile': '%s'," % path.join(OutputPath, 'MooreRateTestOutput_Hlt2.dst')
    output_file = "'outputFile': '%s'," % path.join(OutputPath, 'TeslaOutput.dst')
    evt_max = "'evtMax': '%s'," % evtMax
    trigger_input = "'TriggerLines': %s," % trigger_lines

    replace_dict = {"'TFDB'         :": TFDB_input, "'configCDB'    :": config_cdb,
                    "'inputFile'    :": input_file, "'outputFile'   :": output_file,
                    "'evtMax'       :": evt_max,    "'TriggerLines' :": trigger_input}

    tesla_only_str = constructScript(runTeslaOnlyScript, replace_dict)

    tesla_only_script = path.join(OutputPath, 'runTeslaOnly.py')

    with open(tesla_only_script, 'w') as _file:
        _file.write(tesla_only_str)

    davinci_exec("gaudirun.py '%s'" % tesla_only_script, path.join(OutputPath, 'TeslaOnlyStepOutput.log'))


def constructScript(input_str, replace_dict):
    """
    This method replaces lines in the input_str based upon the contents of the replace_dict
    The keys in the replace_dict indicate a substring to search for in the file. The first line matching this is then replaced with the corresponding value.
    Args:
        input_str (str): This is the multi-line string which contains the input we want to modify
        replace_dict (dict): This is the dict which contains the search replace information for the given string
    Returns:
        str: This is the modified version of the input_str
    """
    output_str = input_str
    for k, v in replace_dict.iteritems():
        output_str = output_str.replace(findLine(input_str, k), v)

    return output_str


def runDaVinci(TFDB, evtMax):
    """
    Construct/Edit the DaVinci script and run tesla once over the data to 'resurrect' things
    Args:
        TFDB (str): This is the TFDB that was used as input for running the trigger over
        evtMax (str): This is the number of events to run through Tesla
    """

    with open(path.join(ScriptPath, 'runDaVinciOnly.py'), 'r') as _file:
        runDavinciOnlyScript = _file.read()

    TFDB_input = "'TFDB': '%s'," % TFDB
    config_cdb = "'configCDB': '%s'," % path.join(OutputPath, 'TCKData', 'config.cdb')
    input_file = "'inputFile': ['%s',]," % path.join(OutputPath, 'TeslaOutput.dst')
    output_file = "'outputFile': '%s'," % path.join(OutputPath, 'DaVinciOutput.dst')
    evt_max = "'evtMax': '%s'," % evtMax

    replace_dict = {"'TFDB'       :": TFDB_input, "'configCDB'  :": config_cdb,
                    "'inputFile'  :": input_file, "'outputFile' :": output_file,
                    "'evtMax'     :": evt_max}

    davinci_script = constructScript(runDavinciOnlyScript, replace_dict)

    davinci_only_script = path.join(OutputPath, 'runDaVinciOnly.py')

    davinci_only_log = path.join(OutputPath, 'DaVinciOnlyStepOutput.log')

    with open(davinci_only_script, 'w') as _file:
        _file.write(davinci_script)

    davinci_exec("gaudirun.py '%s'" % davinci_only_script, davinci_only_log)


def buildTCK():
    """
    This is a small wrapper function calling all of the steps to create a new split TCK for HLT1 and HLT2
    """
    buildTCK1()
    buildTCK2()


def runRateTest(TFDB, HLT1, HLT2, evtMax):
    """
    This is a small wrapper script which actually runs the Moore Rate tests using the new TCK
    Args:
        TFDB (str): This is the TFDB dataset which is used for running the Moore rate tests
        HLT1 (str): This is the TCK to be used for HLT1 rate testing
        HLT2 (str): This is the TCK to be used for HLT2 rate testing
        evtMax (str): This is the maximum number of events to use in the rate test
    """
    runSplitRateTestHLT1(TFDB, HLT1, evtMax)
    runSplitRateTestHLT2(TFDB, HLT2, evtMax)


def main(*args, **kwds):
    """
    This is the main method for chaining a run of
    Moore+Tesla+DaVinci over a sample of data from the TFDB
    """

    parser = optparse.OptionParser( usage = "usage: %prog [options]" )

    default_Moore = "lb-run --nightly-cvmfs --nightly lhcb-head Moore/HEAD"
    default_DaVinci = "lb-run --nightly-cvmfs --nightly lhcb-head DaVinci/HEAD"

    parser.add_option( "--MooreEnv", action = "store", dest = "MooreEnv",
                       default = default_Moore, help = "This command prepends tasks to be executed within a Moore env")

    parser.add_option( "--DaVinciEnv", action = "store", dest = "DaVinciEnv",
                       default = default_DaVinci, help = "This command prepends tasks to be executed within a DaVinci env")

    parser.add_option( "--outputPath", action = "store", dest = "Output",
                       default = path.join(TLD, 'output'), help = "Output path where the outputs from the various steps of the test are stored")

    parser.add_option( "--workPath", action = "store", dest = "Work",
                       default = path.join(TLD, 'output'), help = "Path where the various steps are to be run from as they may generate temporary files")

    parser.add_option( "--TCKPath", action = "store", dest = "TCKPath",
                       default = path.join(TLD, 'output', 'TCKPath'), help = "Path where the 'config.cdb' database is expected to be stored")

    parser.add_option( "--HLT1", action = "store", dest = "HLT1",
                       default = "0x1AA11609", help = "TCK to use for HLT1 trigger")

    parser.add_option( "--HLT2", action = "store", dest = "HLT2",
                       default = "0x2AA11609", help = "TCK to use for HLT2 trigger")

    parser.add_option( "--TCKData", action = "store", dest = "TFDB",
                       default = "2016NB_25ns_L0Filt0x160B", help = "TCKDataset to use e.g. 2016NB_25ns_L0Filt0x160B")

    parser.add_option( "-n", "--evtmax", type="int", action = "store", dest = "evtMax",
                       default = 30000, help = "Number of events to run over" )

    parser.add_option( "--debug", action="store_true", dest = "debug",
                       default = False, help = "Debug the script by printing everything out at every step")

    parser.add_option( "--useLHCbPR2", action="store_true", dest = "usePR2Path",
                       default = False, help = "Temporary flag to allow for setting the environment to match LHCBPR2 workslow")

    # Parse the arguments
    (options, args) = parser.parse_args()

    if options.HLT1[-4:] != options.HLT2[-4:]:
        print("ERROR: Error in configuration, HLT1 and HLT2 have a different L0!")
        sys.exit(-1)
    if options.HLT1[-4:] != options.TFDB[-4:]:
        print("WARNING: HLT generated and TCKData use different L0, be aware")

    # Configurable objects
    HLT1 = options.HLT1
    HLT2 = options.HLT2
    TFDB = options.TFDB
    evtMax = options.evtMax

    global MooreEnv
    MooreEnv = options.MooreEnv
    global DaVinciEnv
    DaVinciEnv = options.DaVinciEnv

    if options.usePR2Path:
        # Plan is to replace this with the abilty to pass "lb-run --use=PRConfig -c {platform} --user-area={build} {app_name}/{app_version}" in the options
        MooreEnv = "lb-run --use=PRConfig -c x86_64-slc6-gcc49-opt --user-area=../build Moore/2016-patches"
        DaVinciEnv = "lb-run --use=PRConfig -c x86_64-slc6-gcc49-opt --user-area=../build DaVinci/2016-patches"

    setOutputPath(options.Output)
    setWorkPath(options.Work)
    setTCKPath(options.TCKPath)
    setDEBUG(options.debug)


    print("")
    print("Config:")
    print("Using TCKs: %s, %s" % (HLT1, HLT2))
    print("Using %s events from %s" % (evtMax, TFDB))
    print("Using Moore Env: '%s'" % MooreEnv)
    print("Using DaVinci Env: '%s'" % DaVinciEnv)

    prepareTCKOptions(HLT1, HLT2)

    # If the TCK private DB doesn't yet exist on disk construct it
    if not path.exists(TCKPath):
        print("\nBuilding TCK:")
        buildTCK()
    else:
        print("\nAttempting to use TCK at: '%s/config.cdb'" % TCKPath)

    # If the output from the Rate test doesn't exist run it
    if not path.exists(path.join(OutputPath, 'MooreRateTestOutput_Hlt2.dst')):
        print("\nRunning Rate Tests:")
        runRateTest(TFDB, HLT1, HLT2, evtMax)
    else:
        print("\nRate test output found, skipping RateTest")

    # Run tesla over the output from MooreRate test
    print("\nRun Tesla:")
    extractHlt2(HLT2)
    runTesla(TFDB, evtMax)

    # Run DaVinci over this to generate a tuple
    print("\nRun DaVinci")
    runDaVinci(TFDB, evtMax)


if __name__ == "__main__":
    """
    We probably only want to run this from the command line but you never know

    Usage is as simple as: ./runIntegrationTest.py
    """
    main()

