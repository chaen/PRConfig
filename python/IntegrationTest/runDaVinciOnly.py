from Configurables import ConfigCDBAccessSvc, DecayTreeTuple, DaVinci, MessageSvc
from DecayTreeTuple import Configuration
from PRConfig import TestFileDB
from GaudiConf.IOHelper import IOHelper
from Gaudi.Configuration import *

# Settings which are used in this script
settings = {
    'TFDB'       : '2016NB_25ns_L0Filt0x1609',
    'configCDB'  : 'MooreDev_v26r0/TCKData/config.cdb',
    'inputFile'  : ['teslaOutputFile.dst',],
    'outputFile' : 'DVntuple.root',
    'evtMax'     : '-1',
    'dttDecay'   : "'[D*(2010)+ -> (D0 -> K- pi+) pi+]CC'",
    'dttInputs'  : ['/Event/Turbo/Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo/Particles',],
}

# Path to private TCK - needed due to private TCK being used
cas = ConfigCDBAccessSvc(File=settings['configCDB'], Mode='ReadOnly')

# Private dst which has been generated by Tesla
ioh = IOHelper()
ioh.setupServices()
ioh.inputFiles(settings['inputFile'])

# Set up a DTT to make an nTuple
dtt = DecayTreeTuple("IntegratedTestDTT")
# Not sure why this is here
dtt.WriteP2PVRelations = False
# Needed for running over Turbo
dtt.InputPrimaryVertices = '/Event/Turbo/Primary'
# Add the decay of interest to DaVinci
dtt.Decay = settings['dttDecay']
# Add the input path of the Turbo objects
dtt.Inputs = settings['dttInputs']
# Add the TTK to get some output
dtt.ToolList+=["TupleToolKinematic",]

# Construct and configure the DaVinci app
m_DaVinci = DaVinci()
# Set the conddb and dddb
TestFileDB.test_file_db[settings['TFDB']].setqualifiers(configurable=m_DaVinci)
# Add the DTT to DaVinci to process this line
m_DaVinci.UserAlgorithms += [dtt]
# Where to place the output tuple
m_DaVinci.TupleFile = settings['outputFile']
# Where to store the histogram
m_DaVinci.HistogramFile = 'daVinci_histo.root'
# How many events to process
m_DaVinci.EvtMax = settings['evtMax']

# Configure the output format
msgSvc = MessageSvc()
msgSvc.OutputLevel = 3
msgSvc.Format = "% F%60W%S%7W%R%T %0W%M"

