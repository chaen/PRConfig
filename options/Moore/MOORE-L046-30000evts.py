from Configurables import Moore
from Gaudi.Configuration import *

importOptions("$HLTSETTINGSROOT/python/HltSettings/Physics_September2012.py")
Moore().L0=False
Moore().ReplaceL0BanksWithEmulated= False
from Configurables import L0MuonAlg
L0MuonAlg( "L0Muon" ).L0DUConfigProviderType = "L0DUConfigProvider"
Moore().Verbose = True
Moore().ThresholdSettings = 'Physics_September2012'
Moore().EvtMax = 30000
EventSelector().PrintFreq = 1
Moore().DataType        = "2012"
Moore().Simulation      = False
Moore().DDDBtag         = "dddb-20130111"
Moore().CondDBtag       = "cond-20130111"
Moore().inputFiles = [
'/castor/cern.ch/user/e/evh/131883/131883_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131884/131884_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131885/131885_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131886/131886_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131887/131887_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131888/131888_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131889/131889_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131890/131890_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131891/131891_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131892/131892_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131893/131893_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131894/131894_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131895/131895_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131896/131896_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131897/131897_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131898/131898_0x0046_NB_L0Phys_00.raw',
'/castor/cern.ch/user/e/evh/131899/131899_0x0046_NB_L0Phys_00.raw'
]

