from Configurables import Moore
from Gaudi.Configuration import *

#importOptions("$HLTSETTINGSROOT/python/HltSettings/Physics_September2012.py")

Moore().L0 = False
Moore().ReplaceL0BanksWithEmulated = False
Moore().ForceSingleL0Configuration = False

from Configurables import L0MuonAlg
L0MuonAlg( "L0Muon" ).L0DUConfigProviderType = "L0DUConfigProvider"

Moore().Verbose = True
Moore().ThresholdSettings = 'Physics_September2012'

Moore().EvtMax = 10000
EventSelector().PrintFreq = 100
from Configurables import TimingAuditor, SequencerTimerTool
p = TimingAuditor()
p.addTool(SequencerTimerTool, name = "TIMER")
p.TIMER.NameSize=100


Moore().DataType        = "2012"
Moore().Simulation      = False
Moore().UseDBSnapshot   = False
Moore().DDDBtag         = "dddb-20130111"
Moore().CondDBtag       = "cond-20130111"
Moore().inputFiles = [
"/opt/dirac/lhcbpr/files/131883_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131884_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131885_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131886_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131887_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131888_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131889_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131890_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131891_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131892_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131893_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131894_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131895_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131896_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131897_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131898_0x0046_NB_L0Phys_00.raw",
"/opt/dirac/lhcbpr/files/131899_0x0046_NB_L0Phys_00.raw",
]

