#!/usr/bin/env gaudirun.py
#
# Minimal file for running Moore from python prompt
# Syntax is:
#   gaudirun.py ../options/Moore.py
# or just
#   ../options/Moore.py
#
import Gaudi.Configuration
#from Moore.Configuration import Moore
from Configurables import Moore




# if you want to generate a configuration, uncomment the following lines:
#Moore().generateConfig = True
#Moore().configLabel = 'Commissioning, no HLT2, no HLT2 technical lines, strippedEndSequence, disable L0-SPD'


from LHCbKernel.Configuration import *
from Gaudi.Configuration import *
## from Configurables import bankKiller


from Configurables import L0Conf, HltConf
#L0Conf().TCK = '0x0029'
 
L0Conf().TCK = '0x0044'

#L0Conf().TCK = "0x1710"
#'0x1710'
#HltConf().L0TCK = '0x1710'
#Moore().L0=True
#Moore().ReplaceL0BanksWithEmulated=True
#Moore().ForceSingleL0Configuration = False

#Moore().HistogrammingLevel= ''

## from LHCbKernel.Configuration import *
## from GaudiConf.Configuration import *
#HistogramPersistencySvc().OutputFile = 'testNewMoni3.root'

#Moore().ThresholdSettings = 'Physics_25Vis_25L0_2Hlt1_AllHlt2_Jun10'

#Moore().ThresholdSettings = 'Physics_HighNu_80Vis_25L0_2Hlt1_CoreHlt2_Jun10'
#Moore().ThresholdSettings = 'Commissioning_PassThrough'
#Moore().ThresholdSettings = 'Physics_HighNu_80Vis_30L0_5Hlt1_CoreHlt2_Jun10'
#Moore().ThresholdSettings = 'Physics_HighNu_40Vis_15L0_2Hlt1_ExpressHlt2_Jun10'
#Moore().ThresholdSettings = 'Physics_HighNu_40Vis_15L0_2Hlt1_noHlt2_Jun10'

#Moore().ThresholdSettings = 'Physics_HighNu_1000Vis_200L0_20Hlt1_noHlt2_Sep10'
#Moore().ThresholdSettings = 'Physics_HighNu_1000Vis_200L0_40Hlt1_CoreHlt2_Sep10'
#Moore().ThresholdSettings = 'Commissioning_Moore_v11_Release_Testing'

#Moore().ThresholdSettings = 'Physics_HighNu_3000Vis_200L0_20Hlt1_CoreHlt2_Sep10'

#Moore().ThresholdSettings = 'Physics_3000Vis_200L0_20Hlt1_yCoreHlt2_Oct10'
#Moore().ThresholdSettings = 'Physics_3000Vis_200L0_20Hlt1_ExpressHlt2_Oct10'
#Moore().ThresholdSettings = 'VanDerMeerScan_Sept10'

#Moore().ThresholdSettings = 'Physics_Hlt1_draft2011'

Moore().ThresholdSettings = 'Physics_September2012'
#Moore().ThresholdSettings = 'Calibration_VeloMicroBias'
#Moore().EnableMonitoring = Tforcerue
                             
#Moore().CheckOdin = False                       
Moore().EvtMax = 300000

## Moore().UseTCK=True
## Moore().InitialTCK = '0x80910000'
## #Moore().InitialTCK = '0x00101710'


Moore().UseDBSnapshot = False
Moore().EnableRunChangeHandler = False
Moore().Verbose= True
#Moore().DDDBtag = "head-20100906"
#Moore().CondDBtag = "head-20100906"
#Moore().Simulation = False
#Moore().DataType   = '2010'
#from Configurables import CondDB
#CondDB().IgnoreHeartBeat = True

#JA ??            
#Moore().ForceSingleL0Configuration = False


#Moore().outputFile = 'test.dst'

#from Configurables import L0Conf
#L0Conf().IgnoreL0MuonCondDB = True
#from L0DU.L0Algs import emulateL0Muon
#l0muon = emulateL0Muon()
#l0muon.IgnoreCondDB = True
#l0muon.FoiXSize =[6,5,0,4,8]
#l0muon.FoiYSize =[0,0,0,1,1]
        

#Moore().DDDBtag = "head-20110622"
#Moore().CondDBtag = "head-20110302"
Moore().Simulation = False
Moore().DataType   = '2012'

#Moore().DDDBtag = "MC09-20100430-vc-100"#090602"#"head-20100906"
#Moore().DDDBtag = "MC09-20090602"
#Moore().CondDBtag = "MC09-20100921-vc-md100"#"head-20100906"
#Moore().Simulation = True
#Moore().DataType   = 'MC09'


#from Configurables import LoKi__HDRFilter   as HDRFilter
#filter = HDRFilter("MyFilter",Code = "HLT_PASS_RE('Hlt1MBMicroBias.*')")
#Moore().WriterRequires = []# filter.getName() ]

#from Configurables       import GaudiSequencer as Sequence
#from Configurables import HltRoutingBitsFilter
#filter = HltRoutingBitsFilter( "MyPhysFilter" , RequireMask = [0x0,0x1<<(46-32),0x0])



#def myf():
    #Sequence('HltEndSequence').Members += [filter]
#    ApplicationMgr().TopAlg += [filter]


#appendPostConfigAction(myf)    

#Moore().WriterRequires = [ filter.getName() ]
#Moore().outputFile = '/castor/cern.ch/user/a/albrecht/data/StripL0xHLT1/'+outDstName
#Moore().EnableLumiEventWriting = False

#Moore().WriterRequires = ['LumiStripperFilter']
#

#Moore().inputFiles = ['castor:/castor/cern.ch/user/d/diegoms/MC2010/Bs2MuMu/GL_above_dot3.dst']



#if SIGNAL:
#    Moore().inputFiles = ['castor:/castor/cern.ch/user/a/albrecht/data/B2mm/MC2010-sim03reco03-GLabove0.3.SeqBmm.dst']
#else: 

#L0 32
Moore().inputFiles = [ 
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/117770/117770_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/117771/117771_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/117772/117772_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/117773/117773_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/117848/117848_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/117849/117849_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118181/118181_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118523/118523_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118524/118524_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118526/118526_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118718/118718_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118719/118719_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118720/118720_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118729/118729_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118730/118730_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118731/118731_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118732/118732_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118733/118733_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118734/118734_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118763/118763_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118764/118764_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118765/118765_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118766/118766_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118767/118767_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118777/118777_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118778/118778_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118779/118779_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118785/118785_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118786/118786_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118792/118792_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118829/118829_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118830/118830_0x0044_NB_L0Phys_00.raw',
'castor://castorlhcb.cern.ch:9002//castor/cern.ch/user/e/evh/118877/118877_0x0044_NB_L0Phys_00.raw'
]



#L0 0x0025
#Moore().inputFiles = ['castor:/castor/cern.ch/user/e/evh/74733_0x0025_MB1.raw']

#L0 0x0026
#Moore().inputFiles = ['castor:/castor/cern.ch/grid/lhcb/data/2010/RAW/FULL/FEST/FEST/78855/078855_0000000001.raw']

#L0 0x1710
#Moore().inputFiles = ['castor:/castor/cern.ch/grid/lhcb/data/2010/DIMUON.DST/00007544/0000/00007544_00001571_1.dimuon.dst']

#L0 0x0029
#Moore().inputFiles = ['castor:/castor/cern.ch/user/e/evh/74465_0x0029_L0Phys.raw']

#MC09 Afs triggger set
#Moore().inputFiles = ['castor:/castor/cern.ch/user/r/rlambert/MC09_data_with_MC09_selection.SeqB0q2DplusMuX.dst']

#very loose Afs early data selected events
#Moore().inputFiles = ['castor:/castor/cern.ch/grid/lhcb/user/r/rlambert/2010_07/9688/9688249/2010_SeqB0q2DplusMuXPresel.dst']


#Moore().inputFiles = ['castor:/castor/cern.ch/grid/lhcb/data/2010/RAW/FULL/FEST/FEST/78854/078854_0000000001.raw']


#Moore().inputFiles = ['castor://castorlhcb.cern.ch:9002//castor/cern.ch/grid/lhcb/MC/2010/DST/00007077/0000/00007077_00000001_1.dst']

#Moore().inputFiles = ['castor:/castor/cern.ch/user/e/evh/74621_0x001f_mbias_bb.raw']
#Moore().inputFiles = ['castor:/castor/cern.ch/user/e/evh/74854_0x001F.raw']
#Moore().inputFiles = ['castor:/castor/cern.ch/user/e/evh/74733_0x001F_MB1.raw']

#Moore().inputFiles = [ 'castor:/castor/cern.ch/grid/lhcb/user/j/jhe/2010_07/9624/9624718/EventsWith_3_PV.mdf']


#Moore().inputFiles = ['castor://castorlhcb.cern.ch:9002//castor/cern.ch/grid/lhcb/data/2010/DST/00006508/0000/00006508_00000527%d_2.MiniBias.dst'%(i) for i in []]

#Moore().inputFiles = ['castor://castorlhcb.cern.ch:9002//castor/cern.ch/grid/lhcb/data/2010/DST/00006394/0000/00006394_0000000%d_2.Dimuon.dst'%(i) for i in [1,2,3,4,5,6,7,8,9]]

#Moore().inputFiles = [ 'castor://castorlhcb.cern.ch:9002//castor/cern.ch/grid/lhcb/data/2010/DST/00006234/0000/00006234_00000%d_1.dst'%(i) for i in [299,300,301,302,303,304,305,306,307,308,309,310]]


#Moore().inputFiles = [ 'castor:/castor/cern.ch/user/n/nmangiaf/RICH/PIDMONITORING/REALDATA/DST/HLT2/IsMuon/Sel.SeqJPsiToMuMu.dst' ]


#Moore().inputFiles = [ 'castor://castorlhcb.cern.ch:9002//castor/cern.ch/grid/lhcb/data/2010/DST/00006312/0000/00006312_0000000%d_4.MiniBias.dst'%(i) for i in [ 1, 2 ]]





Moore().generateConfig = False
#Moore().configLabel = 'Prescale=1'
Moore().configLabel = '2012 Physics, September default, less charm, Hlt2HighPTJets fixed'
#Moore().configLabel = 'Hlt1+Hlt2
#Moore().configLabel = 'Hlt1 only'
#Moore().configLabel = 'PassThrough'
#Moore().configLabel = 'Disable L0-SPD'

#Moore().generateConfig = True
#Moore().configLabel = 'Commissioning Moore v10r0. test of new threshold settings. CAUTION! uses fake L0 001F'
#Moore().configLabel = 'Commissioning Moore v10r0. enabled Hlt1Track, Hlt1SingleHadrons, Hlt1DiHadrons. disabled HLT2'

#"""
#def _forceOutputLevelOfAllConfiguredComponents() :
#    from Gaudi.Configuration import allConfigurables
#    from itertools import ifilter
#    def _setOutputLevel(x) : x.OutputLevel = 1
#    map( _setOutputLevel, ifilter( lambda x : hasattr(x,'OutputLevel') ,  allConfigurables.values() ) )
   

#from Gaudi.Configuration import appendPostConfigAction
#appendPostConfigAction( _forceOutputLevelOfAllConfiguredComponents )
#"""


print Moore()
