#!/usr/bin/env gaudirun.py
#
# Minimal file for running Moore from python prompt
# Syntax is:
#   gaudirun.py ../options/Moore.py
# or just
#   ../options/Moore.py
#
import Gaudi.Configuration
from Configurables import Moore

# if you want to generate a configuration, uncomment the following lines:
#Moore().generateConfig = True
#Moore().configLabel = 'Default'
#Moore().ThresholdSettings = 'Commissioning_PassThrough'
#Moore().configLabel = 'ODINRandom acc=0, TELL1Error acc=1'

Moore().ThresholdSettings = 'Physics_draft2012'

Moore().Verbose = True
Moore().EvtMax = 10000

Moore().UseDBSnapshot = False
Moore().DDDBtag = 'head-20120413'
Moore().CondDBtag = 'head-20120420'
Moore().Simulation = False
Moore().DataType = '2012'
Moore().inputFiles = [ '/afs/cern.ch/lhcb/software/profiling/files/114280_0000000007.raw' ]
#from Configurables import HltRoutingBitsWriter
Moore().ForceSingleL0Configuration = False

from Configurables import EventSelector
EventSelector().PrintFreq = 100

#from Configurables import Hlt__Service
#Hlt__Service().Pedantic = False
