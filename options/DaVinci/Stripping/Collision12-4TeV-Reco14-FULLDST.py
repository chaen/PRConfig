
from Gaudi.Configuration import * 
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles( [
    # LFN:/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00042960_1.full.dst
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00042960_1.full.dst'
    ], clear = True )
