
from Gaudi.Configuration import * 
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles( [
    # LFN:/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00001115_1.full.dst
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00001115_1.full.dst'
    ], clear = True )
