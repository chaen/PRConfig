
from Gaudi.Configuration import * 
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles( [
    # LFN:/lhcb/LHCb/Collision11/FULL.DST/00022719/0000/00022719_00005075_1.full.dst
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/FULL.DST/00022719/0000/00022719_00005075_1.full.dst'
    ], clear = True )
