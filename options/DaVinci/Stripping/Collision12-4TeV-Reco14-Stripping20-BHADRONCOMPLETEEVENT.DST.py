
from Gaudi.Configuration import * 
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles( [
    # LFN:/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00021317/0000/00021317_00003475_1.bhadroncompleteevent.dst
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00021317/0000/00021317_00003475_1.bhadroncompleteevent.dst'
    ], clear = True )
