
from Gaudi.Configuration import * 
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles( [
    # LFN:/lhcb/LHCb/Collision12/BHADRON.MDST/00021211/0000/00021211_00001955_1.bhadron.mdst
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00021211/0000/00021211_00001955_1.bhadron.mdst'
    ], clear = True )
