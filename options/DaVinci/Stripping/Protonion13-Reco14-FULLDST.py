
from Gaudi.Configuration import * 
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles( [
    # LFN:/lhcb/LHCb/Protonion13/FULL.DST/00024845/0000/00024845_00007206_1.full.dst 
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Protonion13/FULL.DST/00024845/0000/00024845_00007206_1.full.dst'
    ], clear = True )
