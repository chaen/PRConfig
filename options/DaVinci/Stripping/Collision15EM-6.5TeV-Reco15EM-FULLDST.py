
from Gaudi.Configuration import * 
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles( [
    # LFN:/lhcb/LHCb/Collision15em/FULL.DST/00046147/0000/00046147_00004357_1.full.dst
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15em/FULL.DST/00046147/0000/00046147_00000533_1.full.dst'
    ], clear = True )
