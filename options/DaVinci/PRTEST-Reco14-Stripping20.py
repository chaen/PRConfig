from Gaudi.Configuration import *

# Run stripping 20
importOptions( "$APPCONFIGROOT/options/DaVinci/DV-Stripping20-Stripping.py" )

# 2012 Reco14 test FULL DSTs
importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco14_Run125113.py")

# Number events etc.
from Configurables import DaVinci
DaVinci().EvtMax    = 100000
DaVinci().EvtMax    = 10
DaVinci().PrintFreq = 1
DaVinci().DataType = "2012"

# Export the histograms
from Configurables import TimingAuditor, SequencerTimerTool
p = TimingAuditor()
p.addTool(SequencerTimerTool, name = "TIMER")
p.TIMER.NameSize=100


