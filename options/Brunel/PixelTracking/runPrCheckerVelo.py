from Gaudi.Configuration import *
from Configurables import CondDB,LHCbApp,Brunel,LHCbConfigurableUser, LHCbApp, L0Conf,TrackSys
from Brunel.Configuration import Brunel
from GaudiConf import IOHelper
from Gaudi.Configuration import *
CondDB.Upgrade = True
from Configurables import DigiConf
LHCbApp().DDDBtag = "dddb-20171010"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"
Brunel().EvtMax = 100

# the path to the local input file on lbhltperf01
# to be changed by entry from EOS
IOHelper('ROOT').inputFiles(['/scratch/lhcbpr2/BBARTESTSAMPLE_1.digi'])

Brunel().InputType = 'DIGI'
Brunel().DataType = 'Upgrade'
Brunel().PrintFreq = 50
Brunel().WithMC = True
Brunel().Simulation = True
Brunel().OutputType = 'None'
Brunel().MainSequence = ['ProcessPhase/Reco',  'ProcessPhase/MCLinks', 'ProcessPhase/Check']
Brunel().RecoSequence = [ 'Decoding','TrFast']
Brunel().Detectors = ['VP','FT','UT']
Brunel().InputType = 'DIGI'
DigiConf().EnableUnpack = True
TrackSys().TrackingSequence = ['Decoding','TrFast']
TrackSys().TrackTypes       = ['Velo']
TrackSys().TracksToConvert  = ['Velo']
TrackSys().VeloUpgradeOnly = True
L0Conf.EnsureKnownTKC = False
def removeAlgorithmsFromSequence():
    try :
        GaudiSequencer('CheckPatSeq').Members.remove( GaudiSequencer('TrackResCheckerFast'))
        #empty the calo sequence
        GaudiSequencer('MCLinksCaloSeq').Members = []
        GaudiSequencer('MCLinksCaloSeq').Members = []
        GaudiSequencer('RecoTrFastSeq').Members.remove( GaudiSequencer('PatPV3D'))
        GaudiSequencer('RecoDecodingSeq').Members.remove( GaudiSequencer('createUTClusters'))
        GaudiSequencer('RecoDecodingSeq').Members.remove( GaudiSequencer('PrGECFilter'))
        GaudiSequencer('CheckPatSeq').Members.remove( GaudiSequencer('TrackResCheckerFast'))
        GaudiSequencer('CheckPatSeq').Members.remove( GaudiSequencer('TrackResCheckerFast'))
        GaudiSequencer('CheckPatSeq').Members.remove( GaudiSequencer('TrackResCheckerFast'))
    except ValueError:
        None
appendPostConfigAction(removeAlgorithmsFromSequence)
