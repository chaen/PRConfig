import subprocess
import os
import sys
from LbNightlyTools import Configuration

def get_project_version(config, project):
    ''' Look up the project version '''
    version = None
    for proj in config[u'projects']:
        if proj['name'].lower() == project.lower():
            version = proj['version']
            break
    return version

def main():
    config = Configuration.load(os.getcwd()+"/../build/slot-config.json")
    version = get_project_version(config, "Brunel")
    return_code = subprocess.call(["lb-run",
                                   "--user-area="+os.getcwd()+"/../build",
                                   "Brunel/"+version,
                                   "python",
                                   "$PRCONFIGOPTS/Brunel/PixelTracking/runPixelTrackingOnlyThroughput.py",
                                   "-p1:1,1:16,1:32,1:48,1:64,2:1,2:8,2:16,2:24,2:32,4:1,4:4,4:8,4:12,4:16,8:1,8:2,8:4,8:6,8:8,12:1,12:2,12:4,12:5,16:1,16:2,16:3,16:4,20:2",
                                   "-n20000",
                                   "-m500",
                                   "-path",
                                   "./output",
                                   "/scratch/lhcbpr2/MINBIASTESTSAMPLE_big_GEC.mdf"])
    return return_code

if __name__ == '__main__':
    sys.exit(main())
