#!/usr/bin/env python
"""Runs MiniBrunel with a set of parameters,
   Example of running:
python runTest.py -p4:12 -n5000 -m500 -path /localdisk1/hlt1/rquaglia/NewDecodingOnly /localdisk1/hlt1/rquaglia/MC_Upg_Down_201710_43k.mdf

# Nice list of points for getting quickly something visible and nice plots in the long term
# -p1:1,1:16,1:32,1:48,1:64,2:1,2:8,2:16,2:24,2:32,4:1,4:4,4:8,4:12,4:16,8:1,8:2,8:4,8:6,8:8,12:1,12:2,12:4,12:5,16:1,16:2,16:3,16:4
-n specifies the number of events/job
-path specifies the path where timeline files (csv), log files and xml files are store
By default there is no GEC in the sequence and the EventSlots for MiniBrunel is 2*ThreadPoolSize
"""


__author__ = "Sebastien Ponce"

import re
import os
import sys
import argparse
import subprocess
import tempfile
import itertools
from LbNightlyTools import Configuration
from runThroughputTest import get_project_version

def prepareInput(inputFileName, outputFileName, threads, events, useHive):
    '''create new input file from a base and options'''
    # write content to temp file
    fd, configFileName = tempfile.mkstemp(suffix=".py")
    outputFile = open(configFileName, 'w')
    outputFile.write('from Configurables import MiniBrunel\n')
    outputFile.write('mbrunel = MiniBrunel()\n')
    outputFile.write('mbrunel.EvtMax = %d\n' % events)
    if useHive:
        outputFile.write('mbrunel.ThreadPoolSize = %d\n' % threads)
        outputFile.write('mbrunel.EventSlots = %d\n' % (2*threads))
    outputFile.write('mbrunel.GECCut = -1\n')
    outputFile.write('mbrunel.IPCut = -1\n')
    outputFile.write('mbrunel.EnableHive = %s\n' % useHive)
    outputFile.write('mbrunel.HLT1Only = True\n')
    outputFile.write('mbrunel.HLT1Fitter = True\n')
    outputFile.write('mbrunel.InputData = ["%s"]*%d\n' % (inputFileName, threads))
    outputFile.write('mbrunel.TimelineFile = "%s.csv"\n' % outputFileName)
    outputFile.write('mbrunel.RunFastForwardFitter = False\n')
    outputFile.write('mbrunel.EnableHLTEventLoopMgr = True\n')
    outputFile.write('mbrunel.VeloOnly     = True\n')
    outputFile.write('mbrunel.DecodingOnly = False\n')
    outputFile.write('mbrunel.IgnoreChecksum = True\n')
    outputFile.close()
    # return temp file name
    return configFileName

def runTest(inputFileName, baseOutputFileName, threads, firstevent, events, nbjobs, useHive):
    '''Run a test with the given options'''

    config = Configuration.load(os.getcwd()+"/../build/slot-config.json")
    version = get_project_version(config, "Brunel")

    # start the different processes in parallel
    processes = []
    for jobNb in range(nbjobs):
        # create inputFile
        outputFileName = baseOutputFileName % jobNb
        configFileName = prepareInput(inputFileName, outputFileName, threads, threads*events, useHive)
        # open log file
        outputFile = open("%s.log" % outputFileName, 'w')
        # run the test
        processes.append((subprocess.Popen(["lb-run",
                                            "--user-area="+os.getcwd()+"/../build",
                                            "Brunel/"+version,
                                            "$PRCONFIGOPTS/Brunel/PixelTracking/lbsmaps",
                                            "-o",
                                            "%s.xml" % outputFileName,
                                            "gaudirun.py",
                                            configFileName],
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE,
                                       ),
                          outputFile,
                          configFileName))

    return_code=0
    # wait for all processes and massage output
    for (process, outputFile, configFileName) in processes:
        (stdout_data, stderr_data) = process.communicate()
        print(stdout_data)
        print(stderr_data)
        outputFile.write(stdout_data)
        outputFile.write(stderr_data)
        outputFile.close()
        # cleanup inputFile
        os.remove(configFileName)
        if process.returncode != 0:
            return_code = process.returncode
        if "Application Manager Terminated successfully" not in stdout_data[-100:]:
            return_code = 1
    return return_code

def main():

    '''Main method : parses options and calls runTest'''
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-t', '--threads', type=str, default='1',
                        help='list of nb of threads per job to run the test for, comma separated')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-j', '--jobs', type=str, default='1',
                        help='list of nb of jobs to run the test for, comma separated')
    group.add_argument('-p', '--points', type=str,
                        help='explicit list of points (ie nb threads:nbjobs) to run the test for, comma separated')
    parser.add_argument('-m', '--maxthreads', type=int, default='500',
                        help='maximum number of threads allowed. This allows to ignore some combinations of -t and -j that would lead to too many concurrent threads.')
    parser.add_argument('-n', '--events', default=1000, type=int,
                        help='nb of events to process per thread')
    parser.add_argument('-e', '--firstevent', default=0, type=int,
                        help='nb of first event, default is 0')
    parser.add_argument('-r', '--runs', default=1, type=int,
                        help='nb of runs of the test, default is 1')
    parser.add_argument('-f', '--firstrun', default=0, type=int,
                        help='nb of first run, default is 0')
    parser.add_argument('-o', '--onlyhive', action='store_true',
                        help='whether to only run in hive mode, not running non hive cases')
    parser.add_argument('inputFileName', type=str,
                        help='Input file name. Needs to have enough events for feeding 1 thread. Then it will be reused')

    parser.add_argument('-path', '--outpath', type=str,  default = "./test",
                        help = 'Output path where to store results')
    args = parser.parse_args()
    pathname = args.outpath
    print("Will store throughput test results at "+pathname)
    if os.path.exists( pathname):
        print "path exists, test already done, NO OUTPUT PRODUCED"
        return
    else:
        os.mkdir( pathname)
    # check options
    if args.points:
        points = [map(int, item.split(':')) for item in args.points.split(',')]
    else:
        nbthreads = map(int,args.threads.split(','))
        nbjobs = map(int,args.jobs.split(','))
        points = itertools.product(nbthreads, nbjobs)
    return_code=1
    print list(points)
    print "looping point"
    for nthreads, njobs in points:
        print "looping for nthreads = "+str(nthreads)+" njobs ="+str(njobs)
        # check total number of threads
        if nthreads * njobs > args.maxthreads:
            print "continue"
            continue
        # run the test the specified number of times

        for i in range(args.runs):
            outputFilebaseName = '%s/MiniBrunel-HTL1.%s.%dt.%dj.hive.%de.%d.%%d' % (pathname, os.environ['CMTCONFIG'], nthreads, njobs, args.events, args.firstrun+i)
            print "run test"
            return_code = runTest(args.inputFileName, outputFilebaseName, nthreads, args.firstevent, args.events, njobs, True)
        # in case of single thread, maybe also run non hive mode
        if nthreads == 1 and not args.onlyhive:
            for i in range(args.runs):
                outputFilebaseName = '%s/MiniBrunel-HTL1.%s.1t.%dj.nohive.%de.%d.%%d' % (pathname, os.environ['CMTCONFIG'], njobs, args.events, args.firstrun+i)
                return_code = runTest(args.inputFileName, outputFilebaseName, nthreads, args.firstevent, args.events, njobs, False)
    return return_code
if __name__ == '__main__':
    sys.exit(main())
