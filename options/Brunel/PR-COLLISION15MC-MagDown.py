# Example 2015 MC collisions options for Brunel

# Syntax is:
#
from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import Brunel

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml" ]

#-- Use latest 2015 database tags for data
Brunel().DataType = "2015"
Brunel().Simulation=True
Brunel().DDDBtag = "dddb-20140729"
Brunel().CondDBtag = "sim-20140730-vc-md100"


EventSelector().Input = [
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_000.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_001.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_003.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_004.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_005.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_006.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_007.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_008.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_009.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_010.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_011.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_012.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_013.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_014.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_015.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_016.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_017.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_018.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_019.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_020.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_021.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_020.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_021.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_020.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_021.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_022.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_023.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_024.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_025.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_026.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_027.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_028.mdf' SVC='LHCb::MDFSelector'",
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/mdf/SPD_lt_420/MagDown/MC2015_MinBias_0xFF65_029.mdf' SVC='LHCb::MDFSelector'",
  ]
