# Config for performance testing 
from Gaudi.Configuration import *

importOptions("$BRUNELROOT/options/Brunel-Default.py")
importOptions("$PRCONFIGOPTS/Brunel/PR-COLLISION12-Beam4000GeV-VeloClosed-MagDown.py")

from Configurables import Brunel
Brunel().EvtMax=1000

from Configurables import TimingAuditor, MemoryAuditor, SequencerTimerTool
AuditorSvc().Auditors += [ 'MemoryAuditor' ]
TimingAuditor('TIMER').addTool(SequencerTimerTool, name="TIMER")
TimingAuditor('TIMER').TIMER.NameSize=100

