from Configurables import MiniBrunel

mbrunel = MiniBrunel()
mbrunel.EvtMax = 1000
mbrunel.ThreadPoolSize = 1
mbrunel.EventSlots = 1
mbrunel.HLT1Only = True
mbrunel.RunFastForwardFitter = False
