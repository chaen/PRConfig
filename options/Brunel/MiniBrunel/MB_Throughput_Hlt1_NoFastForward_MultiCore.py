from Configurables import MiniBrunel

mbrunel = MiniBrunel()
mbrunel.EvtMax = 1000
mbrunel.ThreadPoolSize = 10
mbrunel.EventSlots = 10
mbrunel.HLT1Only = True
mbrunel.RunFastForwardFitter = False
