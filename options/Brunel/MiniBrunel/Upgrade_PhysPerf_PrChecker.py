from Gaudi.Configuration import *
from Configurables import Brunel, GaudiSequencer
from GaudiConf import IOHelper

brunel = Brunel()
brunel.DataType = "Upgrade"
brunel.EvtMax = 1000
brunel.WithMC = True
brunel.OutputType = "NONE"
brunel.Simulation = True
brunel.InputType = 'DIGI'
brunel.DDDBtag    = "dddb-20170301"
brunel.CondDBtag  = "sim-20170301-vc-md100"
brunel.DisableTiming  = True
brunel.MainSequence = ['ProcessPhase/Reco', 'ProcessPhase/MCLinks', 'ProcessPhase/Check']

def ConfGaudiSeq():
    GaudiSequencer("RecoRICHFUTURESeq").Members = []
    GaudiSequencer("RecoCALOSeq").Members = [ ]
    GaudiSequencer("RecoMUONSeq").Members = [ ]
    GaudiSequencer("RecoPROTOSeq").Members = [ ]
    GaudiSequencer("RecoSUMMARYSeq").Members = [ ]
    GaudiSequencer("CheckRICHFUTURESeq").Members = [ ]
    GaudiSequencer("CheckMUONSeq").Members = [ ]

appendPostConfigAction(ConfGaudiSeq)

files = [ 'root://eoslhcb.cern.ch//eos/lhcb/user/c/chasse/data/upgrade_mc_FT61_1000evts_minbias.xdigi']

IOHelper("ROOT").inputFiles(files)

