#!/usr/bin/env python

# 2015 Brunel options for Early Measurements:
# ==================================
from Configurables import Brunel
Brunel().DataType = "2015"
Brunel().EvtMax   = 2000

from Configurables import LHCbApp
LHCbApp().CondDBtag="cond-20150602"
LHCbApp().DDDBtag="dddb-20150526"

from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$APPCONFIGOPTS/Brunel/saveFittedVeloTracks.py")
importOptions("$PRCONFIGOPTS/Brunel/jemalloc/full.py")

def postConfig():
  from Configurables import JemallocProfile
  jp = JemallocProfile()
  jp.StartFromEventN = 499
  jp.StopAtEventN = 1999
  jp.DumpPeriod = 100

  GaudiSequencer("PhysicsSeq").Members += [ jp ]

from Gaudi.Configuration import *
appendPostConfigAction(postConfig)


