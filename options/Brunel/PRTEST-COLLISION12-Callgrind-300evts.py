# Config for performance testing 
from Gaudi.Configuration import *
from Configurables import CallgrindProfile

importOptions("$BRUNELROOT/options/Brunel-Default.py")
importOptions("$PRCONFIGOPTS/Brunel/PR-COLLISION12-Beam4000GeV-VeloClosed-MagDown.py")

def addProfile():
    p = CallgrindProfile()
    p.StartFromEventN = 100 
    p.StopAtEventN = 300
    p.DumpAtEventN= 300
    p.DumpName = "PRTEST"
    GaudiSequencer("InitBrunelSeq").Members.insert(0,p )

from Configurables import Brunel
Brunel().EvtMax=301

# Now add the profiling algorithm to the sequence
appendPostConfigAction(addProfile)
