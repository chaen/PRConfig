#
# PrPixel test added at the request of Tim Head.
# See LBCORE-585
#
from Brunel.Configuration import importOptions, appendPostConfigAction

from Configurables import PrChecker, PrLHCbID2MCParticle, PrTrackAssociator
from Configurables import CondDB, CondDBAccessSvc, LHCbApp, Brunel
from Configurables import HistogramPersistencySvc


def doMyChanges():
    GaudiSequencer("OutputDSTSeq").Enable = False
    OutputStream("DstWriter").Enable = False
    
    prchecker = PrChecker()
    prchecker.WriteVeloHistos = 2
    prchecker.WriteForwardHistos = 2
    prchecker.WriteMatchHistos = 2
    prchecker.WriteDownHistos = 2
    prchecker.WriteUpHistos = 2
    prchecker.WriteTTrackHistos = 2
    prchecker.WriteBestHistos = 2
    prchecker.Eta25Cut = True
    
    # GaudiSequencer("MCLinksTrSeq").Members = [PrLHCbID2MCParticle(), PrTrackAssociator()]
    GaudiSequencer("CheckPatSeq").Members += [prchecker]

def removeStuff():
    sequencers = ["CaloBanksHandler", "RecoVertexSeq", "RecoRICHSeq",
                  "RecoCALOSeq", "RecoMUONSeq", "RecoPROTOSeq",
                  "MCLinksCaloSeq", "CheckRICHSeq", "TrBestSeq",
                  "RecoSUMMARYSeq"]
    for name in sequencers:
        GaudiSequencer(name).Members = []

def execute(localdb):
    importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")
    importOptions("$APPCONFIGOPTS/Brunel/Brunel-Upgrade-Reference.py")
    importOptions("$APPCONFIGOPTS/Brunel/Upgrade-RichPmt.py")
    importOptions("$APPCONFIGOPTS/Brunel/patchUpgrade1.py")
    importOptions("$APPCONFIGOPTS/Brunel/xdst.py")

    Brunel().DataType = "Upgrade"
    Brunel().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Magnet']
    
    HistogramPersistencySvc().OutputFile = 'BrunelHistos-tracking-%s-45deg.root'%(localdb)
    
    t = {"DDDB":"dddb-20140606",
         "CondDB":"sim-20140204-vc-md100",
         }
    LHCbApp().DDDBtag = t['DDDB']
    LHCbApp().CondDBtag = t['CondDB']

    LHCbApp().Simulation = True
    CondDB().Upgrade = True

    import os
    privatedbPath = os.path.join("${PRCONFIGOPTS}", "Brunel", "upgrade", "myDDDB-LHCb-Upgrade-VP-Jun2014.%s" %localdb)
    privatedb = "sqlite_file:"+privatedbPath+".db/DDDB"
    
    myAccessSvc = CondDBAccessSvc("mySuperHyperAccessSvc",
                                  ConnectionString=privatedb,
                                  CacheHighLevel=2000)
    CondDB().addLayer(accessSvc=myAccessSvc)

    appendPostConfigAction(doMyChanges)
    appendPostConfigAction(removeStuff)

job_type = "rotnm_foil_gbt"

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from PRConfig import TestFileDB

test = TestFileDB.test_file_db["upgrade-rotnm_foil_gbt-sim"]

IOHelper('ROOT').inputFiles(test.filenames)
LHCbApp.EvtMax = 1000
execute(job_type)
