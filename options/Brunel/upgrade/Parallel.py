import os, sys, re, atexit
import socket
import zmq
from time import sleep
from itertools import product
from collections import defaultdict
from multiprocessing import Process, Value, Array, Lock
import time

_tmpdir = None

def __remove_tmpdir():
    if not _tmpdir:
        return
    import shutil
    if os.path.exists(_tmpdir) and os.path.isdir(_tmpdir):
        shutil.rmtree(_tmpdir)

atexit.register(__remove_tmpdir)

def get_tmpdir():
    global _tmpdir
    if not _tmpdir:
        import tempfile
        _tmpdir = tempfile.mkdtemp(prefix = "copy_")
    return _tmpdir

class MemCalc(object):
    def __init__(self):
        self.jobpids = Array('i',100) 
        self.lock = Lock()

    def update(self, jobnb, jobpid):
      self.lock.acquire()
      try:
        self.jobpids[jobnb] = jobpid
      finally:
        self.lock.release()

    def getNThreads(self):
      self.lock.acquire()
      nthreads = 0
      try:
        for pid in self.jobpids:
          if(pid > 0):
            nthreads += 1
      finally:
        self.lock.release()
      return nthreads

    def getMem(self):
      self.lock.acquire()
      total_mem = 0
      try:
        for pid in self.jobpids:
          if(pid > 0):
            import os.path
            if(os.path.exists("/proc/"+str(pid))):
              from Memory import pidtotals, procdata
              meminfo_src = procdata()
              mmi = pidtotals(meminfo_src, pid)
              total_mem += mmi['pss']
      finally:
        self.lock.release()
      return total_mem

_memCalc = MemCalc()
def get_MemCalc():
    global _memCalc
    return _memCalc


#Counts number of i.e. processed events
class Counter(object):
    def __init__(self, nEvents = 500):
        self.nEvents = nEvents
        self.val      = Value('i', 0)
        self.lasttime = Value('d',time.time())
        self.statsArray = Array('d',1000) #TODO max 1000 counter entries
        self.memstatsArray = Array('d',1000) #TODO max 1000 counter entries
        self.threadstatsArray = Array('i',1000) #TODO max 1000 counter entries
        self.statsIndex = Value('i',0)
        self.lock = Lock()

    def increment(self, n=1):
      self.lock.acquire()
      try:
        self.val.value += n
        if(self.val.value > self.nEvents -1):
          thistime = time.time()
          dt = thistime-self.lasttime.value
          mem = get_MemCalc().getMem()/1024
          nthreads = get_MemCalc().getNThreads()
          print 'Throughput:',self.val.value/dt,'Events/s (',self.val.value,'Events in',dt,'s),  Memory usage:',mem,'Mb'
          self.statsArray[self.statsIndex.value] = dt
          self.memstatsArray[self.statsIndex.value] = mem
          self.threadstatsArray[self.statsIndex.value] = nthreads
          if(self.statsIndex.value < 1000):
            self.statsIndex.value += 1
          self.val.value = 0
          self.lasttime.value = thistime
      finally:
          self.lock.release()

    def printStats(self):
        for i in range(self.statsIndex.value):
          print("Throughput: %8.2f Events/s (%7d Events in %8.2f s) Memory: %8.2f Mb" % (self.nEvents/self.statsArray[i],self.nEvents,self.statsArray[i],self.memstatsArray[i] ))
        
        file = open("teststats.json","w")
        file.write("{\"stats\": [\n")
        for i in range(self.statsIndex.value):
          file.write("  {\n")
          file.write("    \"Events\": "+ str(self.nEvents)+"\n")
          file.write("    \"time s\": "+ str(self.statsArray[i])+"\n")
          file.write("    \"Throughput evts/s\": "+ str(self.nEvents/self.statsArray[i])+"\n")
          file.write("    \"Memory Mb\": "+ str(self.memstatsArray[i])+"\n")
          file.write("    \"Threads\": "+ str(self.threadstatsArray[i])+"\n")
          file.write("  }\n")
        file.write("] }\n")
        file.close()

    @property
    def value(self):
        return self.val.value

_counter = Counter()
def get_Counter():
    global _counter
    return _counter 

class Runner(object):
    ## Class that manages running multiple processes in parallel. A maximum
    ## number can be launched and new ones will be started as available and
    ## needed.

    ## Worker processes are assumed to send a message when they are done or on
    ## error:
    ## context = zmq.Context()
    ## pub = context.socket(zmq.PUB)
    ## pub.connect("ipc://%s" % os.path.join(get_tmpdir(), "status"))
    ## pub.send_pyobj(((self.index(), "status"), "done"))

    context = zmq.Context()

    def __init__(self, processes, handlers = {}, max_run = 10):
        self.__max_run = max_run
        self.__processes = processes
        self.__handlers = handlers

        self.__poller = zmq.Poller()
        for socket, hs in handlers.iteritems():
            for t in hs.iterkeys():
                self.__poller.register(socket, t)

    def tmpdir(self):
        return get_tmpdir()

    def stop(self, message):
        return False

    def printStats(self):
        get_Counter().printStats()

    def run_processes(self):


        # Listen to status publication of workers
        sub = Runner.context.socket(zmq.SUB)
        sub.bind("ipc://%s" % os.path.join(self.tmpdir(), "status"))
        sub.setsockopt(zmq.SUBSCRIBE, "")

        # The poller to allow listening to additional sockets too
        self.__poller.register(sub, zmq.POLLIN)

        # Keep track of who's running and who's done
        running = {i : False for i in range(len(self.__processes))}
        done = {i : False for i in range(len(self.__processes))}

        # How many do we run
        n_run = self.__max_run if len(self.__processes) > self.__max_run else len(self.__processes)

        # If we should already stop, don't even do anything
        if self.stop(""):
            return

        # Start the initial processes
        for i in range(n_run):
            running[i] = True
            self.__processes[i].start()

        stop = False
        # Main loop
        # Keep running while processes are still left
        # If we should stop, at least no running processes should be around
        while not all(done.values()) and not (stop and all([not r for r in running.itervalues()])):

            socks = dict(self.__poller.poll())
            if sub in socks and socks[sub] == zmq.POLLIN:
                # handle statuses
                (index, msg_type), message = sub.recv_pyobj()
                if msg_type == 'status' and message in ('done', 'error'):
                    done[index] = True
                    running[index] = False
                    self.__processes[index].join()
                else:
                    if self.stop(message):
                        stop = True

            # Dispatch rest of messages to registered handlers
            for socket, handlers in self.__handlers.iteritems():
                if socket in socks and socks[socket] in handlers:
                    handlers[socks[socket]]()

            # Start extra processes if needed
            for m in self.__processes:
                i = m.index()
                if done[i] or running[i]:
                    continue
                if not stop and sum(running.itervalues()) < self.__max_run:
                    m.start()
                    running[i] = True

class Worker(object):

    def __init__(self, index, args = []):
        self.__index = index
        if args:
            self.__process = Process(target = self.run_process, args = args)
        else:
            self.__process = Process(target = self.run_process)
        self.__context = None

    def context(self):
        return self.__context

    def index(self):
        return self.__index

    def tmpdir(self):
        return get_tmpdir()

    def run_process(self, *args):
        self.__context = zmq.Context()
        # Status publishing socket
        self.__pub = self.context().socket(zmq.PUB)
        self.__pub.connect("ipc://%s" % os.path.join(self.tmpdir(), "status"))
        self.run(*args)

    def run(self):
        self.done()

    def publish(self, msg_type, msg):
        self.__pub.send_pyobj(((self.index(), msg_type), msg))

    def done(self):
        get_MemCalc().update(self.__index,-1)
        sleep(0.5)
        self.publish('status', 'done')
        sleep(0.5)

    def error(self):
        get_MemCalc().update(self.__index,-1)
        sleep(0.5)
        self.publish('status', 'error')
        sleep(0.5)

    def process(self):
        return self.__process

    def start( self ):
        self.__process.start()
        #print "job nb:",self.__index,"MY pid is",self.__process.pid
        get_MemCalc().update(self.__index,self.__process.pid)

    def join( self ):
        self.__process.join()

    def counter(self):
        return get_Counter()



