import os, sys, re
from collections import defaultdict
from Parallel import Worker, Runner
from Gaudi.Configuration import importOptions


class JobRunner(Worker):
  def __init__(self, n, input_files, output_file):
    super(JobRunner, self).__init__(n)
    self.__input_files = input_files
    self.__output_file = output_file
    self.__written = 0

  def run(self):
    # General configuration
    from Gaudi.Configuration import ApplicationMgr
    import GaudiPython as GP
    from Configurables import Brunel

    br = Brunel()
    br.EvtMax      = 1337
    br.PrintFreq   = 1000
    br.SkipEvents  = 0
    br.WriteFSR = False
    br.WriteLumi = False
    br.OutputType  = "NONE"  #to disable dst output
    br.DatasetName = "histos_" + str(super(JobRunner, self).index())
    from Configurables import DigiConf
    DigiConf().EnableUnpack = True
    br.WithMC     = False
    br.Simulation = True

    #Detectors
    br.DataType     = "Upgrade"

    #only Fast
    br.RecoSequence = ["Decoding","TrFast" ]
    #DDDB tags
    from Configurables import CondDB, LHCbApp, DDDBConf
    CondDB().Upgrade = True
    CondDB().LoadCALIBDB = 'HLT1'
    LHCbApp().DDDBtag = "dddb-20160304"
    LHCbApp().CondDBtag = "sim-20150716-vc-md100"

    #Disable Moni
    from Configurables import RecMoniConf
    RecMoniConf().MoniSequence = []
    #Patch Trigger
    from Configurables import L0Conf
    L0Conf().EnsureKnownTCK=False

    #Configure input files
    br.InputType  = "DIGI"
    from GaudiConf import IOHelper
    IOHelper('ROOT').inputFiles( self.__input_files, clear=True)

    #run parallel
    gaudi = GP.AppMgr()
    gaudi.initialize()
    iev = 0
    while(iev < br.EvtMax):
      if(not gaudi.run(1)): 
        break
      iev += 1
      self.counter().increment(1)

    gaudi.stop()
    gaudi.finalize()

    self.done()

files =['root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_0_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_1_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_2_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_3_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_4_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_5_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_6_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_7_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_8_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_9_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_10_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_11_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_12_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_13_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_14_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_15_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
#'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_16_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi', #file not available
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_17_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_18_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_19_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_20_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi',
'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/4316_21_UFT5x_MagDown_13104012_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-Extended.digi'
]


##single thread
#jobs = [JobRunner(0, files, 'outputfile')]

##multi thread
nCores = 24
#Process input files multiple times if not enough
while(nCores > len(files)):
  files = files + files
#Fill joblist
jobs = []
for idx, filename in enumerate(files):
   if( idx+1 > nCores): 
     break
   jobs += [JobRunner(idx, [filename], 'outputfile')]

runner = Runner(jobs, max_run=nCores)
runner.run_processes()

print 'Throughput test statistics'
print 'used cores:',nCores
runner.printStats()
