# Config for performance testing 
from Gaudi.Configuration import *
from Configurables import Brunel
from PRConfig import TestFileDB

TestFileDB.test_file_db['2015_raw_full'].run(configurable=Brunel())
Brunel().EvtMax = 1000
Brunel().DatasetName="2015magdown"
Brunel().Monitors=["SC","FPE"]

from Configurables import EventClockSvc
EventClockSvc( InitialTime =1445527400000000000 )

