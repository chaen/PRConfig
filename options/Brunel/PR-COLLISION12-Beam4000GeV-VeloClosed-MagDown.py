# Example 2012 collisions options for Brunel

# Syntax is:
#   gaudirun.py COLLISION12-Beam4000GeV-VeloClosed-MagDown.py
#
from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import Brunel

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml" ]

#-- Use latest 2012 database tags for real data
Brunel().DataType = "2012"

EventSelector().Input = [
  "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/data/2012/RAW/FULL/LHCb/COLLISION12/112181/112181_0000000182.raw' SVC='LHCb::MDFSelector'"
    ]
