from string import maketrans

from Brunel.Configuration import *
from Configurables import PrChecker, TrackEffChecker
from Configurables import PrLHCbID2MCParticle, PrTrackAssociator


def track_checker():
    GaudiSequencer("MCLinksTrSeq").Members = [PrLHCbID2MCParticle(), PrTrackAssociator()]
    GaudiSequencer("CheckPatSeq").Members = [PrChecker()]

def quick():
    GaudiSequencer("RecoCALOSeq").Members = []
    GaudiSequencer("RecoMUONSeq").Members = []
    GaudiSequencer("RecoPROTOSeq").Members = []
    GaudiSequencer("RecoRICHSeq").Members = []
    #GaudiSequencer("RecoVertexSeq").Members = []
    #GaudiSequencer("TrForwardSeq").Members = []
    #GaudiSequencer("TrSeedingSeq").Members = []
    #GaudiSequencer("TrUpSeq").Members = []
    #GaudiSequencer("TrDownSeq").Members = []
    #GaudiSequencer("TrMatchSeq").Members = []
    #GaudiSequencer("TrBestSeq").Members = []
    #GaudiSequencer("TrFitSeq").Members = []
    #
    GaudiSequencer("MCLinksUnpackSeq").Members = []
    GaudiSequencer("CaloBanksHandler").Members = []
    GaudiSequencer("MCLinksTrSeq").Members = []
    GaudiSequencer("CheckPatSeq").Members = []
    #
    GaudiSequencer("MCLinksCaloSeq").Members = []
    GaudiSequencer("TrUpSeq").Members = []
    GaudiSequencer("TrMatchSeq").Members = []
    GaudiSequencer("TrDownSeq").Members = []
    GaudiSequencer("RecoVertexSeq").Members = []
    GaudiSequencer("TrForwardSeq").Members = []
    GaudiSequencer("TrSeedingSeq").Members = []
    GaudiSequencer("TrBestSeq").Members = []
    GaudiSequencer("TrFitSeq").Members = []
    GaudiSequencer("CheckMUONSeq").Members = []
    GaudiSequencer("CheckRICHSeq").Members = []
    GaudiSequencer("OutputDSTSeq").Members = []
    GaudiSequencer("RecoSUMMARYSeq").Members = []

def execute(localdb="", blah=""):
  from Configurables import Brunel, LHCbApp
  #from GaudiConf import IOHelper

  importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")
  importOptions("$APPCONFIGOPTS/Brunel/Upgrade-RichPmt.py")
  importOptions("$APPCONFIGOPTS/Brunel/patchUpgrade1.py")
  importOptions("$APPCONFIGOPTS/Brunel/xdst.py")

  # set input
  #Brunel().InputType = 'DST'
  #input_path = '/opt/dirac/lhcbpr/files'
  #input_files = glob(path.join(input_path, '*.xdst'))
  #IOHelper().inputFiles(input_files)

  LHCbApp().Simulation = True
  LHCbApp().DDDBtag = "dddb-20131025"
  LHCbApp().CondDBtag = "sim-20130830-vc-md100"
  CondDB().AllLocalTagsByDataType=["VP_UVP+RICH_2019+UT"]
  CondDB().Upgrade = True
  
  Brunel().DataType = "Upgrade"  
  Brunel().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr' ]
  
  Brunel().InputType = "DIGI"
  Brunel().OutputType = "NONE"
  Brunel().WithMC    = True

  #EventSelector().Input = ["/castor/cern.ch/user/t/thead/ganga/VPdigital/325/%s/OctTest-Extended.digi"%n for n in xrange(90)]

  loss = 0.00
  
  if loss == 0.00:
      EventSelector().Input = ["DATAFILE='PFN:root://castorlhcb.cern.ch//castor/cern.ch/user/t/thead/ganga/VPdigital/349/%s/OctTest-Extended.digi"%n for n in xrange(20)]
  
  #importOptions("VP-Kst-nu7.6.py")
  outpath = "OctTest-vp-long-tracks-IPreso-monday"
  outpath = "OctTest-vp-losses-%.2f"%loss
  #outpath = "valgrind"
  #outpath = "pictures"
  
  LHCbApp().EvtMax = 1000
  Brunel().PrintFreq = 100
  
  from Configurables import PrChecker
  from Configurables import ProcessPhase, GaudiSequencer, NTupleSvc, OutputStream
  
  prchecker = PrChecker()
  prchecker.WriteVeloHistos = 2
  prchecker.WriteForwardHistos = 2
  prchecker.WriteMatchHistos = 2
  prchecker.WriteDownHistos = 2
  prchecker.WriteUpHistos = 2
  prchecker.WriteTTrackHistos = 2
  prchecker.WriteBestHistos = 2

  
  appendPostConfigAction(quick)
  appendPostConfigAction(track_checker)
    
  HistogramPersistencySvc().OutputFile = outpath+'-BrunelHistos.root'
  Brunel().DatasetName = outpath
  
execute()
