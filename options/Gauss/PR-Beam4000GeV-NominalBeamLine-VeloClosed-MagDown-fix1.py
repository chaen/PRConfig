# Reference conditions for tests, with Beam at 4000 GeV and fix number of
# interactions

from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
Gauss().Luminosity        = 0.121*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 93.2*SystemOfUnits.millibarn

#  but set a fix number of interaction to 1 for easier comparisons
from Configurables import Generation
gaussGen = Generation("Generation")
gaussGen.PileUpTool = "FixedNInteractions"

#--Set the average position of the IP in nominal position
Gauss().InteractionPosition = [  0.0*SystemOfUnits.mm ,
                                 0.0*SystemOfUnits.mm ,
                                 0.0*SystemOfUnits.mm ]

#  and closed Velo around it
from Configurables import CondDB
CondDB().LocalTags["SIMCOND"] = ["velo-closed"]


#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 73.54*SystemOfUnits.mm

#--Set the energy of the beam,
Gauss().BeamMomentum      = 4.0*SystemOfUnits.TeV

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. And tilts of the beam line
Gauss().BeamHCrossingAngle = -0.236*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.100*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.0038*SystemOfUnits.mm
Gauss().BeamBetaStar      = 3.2*SystemOfUnits.m


