from Gaudi.Configuration import importOptions
importOptions("$GAUSSROOT/options/Gauss-2016.py")
importOptions("$DECFILESROOT/options/10000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")

# Disable spill-over
from Configurables import Gauss
Gauss().SpilloverPaths = []

# Events to process
from Configurables import LHCbApp
LHCbApp().EvtMax = 100

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'
