# Config for performance and regression testing 
from Gaudi.Configuration import *

importOptions("$GAUSSROOT/options/Gauss-DEV.py")
importOptions("$PRCONFIGOPTS/Gauss/PR-Beam4000GeV-NominalBeamLine-VeloClosed-MagDown-fix1.py")
importOptions("$DECFILESROOT/options/10000000.py")
importOptions("$LBPYTHIAROOT/options/Pythia.py")
importOptions("$APPCONFIGOPTS/Gauss/gen.py")


from Configurables import LHCbApp
LHCbApp().EvtMax = 10000


