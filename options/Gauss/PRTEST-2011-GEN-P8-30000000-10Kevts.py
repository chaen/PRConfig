from Gaudi.Configuration import importOptions
importOptions("$GAUSSROOT/options/Gauss-2011.py")
importOptions("$DECFILESROOT/options/30000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")

# Events to process
from Configurables import LHCbApp
LHCbApp().EvtMax = 10000

# Run only the generator part, no full simulation
importOptions('$APPCONFIGOPTS/Gauss/gen.py')

# Fix the number of interactions to one
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'
