from Gaudi.Configuration import importOptions
importOptions("$GAUSSROOT/options/Gauss-2016.py")
importOptions("$DECFILESROOT/options/42981000.py")
importOptions("$LBALPGENROOT/options/AlpGen.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")

# Disable spill-over
from Configurables import Gauss
Gauss().SpilloverPaths = []

# Events to process
from Configurables import LHCbApp
LHCbApp().EvtMax = 10000

# Run only the generator part, no full simulation
importOptions('$APPCONFIGOPTS/Gauss/gen.py')

# Fix the number of interactions to one
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'
