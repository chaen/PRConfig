# Config for performance and regression testing 
from Gaudi.Configuration import *

importOptions("$GAUSSROOT/options/Gauss-DEV.py")
importOptions("$PRCONFIGOPTS/Gauss/PR-Beam4000GeV-NominalBeamLine-VeloClosed-MagDown-fix1.py")
importOptions("$DECFILESROOT/options/10000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")


from Configurables import LHCbApp
LHCbApp().EvtMax = 1000


