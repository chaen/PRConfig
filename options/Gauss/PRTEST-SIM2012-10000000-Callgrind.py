# Config for performance testing with callgrind
#
# To run:
# gaudirun.py --profilerName=valgrindcallgrind --profilerExtraOptions="__instr-atstart=no -v __smc-check=all-non-file __dump-instr=yes __trace-jump=yes" PRTEST-SIM2012-10000000-Callgrind.py

from Gaudi.Configuration import *
from Configurables import CallgrindProfile

def addProfile():
    p = CallgrindProfile()
    p.StartFromEventN = 2
    p.StopAtEventN = 3
    p.DumpAtEventN= 3
    p.DumpName = "calgrinddump"
    p.OutputLevel = DEBUG
    GaudiSequencer("Simulation").Members.insert(0,p )
    
importOptions("$GAUSSROOT/options/Gauss-2011.py")
importOptions("$DECFILESROOT/options/10000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")


from Configurables import LHCbApp
LHCbApp().EvtMax = 4
appendPostConfigAction(addProfile)

