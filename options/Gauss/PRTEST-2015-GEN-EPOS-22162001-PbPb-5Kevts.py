from Gaudi.Configuration import importOptions
importOptions("$APPCONFIGOPTS/Gauss/PbPb-Beam2510GeV-md100-2015-fix1.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2015.py")
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")
importOptions("$DECFILESROOT/options/22162001.py")
importOptions("$LBCRMCROOT/options/EPOS.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")

# DB tags
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20161124-vc-md100"

# Events to process
LHCbApp().EvtMax = 5000

# Run only the generator part, no full simulation
importOptions('$APPCONFIGOPTS/Gauss/gen.py')

# Fix the number of interactions to one
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'
